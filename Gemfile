source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', group: :doc

# My Gems

# WIRB highlights inspected Ruby objects.
gem 'wirb'
# Haml (HTML Abstraction Markup Language)
gem 'haml-rails'
# ZURB Foundation on Sass/Compass
gem 'foundation-rails', '~>5.5.3.2'
# Flexible authentication solution for Rails with Warden
gem 'devise'
# The official AWS SDK for Ruby.
gem 'aws-sdk'
# Provides helpers to integrate the AWS SDK for Ruby with Ruby on Rails.
gem 'aws-sdk-rails'
# Simple, efficient background processing for Ruby.
gem 'sidekiq'
# Shoryuken is a super efficient AWS SQS thread based message processor
gem 'shoryuken'
# Classy web-development dressed in a DSL
gem 'sinatra', :require => nil
# A Ruby library that encodes QR Codes
gem 'rqrcode'
gem 'wicked_pdf'


# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
	# Call 'byebug' anywhere in the code to stop execution and get a debugger console
	gem 'byebug'
	# Fast Faker, generates dummy data
	gem 'ffaker'
	# BDD for Ruby
	gem 'rspec-rails'
	# Provides a Jasmine Spec Runner and sets up jasmine-headless-webkit
	# gem 'jasmine-rails'
end

group :development do
	# Access an IRB console on exception pages or by using <%= console %> in views
	gem 'web-console'
	# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
	gem 'spring'
end

group :test do
	# Framework and DSL for defining and using factories
	gem 'factory_girl'
	# Auto-install phantomjs on demand for current platform. Comes with poltergeist integration.
	gem 'phantomjs2'
	# Gem for integration testing
	gem 'capybara'
	# A PhantomJS driver for Capybara
	gem 'poltergeist'
end
