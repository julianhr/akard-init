states = [
	'Aguascalientes',
	'Baja California',
	'Baja California Sur',
	'Campeche',
	'Ciudad de México',
	'Chiapas',
	'Chihuahua',
	'Coahuila',
	'Colima',
	'Durango',
	'Estado de México',
	'Guanajuato',
	'Guerrero',
	'Hidalgo',
	'Jalisco',
	'Michoacán',
	'Morelos',
	'Nayarit',
	'Nuevo León',
	'Oaxaca',
	'Puebla',
	'Querétaro',
	'Quintana Roo',
	'San Luis Potosí',
	'Sinaloa',
	'Sonora',
	'Tabasco',
	'Tamaulipas',
	'Tlaxcala',
	'Veracruz',
	'Yucatán',
	'Zacatecas'
]

ActiveRecord::Base.transaction do
	states.each do |state|
		Admin::State.create(name: state)
	end
	puts "Finished seeding State data."
end


agent_julian = Agent.new(
	first_name: 'John', 
	last_name: 'Smith',
	email: 'admin@email.com',
	password: '123456789'
)
puts "Agent Julian seeded." if agent_julian.save


email_registration = Admin::Email.new(
	model: 'users',
	purpose: 'registro_exito',
	subject: 'This is the registration email template.',
	template_name: 'TBD'
)
email_coupon_winner = Admin::Email.new(
	model: 'users',
	purpose: 'usuario_gano_cupon',
	subject: 'Cupón ganado en Akard.',
	template_name: 'TBD'
)
puts "Emails seeded." if email_registration.save and email_coupon_winner.save


survey = Admin::Survey.new(
	purpose: 'registro_exito',
	audience: 'users',
	template_name: 'TBD'
)
puts "Survey seeded." if survey.save
