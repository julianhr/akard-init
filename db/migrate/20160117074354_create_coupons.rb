class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :business, index: true, foreign_key: true
      t.references :business_branch, index: true, foreign_key: true
      t.string :type_of_coupon
      t.boolean :publish_active, null: false, default: true
      t.datetime :publish_start_datetime
      t.datetime :publish_end_datetime
      t.datetime :valid_start_datetime
      t.datetime :valid_end_datetime
      t.datetime :exchanged_datetime
      t.string :status
      t.string :title
      t.text :description
      t.decimal :price_regular, {precision: 8, scale: 2}
      t.decimal :price_promo, {precision: 8, scale: 2}
      t.integer :quantity
      t.integer :available
      t.text :conditions
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
