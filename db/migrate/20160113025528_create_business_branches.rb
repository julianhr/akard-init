class CreateBusinessBranches < ActiveRecord::Migration
  def change
    create_table :business_branches do |t|
      t.references :business, index: true, foreign_key: true
      t.string :type_of_branch
      t.string :street_name
      t.string :street_no
      t.string :inner_no
      t.string :street_context
      t.string :borough
      t.string :city
      t.integer :zip
      t.references :state, index: true, foreign_key: true
      t.text :contact
      t.decimal :latitude, {precision: 10, scale: 7}
      t.decimal :longitude, {precision: 10, scale: 7}
      t.float :grade
      t.integer :graded_by
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
