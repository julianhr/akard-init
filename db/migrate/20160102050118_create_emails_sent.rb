class CreateEmailsSent < ActiveRecord::Migration
  def change
    create_table :emails_sent do |t|
      t.references :email, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :sent
      t.string :status
      t.integer :attempts
      t.jsonb :response
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
