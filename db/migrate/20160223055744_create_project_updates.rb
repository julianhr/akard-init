class CreateProjectUpdates < ActiveRecord::Migration
  def change
    create_table :project_updates do |t|
      t.references :user, index: true, foreign_key: true
      t.string :email_status
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
