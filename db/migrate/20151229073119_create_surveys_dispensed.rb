class CreateSurveysDispensed < ActiveRecord::Migration
  def change
    create_table :surveys_dispensed do |t|
      t.references :survey, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :where
      t.jsonb :response
      t.json :metadata

      t.timestamps null: false
    end
  end
end
