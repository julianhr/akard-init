class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :model
      t.string :purpose
      t.string :subject
      t.string :template_name

      t.timestamps null: false
    end
  end
end
