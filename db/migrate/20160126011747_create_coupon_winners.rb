class CreateCouponWinners < ActiveRecord::Migration
  def change
    create_table :coupon_winners do |t|
      t.references :coupon, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :token
      t.references :email_sent, index: true, foreign_key: true
      t.string :status
      t.datetime :redeemed_at
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
