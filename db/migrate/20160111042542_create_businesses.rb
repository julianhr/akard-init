class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :name
      t.string :line_of_business
      t.string :url
      t.text :description
      t.jsonb :metadata

      t.timestamps null: false
    end
  end
end
