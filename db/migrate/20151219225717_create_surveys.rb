class CreateSurveys < ActiveRecord::Migration
	def change
		create_table :surveys do |t|
			t.string :purpose
			t.string :audience
			t.string :template_name

			t.timestamps null: false
		end
	end
end
