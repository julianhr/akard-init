# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160223055744) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agents", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.boolean  "approved"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "agents", ["confirmation_token"], name: "index_agents_on_confirmation_token", unique: true, using: :btree
  add_index "agents", ["email"], name: "index_agents_on_email", unique: true, using: :btree
  add_index "agents", ["reset_password_token"], name: "index_agents_on_reset_password_token", unique: true, using: :btree
  add_index "agents", ["unlock_token"], name: "index_agents_on_unlock_token", unique: true, using: :btree

  create_table "business_branches", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "type_of_branch"
    t.string   "street_name"
    t.string   "street_no"
    t.string   "inner_no"
    t.string   "street_context"
    t.string   "borough"
    t.string   "city"
    t.integer  "zip"
    t.integer  "state_id"
    t.text     "contact"
    t.decimal  "latitude",       precision: 10, scale: 7
    t.decimal  "longitude",      precision: 10, scale: 7
    t.float    "grade"
    t.integer  "graded_by"
    t.jsonb    "metadata"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "business_branches", ["business_id"], name: "index_business_branches_on_business_id", using: :btree
  add_index "business_branches", ["state_id"], name: "index_business_branches_on_state_id", using: :btree

  create_table "businesses", force: :cascade do |t|
    t.string   "name"
    t.string   "line_of_business"
    t.string   "url"
    t.text     "description"
    t.jsonb    "metadata"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "coupon_winners", force: :cascade do |t|
    t.integer  "coupon_id"
    t.integer  "user_id"
    t.string   "token"
    t.integer  "email_sent_id"
    t.string   "status"
    t.datetime "redeemed_at"
    t.jsonb    "metadata"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "coupon_winners", ["coupon_id"], name: "index_coupon_winners_on_coupon_id", using: :btree
  add_index "coupon_winners", ["email_sent_id"], name: "index_coupon_winners_on_email_sent_id", using: :btree
  add_index "coupon_winners", ["user_id"], name: "index_coupon_winners_on_user_id", using: :btree

  create_table "coupons", force: :cascade do |t|
    t.integer  "business_id"
    t.integer  "business_branch_id"
    t.string   "type_of_coupon"
    t.boolean  "publish_active",                                 default: true, null: false
    t.datetime "publish_start_datetime"
    t.datetime "publish_end_datetime"
    t.datetime "valid_start_datetime"
    t.datetime "valid_end_datetime"
    t.datetime "exchanged_datetime"
    t.string   "status"
    t.string   "title"
    t.text     "description"
    t.decimal  "price_regular",          precision: 8, scale: 2
    t.decimal  "price_promo",            precision: 8, scale: 2
    t.integer  "quantity"
    t.integer  "available"
    t.text     "conditions"
    t.jsonb    "metadata"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "coupons", ["business_branch_id"], name: "index_coupons_on_business_branch_id", using: :btree
  add_index "coupons", ["business_id"], name: "index_coupons_on_business_id", using: :btree

  create_table "emails", force: :cascade do |t|
    t.string   "model"
    t.string   "purpose"
    t.string   "subject"
    t.string   "template_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "emails_sent", force: :cascade do |t|
    t.integer  "email_id"
    t.integer  "user_id"
    t.boolean  "sent"
    t.string   "status"
    t.integer  "attempts"
    t.jsonb    "response"
    t.jsonb    "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "emails_sent", ["email_id"], name: "index_emails_sent_on_email_id", using: :btree
  add_index "emails_sent", ["user_id"], name: "index_emails_sent_on_user_id", using: :btree

  create_table "project_updates", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email_status"
    t.jsonb    "metadata"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "project_updates", ["user_id"], name: "index_project_updates_on_user_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "purpose"
    t.string   "audience"
    t.string   "template_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "surveys_dispensed", force: :cascade do |t|
    t.integer  "survey_id"
    t.integer  "user_id"
    t.string   "where"
    t.jsonb    "response"
    t.json     "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "surveys_dispensed", ["survey_id"], name: "index_surveys_dispensed_on_survey_id", using: :btree
  add_index "surveys_dispensed", ["user_id"], name: "index_surveys_dispensed_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.jsonb    "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "business_branches", "businesses"
  add_foreign_key "business_branches", "states"
  add_foreign_key "coupon_winners", "coupons"
  add_foreign_key "coupon_winners", "emails_sent"
  add_foreign_key "coupon_winners", "users"
  add_foreign_key "coupons", "business_branches"
  add_foreign_key "coupons", "businesses"
  add_foreign_key "emails_sent", "emails"
  add_foreign_key "emails_sent", "users"
  add_foreign_key "project_updates", "users"
  add_foreign_key "surveys_dispensed", "surveys"
  add_foreign_key "surveys_dispensed", "users"
end
