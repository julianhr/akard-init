require 'rails_helper'
require 'support/include_haml_helpers'

RSpec.describe ApplicationHelper, type: :helper do
	include_context 'include_haml_helpers'

	admin_fullpath_test = {
		'/'				=> 'user',
		'/any'			=> 'user',
		'/admins'		=> 'user',
		'/admin'			=> 'admin',
		'/admin/'		=> 'admin',
		'/admin/any'	=> 'admin',
		'/admin/any/'	=> 'admin'
	}

	describe '#get_title' do
		let(:title_admin) { 'AKard - Admin' }
		let(:title_user) { 'AKard' }

		admin_fullpath_test.each do |fullpath, user_type|
			it 'returns correct title' do
				title = eval('title_' + user_type)
				expect(get_title(fullpath)).to eq(title)
			end
		end
	end

	describe '#set_robots_meta_tag' do
		let(:no_follow_meta_tag) do 
			capture_haml {
				haml_tag :meta, {name: 'robots', content: 'noindex, nofollow, noarchive'}
			}
		end

		admin_fullpath_test.each do |fullpath, user_type|
			it 'returns correct title' do
				case user_type
				when 'admin'
					res = no_follow_meta_tag
				when 'user'
					res = nil
				end

				expect(capture_haml { set_robots_meta_tag(fullpath) }).to eq(res)
			end
		end
	end

	describe '#global_notifications' do
		context 'setting "notice" variable' do 
			let(:notice) { 'This is a notifications' }
			let(:alert) { nil }
			let(:html_s) { capture_haml { global_notifications() } }

			specify { expect(html_s).to have_css('div.alert-box.info', count: 1) }
			specify { expect(html_s).to have_css('button.close', count: 1) }
			specify { expect(html_s).to include(notice) }
		end

		context 'setting "alert" variable' do 
			let(:notice) { nil }
			let(:alert) { 'This is an alert' }
			let(:html_s) { capture_haml { global_notifications() } }

			specify { expect(html_s).to have_css('div.alert-box.info', count: 1) }
			specify { expect(html_s).to have_css('button.close', count: 1) }
			specify { expect(html_s).to include(alert) }
		end

		context 'setting "alert" variable' do 
			let(:notice) { nil }
			let(:alert) { nil }
			let(:html_s) { capture_haml { global_notifications() } }

			specify { expect(html_s).not_to have_css('div.alert-box.info', count: 1) }
			specify { expect(html_s).not_to have_css('button.close', count: 1) }
			specify { expect(html_s).not_to have_css('div.alert-box.info', text: /\w/) }
		end
	end

	describe '#wrap_lines_with_tag' do
		let(:first_line) { "This is the first line." }
		let(:text) do
		%Q(

			#{first_line}
			This is the second line.
			This is the third line.

		)	
		end
		let(:html_s) { capture_haml { wrap_lines_with_tag(text, :li) } }

		context 'valid arguments' do 
			specify { expect(html_s).to have_css('li', text: /.{2,}/, count: 3) }
			specify { expect(html_s).to have_css('li', text: first_line, count: 1) }
		end

		context 'invalid arguments' do 
			it 'raises error if text is not of type String' do
				expect { wrap_lines_with_tag(Object.new, :li) }.to raise_error(TypeError)
			end

			it 'raises error if tag does not respond to #to_sym' do
				expect { wrap_lines_with_tag(text, Object.new) }.to raise_error(TypeError)
			end
		end
	end

	describe '#format_datetime' do
		context 'valid arguments' do 
			it 'returns a string with morning time' do
				year, month, day, hour, minute = 2016, 3, 19, 8, 0
				datetime = DateTime.new year, month, day, hour, minute
				response = format_datetime(datetime)
				expect(response).to be_a(String)
				expect(response).to eq('sábado, 19/marzo/2016,  8:00 AM')
			end

			it 'returns a string with afternoon time' do
				year, month, day, hour, minute = 2016, 3, 19, 23, 0
				datetime = DateTime.new year, month, day, hour, minute
				response = format_datetime(datetime)
				expect(response).to be_a(String)
				expect(response).to eq('sábado, 19/marzo/2016, 11:00 PM')
			end
		end

		context 'invalid arguments' do 
			it 'raises TypeError if argument does not respond to #to_datetime' do
				expect { format_datetime(Object.new) }.to raise_error(TypeError)
			end
		end
	end

	describe '#form_field' do
		context 'valid arguments' do 
			pending
		end

		context 'invalid arguments' do 
			pending
		end
	end

	describe '#generic_logo_section' do
		it 'contains main elements' do
			testing_text = 'About Testing'
			html = capture_haml { generic_logo_section(:h2, testing_text) }
			expect(html).to have_css('.column.medium-2 > a > img[alt="Logo"]')
			expect(html).to have_css('.column.medium-8.end > h2.title-generic', text: testing_text)
		end
	end
end
