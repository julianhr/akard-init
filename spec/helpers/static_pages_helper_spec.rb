require 'rails_helper'
require 'support/include_haml_helpers'

RSpec.describe StaticPagesHelper, type: :helper do
	include_context 'include_haml_helpers'

	describe '#generic_logo_section' do
		it 'contains main elements' do
			testing_text = 'About Testing'
			html = capture_haml { static_logo_section(:h2, testing_text) }
			expect(html).to have_css('.column.medium-2 > a > img[alt="Logo"]')
			expect(html).to have_css('.column.medium-8.end > h2.title-static', text: testing_text)
		end
	end

end
