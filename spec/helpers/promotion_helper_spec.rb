require 'rails_helper'
require 'support/include_haml_helpers'

RSpec.describe PromotionHelper, type: :helper do
	include_context 'include_haml_helpers'

	fixtures :coupons
	fixtures :users

	let(:coupon_fixture) { coupons(:sushi) }

	describe '#business_address' do
		it "has the basic elements" do
			html = capture_haml { business_address(coupon_fixture) }
			expect(html).to have_css('address', count: 1)
			expect(html).to have_css('br')
			expect(html).to have_text(coupon_fixture.business_branch.street_name)
			expect(html).to have_text(coupon_fixture.business_branch.state.name)
		end
	end

	describe '#promotion_time_left_noscript' do
		def run_method(end_date)
			promotion_time_left_noscript(end_date)
		end
			
		context 'Same time zone with valid arguments' do 
			specify { expect(run_method(Time.now - 1.day))
					.to eq("Tiempo agotado") }
			specify { expect(run_method(Time.now + 29.minutes + 9.seconds))
					.to eq("29 min 9 seg") }
			specify { expect(run_method(Time.now + 23.hours + 9.minutes + 59.seconds))
					.to eq("23 hr 9 min 59 seg") }
			specify { expect(run_method(Time.now + 1.day + 4.hours + 9.minutes + 59.seconds))
					.to eq("1 día 04:09:59") }
			specify { expect(run_method(Time.now + 2.days + 4.hours + 9.minutes + 59.seconds))
					.to eq("2 días 04:09:59") }
		end

		context 'Different time zone with valid arguments' do 
			pending
		end
	end

	describe '#linked_business_map' do
		context 'invalid arguments' do
			let(:html) do
				size = '300x300'
				zoom_level = 15
				linked_business_map(size, zoom_level)
			end

			before(:example) { @promotion = coupon_fixture }

			specify { expect(html).to have_css('a', count: 1) }
			specify { expect(html).to have_css('img', count: 1) }
		end
	end

	describe 'print_signup_survey_link_if_unanswered' do
		context 'valid arguments' do 
			it 'returns correct html if user has not answered signup survey' do
				user = users(:maria) # this fixture has no survey answers.
				html = capture_haml { print_signup_survey_link_if_unanswered(user) }
				expect(html).to have_text("Ayúdanos")
				expect(html).to have_css("a[href='#{registro_exito_path(email: user.email, id: user.id)}']")
				expect(html).to have_css("a[target='_blank']")
			end
		end

		context 'invalid arguments' do 
			it 'raises TypeError if user is nil' do
				expect {print_signup_survey_link_if_unanswered(nil)}.to raise_error(TypeError)
			end

			it 'raises TypeError if user is not of type User' do
				expect {print_signup_survey_link_if_unanswered(Object.new)}.to raise_error(TypeError)
			end
		end
	end
end
