require 'rails_helper'
require 'support/include_haml_helpers'

RSpec.describe Admin::BusinessesHelper, type: :helper do
	include_context "include_haml_helpers"

	fixtures :business_branches
	let(:bb_fixture) { business_branches(:americas) }

	describe '#concatenate_address_from_branch' do
		context 'valid arguments' do 
			let(:address) { helper.concatenate_address_from_branch( bb_fixture ) }

			specify { expect(address).to have_text(bb_fixture.street_name) }
			specify { expect(address).to have_text(bb_fixture.street_no) }
			specify { expect(address).to have_text(" int. ") }
		end

		context 'invalid arguments' do 
			it 'raises TypeError if argument not a Admin::BusinessBranch' do
				expect { concatenate_address_from_branch(Object.new) }
					.to raise_error(TypeError)
			end
		end
	end

	describe '#print_logos_from_paths' do
		let(:image_paths) do
			[
				'business/1/image_1.jpg', 
				'business/1/image_2.jpg'
			]
		end
		let(:business) { bb_fixture.business }
		let(:html) { capture_haml {print_logos_from_paths(image_paths, :li, business)} }

		before(:example) { allow(File).to receive(:size) {1024} }

		context 'valid arguments' do 
			specify { expect(html).to have_css("li > a > img", count: 2) }
			specify { image_url = root_url + 'images/' + image_paths[0] 
				expect(html).to have_link("a", href: image_url, count: 1) }
			specify { expect(html).to have_css("li > a", text: 'Eliminar', count: 2) }
		end

		context 'invalid arguments' do 
			it 'image_paths does not respond to #each' do
				expect { print_logos_from_paths(Object.new, :li, business) }
					.to raise_error(TypeError)
			end

			it 'tag does not respond to #to_sym' do
				expect { print_logos_from_paths(image_paths, Object.new, business) }
					.to raise_error(TypeError)
			end

			it 'business is not of type Admin::Business' do
				expect { print_logos_from_paths(image_paths, :li, Object.new) }
					.to raise_error(TypeError)
			end
		end
	end
end
