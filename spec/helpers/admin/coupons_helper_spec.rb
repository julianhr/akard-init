require 'rails_helper'
require 'support/include_haml_helpers'

RSpec.describe Admin::CouponsHelper, type: :helper do
	include_context "include_haml_helpers"

	fixtures :coupons
	fixtures :businesses

	let(:coupon_fixture) { coupons(:sushi) }

	describe '#print_images_from_paths' do
		let(:image_paths) do
			[
				'coupons/1/image_1.jpg', 
				'coupons/1/image_2.jpg'
			]
		end
		let(:html) { capture_haml {print_images_from_paths(image_paths, :li, coupon_fixture)} }

		before(:example) { allow(File).to receive(:size) {1024} }

		context 'valid arguments' do 
			specify { expect(html).to have_css("li > a > img", count: 2) }
			specify { image_url = root_url + 'images/' + image_paths[0] 
				expect(html).to have_link("a", href: image_url, count: 1) }
			specify { expect(html).to have_css("li > a", text: 'Eliminar', count: 2) }
		end

		context 'invalid arguments' do 
			it 'image_paths does not respond to #each_with_index' do
				expect { print_images_from_paths(Object.new, :li, coupon_fixture) }
					.to raise_error(TypeError)
			end

			it 'tag does not respond to #to_sym' do
				expect { print_images_from_paths(image_paths, Object.new, coupon_fixture) }
					.to raise_error(TypeError)
			end

			it 'coupon is not of type Admin::Coupon' do
				expect { print_images_from_paths(image_paths, :li, Object.new) }
					.to raise_error(TypeError)
			end
		end
	end

	describe '#form_select_for_business' do
		let(:coupon) { coupons(:sushi) }
		let(:businesses) { Admin::Business.order(:name) }
		let(:form_double) { instance_double("ActionView::Helpers::FormBuilder") }
		let(:buffer_double) { instance_double("ActiveSupport::SafeBuffer") }

		before(:example) do
			allow(form_double).to receive(:select) { buffer_double }
			@coupon = coupon
			@businesses = businesses
		end

		after(:example) do
			@coupon = nil
			@businesses = nil
		end

		context 'valid params' do 
			it 'returns a SafeBuffer' do
				expect(form_select_for_business(form_double)).to eq(buffer_double)
			end

			context '@options' do 
				let(:businesses_list) { businesses.map {|b| [b.name, b.id]} }
				let(:options) { {selected: coupon.business_id, include_blank: true} }

				it 'assigns @options[selected]=business.id if params[coupon_business_id]=nil and coupons is persisted' do
					expect(form_double).to receive(:select)
						.with(:business_id, businesses_list, options)
					form_select_for_business(form_double)					
				end

				it 'assigns @options[selected]=params[coupon_business_id] if present' do
					coupon_business_id = 999
					params[:coupon_business_id] = coupon_business_id
					options[:selected] = coupon_business_id
					expect(form_double).to receive(:select)
						.with(:business_id, businesses_list, options)
					form_select_for_business(form_double)					
				end
			end
		end

		context 'invalid params' do 
			it 'returs nil if @businesses=nil' do
				@businesses = nil
				expect(form_select_for_business(form_double)).to be nil
			end			
		end
	end

end
