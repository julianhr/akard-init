require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe User, type: :model do
	include_context "model_attributes"

	fixtures :users

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_user) { User.new user_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'email' do
		it_behaves_like("email_tests", :email) do
			let(:new_record) { new_user }
			let(:error_msg_email_missing) { error_msg[:email_missing_cry] }
		end

		context 'invalid attributes' do 
			it 'must be unique' do
				new_user.email = users(:pedro).email
				expect(new_user).to be_invalid
				expect(new_user.errors.messages[:email]).to include(error_msg[:email_duplicity])
			end
		end
	end

end
