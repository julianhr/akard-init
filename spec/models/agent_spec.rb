require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Agent, type: :model do
	include_context "model_attributes"

	fixtures :admin_agents

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_agent) { Agent.new agent_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'first_name' do
		it_behaves_like "accepts_string", :first_name, false do
			let(:new_record) { new_agent }
			let(:test_string) { 'John' }
		end

		context 'invalid attributes' do 
			it 'cannot be > 254 characters' do
				new_agent.first_name = f_bacon.characters 255
				expect(new_agent).to be_invalid
				expect(new_agent.errors.messages[:first_name]).to include(error_msg[:text_too_long])
			end
		end
	end

	describe 'last_name' do
		it_behaves_like "accepts_string", :last_name, false do
			let(:new_record) { new_agent }
			let(:test_string) { 'Smith' }
		end

		context 'invalid attributes' do 
			it 'cannot be > 254 characters' do
				new_agent.last_name = f_bacon.characters 255
				expect(new_agent).to be_invalid
				expect(new_agent.errors.messages[:last_name]).to include(error_msg[:text_too_long])
			end
		end
	end

	describe 'email' do
		it_behaves_like("email_tests", :email) do
			let(:new_record) { new_agent }
			let(:error_msg_email_missing) { error_msg[:email_missing] }
		end

		context 'invalid attributes' do 
			it 'must be unique' do
				new_agent.email = admin_agents(:julian).email
				expect(new_agent).to be_invalid
				expect(new_agent.errors.messages[:email]).to include(error_msg[:email_duplicity])
			end
		end
	end

end
