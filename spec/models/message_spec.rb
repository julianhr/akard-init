require 'rails_helper'
require 'support/model_attributes'

RSpec.describe Message, type: :model do
	include_context "model_attributes"

	let(:error_msg) { ErrorMsg::MSG }

	context 'valid attributes' do 
		it 'responds to name, email, subject and body' do
			msg = Message.new
			%i(name email subject body).each do |attr|
				expect(msg.respond_to? attr).to be true
			end
		end
	end

	context 'invalid attributes' do 
		it 'name cannot be empty' do
			msg = Message.new message_valid_attributes
			msg.name = nil
			expect(msg).to be_invalid
			expect(msg.errors[:name].first).to eq(error_msg[:empty_field])
		end

		it 'email cannot be empty' do
			msg = Message.new message_valid_attributes
			msg.email = nil
			expect(msg).to be_invalid
			expect(msg.errors[:email].first).to eq(error_msg[:empty_field])
		end

		it 'rejects an invalid email' do
			msg = Message.new message_valid_attributes
			msg.email = 'invalid_email'
			expect(msg).to be_invalid
			expect(msg.errors[:email].first).to eq(error_msg[:email_invalid])
		end

		it 'subject cannot be empty' do
			msg = Message.new message_valid_attributes
			msg.subject = nil
			expect(msg).to be_invalid
			expect(msg.errors[:subject].first).to eq(error_msg[:empty_field])
		end

		it 'body cannot be empty' do
			msg = Message.new message_valid_attributes
			msg.body = nil
			expect(msg).to be_invalid
			expect(msg.errors[:body].first).to eq(error_msg[:empty_field])
		end
	end

end
