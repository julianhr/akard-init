require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::Email, type: :model do
	include_context 'model_attributes'

	fixtures :emails

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_email) { Admin::Email.new email_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'MODEL_OPT' do
		it 'is composed of strings' do
			Admin::Email::MODEL_OPT.each do |entry|
				expect(entry).to be_a(String)
			end
		end
	end

	describe 'model' do
		it_behaves_like "accepts_string", :model, false do
			let(:new_record) {new_email}
			let(:test_string) { Admin::Email::MODEL_OPT.last }
		end

		context 'invalid attributes' do 
			it 'receives an invalid option' do
				email = new_email
				email.model = 'invalid model'
				error_txt = error_msg_invalid_opt(email.model)

				expect(email).to be_invalid
				expect(email.errors.messages[:model]).to include(error_txt)
			end
		end
	end

	describe 'purpose' do
		it_behaves_like "accepts_string", :purpose, false do
			let(:new_record) {new_email}
			let(:test_string) { 'test string' }
		end

		context 'invalid attributes' do 
			it 'purpose is not unique' do
				email = new_email
				email.purpose = emails(:registration).purpose

				expect(email).to be_invalid
				expect(email.errors.messages[:purpose]).to include(error_msg[:not_unique])
			end
		end
	end

	describe 'subject' do
		it_behaves_like "accepts_string", :subject, false do
			let(:new_record) {new_email}
			let(:test_string) { 'This is the email subject line' }
		end
	end

	describe 'template_name' do
		it_behaves_like "accepts_string", :template_name, false do
			let(:new_record) {new_email}
			let(:test_string) { 'valid template name' }
		end
	end

end
