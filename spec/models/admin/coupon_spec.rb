require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::Coupon, type: :model do
	include_context 'model_attributes'

	fixtures :coupons
	fixtures :businesses
	fixtures :business_branches

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_coupon) { Admin::Coupon.new coupon_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	before(:example) { Time.zone = 'Guadalajara' }

	describe 'TYPE_OF_COUPON_OPT' do
		it 'is composed of strings' do
			Admin::Coupon::TYPE_OF_COUPON_OPT.each do |entry|
				expect(entry).to be_a(String)
			end
		end
	end

	describe 'business_id' do
		it_behaves_like "record_foreign_id", :business_id, false do
			let(:new_record) {new_coupon}
			let(:fixture_id) {businesses(:restaurant).id}
		end

		it "is not included in the Admin::BusinessBranch id's" do
			new_coupon.business_id = 999
			error_txt = error_msg_invalid_opt(new_coupon.business_id)

			expect(new_coupon).to be_invalid
			expect(new_coupon.errors.messages[:business_id]).to include(error_txt)
		end
	end

	describe 'business_branch_id' do
		it_behaves_like "record_foreign_id", :business_branch_id, false do
			let(:new_record) {new_coupon}
			let(:fixture_id) {business_branches(:americas).id}
		end

		it "is not included in the Admin::BusinessBranch id's" do
			new_coupon.business_branch_id = 999
			error_txt = error_msg_invalid_opt(new_coupon.business_branch_id)

			expect(new_coupon).to be_invalid
			expect(new_coupon.errors.messages[:business_branch_id]).to include(error_txt)
		end
	end

	describe 'type_of_coupon' do
		it_behaves_like("accepts_string", :type_of_coupon, false) do
			let(:new_record) { new_coupon }
			let(:test_string) { Admin::Coupon::TYPE_OF_COUPON_OPT.first }
		end

		context 'invalid attributes' do 
			it 'string not in TYPE_OF_COUPON_OPT' do
				new_coupon.type_of_coupon = 'invalid option'
				error_txt = error_msg_invalid_opt(new_coupon.type_of_coupon)

				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:type_of_coupon]).to include(error_txt)
			end
		end
	end

	describe 'publish_active' do
		context 'valid attributes' do 
			it 'can be empty' do
				new_coupon.publish_active = nil
				expect(new_coupon).to be_valid
			end

			it 'accepts true or false if persisted' do
				coupon = coupons(:sushi)
				coupon.publish_active = true
				expect(coupon).to be_valid
			end

			it 'accepts 0 or 1 if persisted' do
				coupon = coupons(:sushi)
				coupon.publish_active = 1
				expect(coupon).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'cannot be nil if persisted' do
				new_coupon.publish_active = nil
				expect{new_coupon.save!}.to raise_error(ActiveRecord::StatementInvalid)
			end
		end
	end

	shared_examples 'datetime_tests' do |method_name, time_adj, can_be_nil|
		context 'valid attributes' do 
			it 'accepts a DateTime' do
				new_coupon[method_name] = DateTime.now + time_adj
				expect(new_coupon).to be_valid
			end

			it 'accepts parsable text' do
				t = Time.now
				new_coupon[method_name] = DateTime.new(t.year, t.month, t.day, t.hour, t.min) + time_adj
				expect(new_coupon).to be_valid
			end

			it 'saves datime as UTC but converts to local time when consumed' do
				time = new_coupon[method_name]
				before_typecast_attribute = (method_name.to_s + '_before_type_cast').to_sym
				new_coupon[method_name] = time.to_datetime
				new_coupon.save
				new_coupon.reload

				expect(Time.zone.name).to eq('Guadalajara')
				expect(new_coupon.send(before_typecast_attribute))
					.not_to include(sprintf '%02d:%02d:%02d', time.hour, time.min, time.sec)
				expect(new_coupon.send(before_typecast_attribute))
					.to include(sprintf '%02d:%02d', time.min, time.sec)
			end
		end

		context 'invalid attributes' do 
			it 'cannot be empty', unless: (method_name == :exchanged_datetime) do 
				new_coupon[method_name] = ''
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:empty_field])
			end

			it 'converts unparsable string to nil', unless: can_be_nil do
				new_coupon[method_name] = 'invalid_string'
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:empty_field])
			end

			it 'converts unparsable string to nil', if: can_be_nil do
				new_coupon[method_name] = 'invalid_string'
				expect(new_coupon).to be_valid
			end
		end
	end

	describe 'publish_start_datetime' do
		it_behaves_like "datetime_tests", :publish_start_datetime, 0, false
	end

	describe 'publish_end_datetime' do
		it_behaves_like "datetime_tests", :publish_end_datetime, 2.days, false

		context 'invalid attributes' do 
			it 'cannot be = publish_start_datetime' do
				new_coupon.publish_start_datetime = DateTime.now
				new_coupon.publish_end_datetime = new_coupon.publish_start_datetime

				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:publish_end_datetime])
					.to include(error_msg[:date_end_lt_or_eq_to_start])
			end

			it 'cannot be < publish_start_datetime' do
				new_coupon.publish_start_datetime = DateTime.now
				new_coupon.publish_end_datetime = new_coupon.publish_start_datetime - 1.hour

				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:publish_end_datetime])
					.to include(error_msg[:date_end_lt_or_eq_to_start])
			end
		end
	end

	describe 'valid_start_datetime' do
		it_behaves_like "datetime_tests", :valid_start_datetime, 0, false
	end

	describe 'valid_end_datetime' do
		it_behaves_like "datetime_tests", :valid_end_datetime, 1.month, false

		context 'invalid attributes' do 
			it 'cannot be <= valid_start_datetime' do
				new_coupon.valid_end_datetime = new_coupon.valid_start_datetime - 1.minute
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:valid_end_datetime])
					.to include(error_msg[:date_valid_lt_publish])
			end

			it 'cannot be < publish_end_datetime' do
				new_coupon.valid_end_datetime = new_coupon.publish_end_datetime - 1.minute
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:valid_end_datetime])
					.to include(error_msg[:date_valid_lt_publish])
			end
		end
	end

	describe 'exchanged_datetime' do
		it_behaves_like "datetime_tests", :exchanged_datetime, 1.month, true

		context 'valid attributes' do 
			it 'can be nil' do
				new_coupon.exchanged_datetime = nil
				expect(new_coupon).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'cannot be < valid_start_datetime' do
				new_coupon.exchanged_datetime = new_coupon.valid_start_datetime - 1.minute
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:exchanged_datetime])
					.to include(error_msg[:date_exchanged_lt_valid_start])
			end
		end
	end

	describe 'status' do
		it_behaves_like("accepts_string", :status, true) do
			let(:new_record) { new_coupon }
			let(:test_string) { 'new status' }
		end
	end

	describe 'title' do
		it_behaves_like("accepts_string", :title, false) do
			let(:new_record) { new_coupon }
			let(:test_string) { f_bacon.sentence 15 }
		end

		context 'invalid attributes' do 
			it 'length > 254 characters' do
				new_coupon.title = f_bacon.sentence 40
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:title])
					.to include(error_msg[:text_too_long])
			end
		end
	end

	describe 'description' do
		it_behaves_like("accepts_string", :description, false) do
			let(:new_record) { new_coupon }
			let(:test_string) { f_bacon.paragraph 30 }
		end
	end

	shared_examples 'price_tests' do |method_name, price_float, price_int|
		context 'valid attributes' do 
			it 'receives a positive decimal' do
				price = price_float
				new_coupon[method_name] = price
				expect(new_coupon).to be_valid
				expect(new_coupon[method_name]).to eq(price)
			end

			it 'receives a positive integer' do
				price = price_int
				new_coupon[method_name] = price
				expect(new_coupon).to be_valid
				expect(new_coupon[method_name]).to eq(price)
			end
		end
	end

	describe 'price_regular' do
		it_behaves_like "price_tests", :price_regular, 999.99, 999

		context 'invalid attributes' do 
			it 'receives 0' do
				new_coupon.price_regular = 0
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:price_regular])
					.to include(error_msg[:number_currency_gr_zero])
			end

			it 'receives negative number' do
				price = -1.99
				new_coupon.price_regular = price
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:price_regular])
					.to include(error_msg[:number_currency_gr_zero])
			end
		end
	end

	describe 'price_promo' do
		it_behaves_like "price_tests", :price_promo, 9.99, 9

		context 'valid attributes' do 
			it 'receives 0' do
				new_coupon.price_promo = 0
				expect(new_coupon).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'cannot be = :price_reguar' do
				new_coupon.price_promo = new_coupon.price_regular
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:price_promo])
					.to include(error_msg[:coupon_price_promo_gt_regular])
			end

			it 'cannot be > :price_reguar' do
				new_coupon.price_promo = new_coupon.price_regular + 0.01
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:price_promo])
					.to include(error_msg[:coupon_price_promo_gt_regular])
			end

			it 'receives negative number' do
				price = -1.99
				new_coupon[:price_promo] = price
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:price_promo])
					.to include(error_msg[:number_currency])
			end
		end

	end

	describe 'conditions' do
		it_behaves_like("accepts_string", :conditions, false) do
			let(:new_record) { new_coupon }
			let(:test_string) { f_bacon.paragraph 30 }
		end
	end

	shared_examples 'amount_tests' do |method_name, value|
		context 'valid attributes' do 
			it 'accepts a well-formed entry' do
				new_coupon[method_name] = value
				expect(new_coupon).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'empty value' do
				new_coupon[method_name] = ''
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:empty_field])
			end

			it 'is a negative digit' do
				new_coupon[method_name] = -123_456
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:number_digits_positive])
			end

			it 'has a decimal part' do
				new_coupon[method_name] = 123.567
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:number_digits_positive])
			end

			it 'contains letters' do
				new_coupon[method_name] = 'abc'
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[method_name])
					.to include(error_msg[:number_digits_positive])
			end
		end
	end

	describe 'quantity' do
		it_behaves_like "amount_tests", :quantity, 1_000
	end

	describe 'available' do
		it_behaves_like "amount_tests", :available, 1

		context 'valid attributes' do 
			it 'between 0 and :quantity' do
				new_coupon.available = 1
				expect(new_coupon).to be_valid
			end

			it 'is = :quantity' do
				new_coupon.available = new_coupon.quantity
				expect(new_coupon).to be_valid
			end

			it 'is = 0' do
				new_coupon.available = 0
				expect(new_coupon).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'is > :quantity' do
				new_coupon.available = new_coupon.quantity + 1
				expect(new_coupon).to be_invalid
				expect(new_coupon.errors.messages[:available])
					.to include(error_msg[:coupon_availability_out_of_range])
			end
		end
	end

end
