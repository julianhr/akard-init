require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::BusinessBranch, type: :model do
	include_context 'model_attributes'
  
	fixtures :business_branches
	fixtures :businesses
	fixtures :states

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_business_branch) { Admin::BusinessBranch.new business_branch_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'TYPE_OF_BRANCH_OPT' do
		it 'is an array of strings' do
			Admin::BusinessBranch::TYPE_OF_BRANCH_OPT.each do |entry|
				expect(entry).to be_a(String)
			end
		end
	end

	describe 'business_id' do
		it_behaves_like "record_foreign_id", :business_id, false do
			let(:new_record) {new_business_branch}
			let(:fixture_id) {businesses(:restaurant).id}
		end

		context 'invalid attributes' do 
			it "is not included in the Admin::State id's" do
				new_business_branch.business_id = 999
				error_txt = error_msg_invalid_opt(new_business_branch.business_id)

				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:business_id]).to include(error_txt)
			end
		end
	end

	describe 'type_of_branch' do
		it_behaves_like("accepts_string", :type_of_branch, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { Admin::BusinessBranch::TYPE_OF_BRANCH_OPT.first }
		end

		context 'invalid attributes' do 
			it 'string not in TYPE_OF_BRANCH_OPT' do
				new_business_branch.type_of_branch = 'invalid option'
				error_txt = error_msg_invalid_opt(new_business_branch.type_of_branch)

				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:type_of_branch]).to include(error_txt)
			end
		end
	end

	describe 'street_name' do
		it_behaves_like("accepts_string", :street_name, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { 'Avenida Vallarta' }
		end
	end

	describe 'street_no' do
		it_behaves_like("accepts_string", :street_name, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { '123456' }
		end

		context 'valid attributes' do 
			it 'accepts a number' do
				new_business_branch.street_no = 123456
				expect(new_business_branch).to be_valid
			end
		end
	end

	describe 'borough' do
		it_behaves_like("accepts_string", :borough, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { 'Col. Americana' }
		end
	end

	describe 'city' do
		it_behaves_like("accepts_string", :city, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { 'Zapopan' }
		end
	end

	describe 'zip' do
		it_behaves_like("accepts_string", :zip, false) do
			let(:new_record) { new_business_branch }
			let(:test_string) { '12345' }
		end

		context 'valid attributes' do 
			it 'accepts an integer' do
				new_business_branch.zip = 12345
				expect(new_business_branch).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'contains letters' do
				new_business_branch.zip = 'abc'
				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:zip]).to include(error_msg[:number_digits_only])
			end

			it 'is < 5 digits' do
				new_business_branch.zip = 1234
				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:zip]).to include(error_msg[:number_zip])
			end

			it 'is > 5 digits' do
				new_business_branch.zip = 123456
				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:zip]).to include(error_msg[:number_zip])
			end
		end
	end

	describe 'state_id' do
		it_behaves_like "record_foreign_id", :state_id, false do
			let(:new_record) {new_business_branch}
			let(:fixture_id) {states(:jalisco).id}
		end

		context 'invalid attributes' do 
			it "is not included in the Admin::State id's" do
				new_business_branch.state_id = 999
				error_txt = error_msg_invalid_opt(new_business_branch.state_id)

				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:state_id]).to include(error_txt)
			end
		end
	end

	shared_examples 'coordinate_tests' do |value|
		context 'valid attributes' do 
			it 'accepts a number as string' do
				new_business_branch.latitude = value.to_s
				expect(new_business_branch).to be_valid
				expect(new_business_branch.latitude).to eq(value)
			end

			it 'accepts a number as number type' do
				new_business_branch.latitude = value
				expect(new_business_branch).to be_valid
				expect(new_business_branch.latitude).to eq(value)
			end
		end

		context 'invalid attributes' do 
			it 'contains letters' do
				new_business_branch.latitude = 'abc'
				expect(new_business_branch).to be_invalid
				expect(new_business_branch.errors.messages[:latitude]).to include(error_msg[:number_only])
			end
		end
	end

	describe('latitude') { it_behaves_like "coordinate_tests", -123.1234567 }
	describe('longitude') { it_behaves_like "coordinate_tests", -123.1234567 }
end
