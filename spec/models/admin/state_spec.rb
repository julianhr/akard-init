require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::State, type: :model do
	include_context 'model_attributes'

	let(:new_state) { Admin::State.new state_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'name' do
		it_behaves_like "accepts_string", :name, false do
			let(:new_record) { new_state }
			let(:test_string) { 'Jalisco' }
		end
	end

end
