require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::Agent, type: :model do
	include_context "model_attributes"

	fixtures :admin_agents

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_agent) { Admin::Agent.new admin_agent_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'first_name' do
		it_behaves_like("accepts_string", :first_name, false) do
			let(:new_record) { new_agent }
			let(:test_string) { 'John' }
		end
	end

	describe 'last_name' do
		it_behaves_like("accepts_string", :last_name, false) do
			let(:new_record) { new_agent }
			let(:test_string) { 'Smith' }
		end
	end

	describe 'email' do
		it_behaves_like("email_tests", :email) do
			let(:new_record) { new_agent }
			let(:error_msg_email_missing) { error_msg[:email_missing] }
		end

		context 'invalid attributes' do 
			it 'must be unique' do
				new_agent.email = admin_agents(:julian).email
				expect(new_agent).to be_invalid
				expect(new_agent.errors.messages[:email]).to include(error_msg[:email_duplicity])
			end
		end
	end

end
