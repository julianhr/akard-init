require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::Business, type: :model do
	include_context 'model_attributes'

	fixtures :businesses

	let(:new_business) { Admin::Business.new business_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'name' do
		it_behaves_like "accepts_string", :name, false do
			let(:new_record) { new_business }
			let(:test_string) { 'My Valid Name' }
		end
	end

	describe 'line_of_business' do
		it_behaves_like "accepts_string", :name, false do
			let(:new_record) { new_business }
			let(:test_string) { 'My Valid Line of Business' }
		end
	end

	describe 'url' do
		context 'valid attributes' do 
			let(:business) { new_business }

			let(:urls) do
				[
					'http://mypage.com',
					'http://my-page.com/my_full-path',
					'http://my-page.com/my_full-path/index.html'
				]
			end

			it 'has https with www' do 
				urls.each do |url|
					url = url.gsub("http", "https")
					url = url.gsub("://", "://www.")
					business.url = url
					expect(business).to be_valid
				end
			end

			it 'has https without www' do 
				urls.each do |url|
					business.url = url.gsub("http", "https")
					expect(business).to be_valid
				end
			end

			it 'has http with www' do 
				urls.each do |url|
					business.url = url.gsub("://", "://www.")
					expect(business).to be_valid
				end
			end

			it 'http without www' do 
				urls.each do |url|
					business.url = url
					expect(business).to be_valid
				end
			end
		end

		context 'invalid attributes' do 
			it 'cannot be duplicated' do
				new_business.url = businesses(:restaurant).url
				expect(new_business).to be_invalid
				expect(new_business.errors.messages[:url]).to include(error_msg[:url_duplicity])
			end

			it 'is a plain string' do
				new_business.url = 'mypage'
				expect(new_business).to be_invalid
				expect(new_business.errors.messages[:url]).to include(error_msg[:url_invalid])
			end

			it 'has www and domain is missing extension' do
				new_business.url = 'http://www.mypage'
				expect(new_business).to be_invalid
				expect(new_business.errors.messages[:url]).to include(error_msg[:url_invalid])
			end

			it 'has not www and domain is missing extension' do
				new_business.url = 'http://mypage'
				expect(new_business).to be_invalid
				expect(new_business.errors.messages[:url]).to include(error_msg[:url_invalid])
			end			
		end
	end

end
