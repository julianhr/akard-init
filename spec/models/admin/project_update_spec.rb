require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::ProjectUpdate, type: :model do
	include_context "model_attributes"

	fixtures :users

	let(:new_pu) { Admin::ProjectUpdate.new project_update_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'EMAIL_STATUS_OPT' do
		it 'should be an array' do
			expect(Admin::ProjectUpdate::EMAIL_STATUS_OPT).to be_an(Array)
		end
	end

	describe 'user_id' do
		it_behaves_like "accepts_string", :user_id, false do
			let(:new_record) {new_pu}
			let(:test_string) { project_update_valid_attributes[:user_id] }
		end

		context 'invalid attributes' do 
			it 'rejects non-digit strings' do
				new_pu.user_id = 'test'
				expect(new_pu).to be_invalid
				expect(new_pu.errors.messages[:user_id]).to include(error_msg[:number_digits_only])
			end

			it 'rejects an invalid user_id' do
				invalid_id = 999
				new_pu.user_id = invalid_id
				error_txt = error_msg_invalid_opt(new_pu.user_id)

				expect(new_pu).to be_invalid
				expect(new_pu.errors.messages[:user_id]).to include(error_txt)
			end
		end
	end

	describe 'email_status' do
		it_behaves_like "accepts_string", :email_status, false do
			let(:new_record) {new_pu}
			let(:test_string) { project_update_valid_attributes[:email_status] }
		end

		context 'invalid attributes' do 
			it 'rejects a string not in EMAIL_STATUS_OPT' do
				new_pu.email_status = 'invalid entry'
				error_txt = error_msg_invalid_opt(new_pu.email_status)
				expect(new_pu).to be_invalid
				expect(new_pu.errors.messages[:email_status]).to include(error_txt)
			end
		end
	end

	describe 'metadata' do
		context 'valid attributes' do 
			it 'saves a hash' do
				new_pu.save
				new_pu.reload
				expect(new_pu.metadata).to be_a(Hash)
				expect(new_pu.metadata).to be_present
			end
		end
	end

end
