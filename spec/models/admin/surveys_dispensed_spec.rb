require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::SurveysDispensed, type: :model do
	include_context 'model_attributes'

	fixtures :surveys_dispensed
	fixtures :surveys
	fixtures :users

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_sd) { Admin::SurveysDispensed.new survey_dispensed_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'user_id' do
		it_behaves_like("record_foreign_id", :user_id, false) do
			let(:new_record) { new_sd }
			let(:fixture_id) { users(:pedro).id }
		end

		context 'invalid attributes' do 
			it 'is not included in User' do
				new_sd.user_id = 999
				expect(new_sd).to be_invalid
				error_txt = error_msg_invalid_opt(new_sd.user_id)
				expect(new_sd.errors.messages[:user_id]).to include(error_txt)
			end
		end
	end

	describe 'survey_id' do
		it_behaves_like("record_foreign_id", :survey_id, false) do
			let(:new_record) { new_sd }
			let(:fixture_id) { surveys(:signup).id }
		end

		context 'invalid attributes' do 
			it 'is not included in Admin::Survey' do
				new_sd.survey_id = 999
				expect(new_sd).to be_invalid
				error_txt = error_msg_invalid_opt(new_sd.survey_id)
				expect(new_sd.errors.messages[:survey_id]).to include(error_txt)
			end
		end
	end

	describe 'where' do
		it_behaves_like "accepts_string", :where, false do
			let(:new_record) { new_sd }
			let(:test_string) { 'valid where' }
		end
	end

end
