require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::Survey, type: :model do
	include_context "model_attributes"
	
	fixtures :surveys

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_survey) { Admin::Survey.new survey_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'AUDIENCE_OPT' do
		specify {expect(Admin::Survey::AUDIENCE_OPT).to be_an(Array)}
	end

	describe 'purpose' do
		it_behaves_like "accepts_string", :purpose, false do
			let(:new_record) { new_survey }
			let(:test_string) { 'signup_success' }
		end
	end

	describe 'audience' do
		it_behaves_like "accepts_string", :audience, false do
			let(:new_record) { new_survey }
			let(:test_string) { 'users' }
		end

		context 'invalid attributes' do 
			it 'cannot be outside the options in AUDIENCE_OPT' do
				new_survey.audience = 'new_audience'
				error_txt = error_msg_invalid_opt(new_survey.audience)
				expect(new_survey).to be_invalid
				expect(new_survey.errors.messages[:audience]).to include(error_txt)
			end
		end
	end

	describe 'template_name' do
		it_behaves_like "accepts_string", :template_name, false do
			let(:new_record) { new_survey }
			let(:test_string) { 'users' }
		end
	end

end
