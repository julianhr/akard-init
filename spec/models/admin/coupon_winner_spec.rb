require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::CouponWinner, type: :model do
	include_context 'model_attributes'

	fixtures :coupon_winners
	fixtures :coupons
	fixtures :emails_sent
	fixtures :users

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_coupon_winner) { Admin::CouponWinner.new coupon_winner_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'STATUS_OPT' do
		it 'is composed of strings' do
			Admin::CouponWinner::STATUS_OPT.each do |entry|
				expect(entry).to be_a(String)
			end
		end
	end

	describe 'coupon_id' do
		it_behaves_like("record_foreign_id", :coupon_id, false) do
			let(:new_record) {new_coupon_winner}
			let(:fixture_id) {coupons(:sushi).id}
		end
	end

	describe 'user_id' do
		it_behaves_like("record_foreign_id", :user_id, false) do
			let(:new_record) {new_coupon_winner}
			let(:fixture_id) {users(:pedro).id}
		end
	end

	describe 'token' do
		it_behaves_like "accepts_string", :token, false do
			let(:new_record) {new_coupon_winner}
			let(:test_string) { 'ABCDEFG' }
		end
	end

	describe 'email_sent_id' do
		it_behaves_like("record_foreign_id", :email_sent_id, true) do
			let(:new_record) {new_coupon_winner}
			let(:fixture_id) {emails_sent(:coupon_sushi).id}
		end
	end

	describe 'status' do
		it_behaves_like "accepts_string", :status, false do
			let(:new_record) {new_coupon_winner}
			let(:test_string) { Admin::CouponWinner::STATUS_OPT.first }
		end

		context 'invalid attributes' do 
			it 'option not in STATUS_OPT' do
				new_coupon_winner.status = 'new invalid option'
				error_txt = error_msg_invalid_opt(new_coupon_winner.status)

				expect(new_coupon_winner).to be_invalid
				expect(new_coupon_winner.errors.messages[:status]).to include(error_txt)
			end
		end
	end

end
