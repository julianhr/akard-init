require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::EmailSent, type: :model do
	include_context 'model_attributes'

	fixtures :admin_agents
	fixtures :users
	fixtures :emails
	fixtures :emails_sent

	let(:f_bacon) { FFaker::BaconIpsum }
	let(:new_email_sent) { Admin::EmailSent.new email_sent_valid_attributes }
	let(:error_msg) { ErrorMsg::MSG }

	describe 'STATUS_OPT' do
		specify {expect(Admin::EmailSent::STATUS_OPT).to be_a Hash}
	end

	describe 'status' do
		it_behaves_like "accepts_string", :status, false do
			let(:new_record) {new_email_sent}
			let(:test_string) { Admin::EmailSent::STATUS_OPT.keys.first.to_s }
		end

		context 'invalid_attributes' do 
			it "is not an option from STATUS_OPT" do
				new_email_sent.status = 'invalid status'
				expect(new_email_sent).to be_invalid
			end
		end
	end

	describe 'attempts' do
		context 'valid attributes' do 
			it 'accepts a well-formed entry' do
				new_email_sent.attempts = 5
				expect(new_email_sent).to be_valid
			end
		end

		context 'invalid attributes' do 
			it 'can only be a number' do
				new_email_sent.attempts = 'string'
				expect(new_email_sent).to be_invalid
				expect(new_email_sent.errors.messages[:attempts]).to include(error_msg[:number_digits_only])
			end
		end
	end
end
