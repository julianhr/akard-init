require 'rails_helper'

RSpec.describe SurveySignupProcessor do
	fixtures :users
	fixtures :surveys_dispensed

	f_bacon = FFaker::BaconIpsum

	let(:params_valid_survey) do
		ssp_opt = SurveySignupProcessor::QUESTIONS

		ActionController::Parameters.new({
			category: "laptops, electronics",
			subscribed: "Groupon, Groupalia",
			apps_use: ssp_opt[:apps_use].last,
			age_range: ssp_opt[:age_range].last,
			sex: ssp_opt[:sex].last,
			comments: "I want to receive discounts now."
		})
	end

	let(:user_fixture) { users(:pedro) }
	let(:sd_fixture) { surveys_dispensed(:followup) }

	def new_params(hash)
		ActionController::Parameters.new(hash)
	end

	def new_ssp(params, user_id = user_fixture.id, sd_id = sd_fixture.id)
		SurveySignupProcessor.new(params, user_id, sd_id)
	end

	describe 'OPTIONS' do
		it 'is an Array with valid entries' do
			SurveySignupProcessor::QUESTIONS.each do |k, v|
				if v.class == Array
					expect(v.count).to be > 0
				end
			end
		end
	end

	describe '#build' do
		context 'valid attributes' do 
			it 'returns true' do
				ssp = new_ssp(params_valid_survey)
				expect(ssp.build).to be true
			end
		end

		context 'invalid attributes' do 
			it 'raises TypeError if survey params are not of type ActionController::Parameters' do
				ssp = new_ssp({})
				expect {ssp.build}.to raise_error(TypeError)
			end

			it 'raises IndexError error if user was not found' do
				ssp = new_ssp(params_valid_survey, 'invalid', sd_fixture.id)
				expect {ssp.build}.to raise_error(IndexError)
			end

			it 'raises IndexError error if user_dispensed was not found' do
				ssp = new_ssp(params_valid_survey, user_fixture.id, 'invalid')
				expect {ssp.build}.to raise_error(IndexError)
			end
		end
	end

	describe '#verify_responses' do
		context 'valid attributes' do 
			it 'returns a Hash with all original survey elements' do
				ssp = new_ssp(params_valid_survey)
				res = ssp.verify_responses()
				expect(res).to be_a(Hash)
				expect(res).to eq( params_valid_survey.symbolize_keys )
			end

			it 'calls #check_build' do
				ssp = new_ssp(params_valid_survey)
				expect(ssp).to receive(:check_build)
				ssp.verify_responses
			end
		end

		context 'invalid attributes' do 
			it 'empty answers returns empty hash' do
				ssp = new_ssp(new_params({}))
				expect(ssp.verify_responses).to eq({}) 
			end

			it 'questions outside scope are ignored' do
				ssp = new_ssp( new_params({invalid: 'invalid'}) )
				expect(ssp.verify_responses).to eq({}) 
			end

			it 'chops answers > 4,000 characters to that length' do
				params = new_params({comments: f_bacon.paragraph(120)})
				expect(params[:comments].length).to be > 4_000
				ssp = new_ssp(params)
				res = ssp.verify_responses()
				expect(res[:comments].length).to eq 4_000
			end
		end
	end

	describe '#save' do
		context 'valid attributes' do 
			it 'saves the responses' do
				sd = sd_fixture
				expect(sd.response).to be nil
				ssp = new_ssp(params_valid_survey, user_fixture.id, sd.id)
				ssp.verify_responses()
				expect( ssp.save() ).to be true
				sd.reload
				expect(sd.response).to be_a(Hash)
			end			
		end

		context 'invalid situations' do 
			it 'survey is en empty, then save is not called' do
				sd = sd_fixture
				expect(sd.response).to be nil
				expect(sd).not_to receive(:save)
				ssp = new_ssp(new_params({}), user_fixture.id, sd.id)
				ssp.verify_responses()
				expect( ssp.save() ).to be nil
				sd.reload
				expect(sd.response).to be nil
			end

			it '#save is called before #verify_responses' do
				sd = sd_fixture
				expect(sd.response).to be nil
				ssp = new_ssp(params_valid_survey, user_fixture.id, sd.id)
				expect( ssp.save() ).to be nil
				sd.reload
				expect(sd.response).to be nil
			end
		end
	end

end
