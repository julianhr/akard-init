require 'rails_helper'

RSpec.describe EmailSqsUpdater do

	fixtures :emails
	fixtures :emails_sent
	fixtures :users
	f_bacon = FFaker::BaconIpsum

	let(:delivery_json) { '{"Type" : "Notification","MessageId" : "effbeef5-a638-5032-a678-a1f1a9018fc0","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Delivery\",\"delivery\":{\"timestamp\":\"2016-01-04T06:11:05.317Z\",\"processingTimeMillis\":8998,\"recipients\":[\"user1@email.com\"],\"smtpResponse\":\"250 OK id=1aFyM6-0005Aj-1O\",\"reportingMTA\":\"a27-30.smtp-out.us-west-2.amazonses.com\"},\"mail\":{\"timestamp\":\"2016-01-04T06:10:56.319Z\",\"source\":\"akard@akardcupones.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"sendingAccountId\":\"274732928163\",\"messageId\":\"000001520b428abf-b7012cda-fe4f-4dad-b8ee-59f139d55e85-000000\",\"destination\":[\"user1@email.com\"]}}","Timestamp" : "2016-01-04T06:11:05.434Z","SignatureVersion" : "1","Signature" : "UqYh1x+AYGvqGUXZ6DTdd1SPpAG7Tg3GnaEq9sboabkYytaytVAFp1xz+4ar2DFCe/tMLup+TlVnLnTUjtUan8pThs0RGfiy6hZe9+5mNQG+rD1HaQSxvHnHme/p7/MGHzbhydK5LvJ3IO00obwQGK1bSshtVGO00synp+bOlujvxalNvVk/GQfVvzDaPfIVlENv72uy8HqaJX/UBgNKzNNjyMYwxe2kJVFr6KxoExLGlweCjV867819wmtL2iUUtM7d4dKdZXH4M0hYH9YDgRrE3u7Hb5b5bcoWsx/r6sJlcuV7/K7IZEQ7KVd5r9sg/osajVMxIWCoB/YWZw1jXg==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }
	let(:bounce_transient_json) { '{"Type" : "Notification","MessageId" : "b0fc65ec-b9bd-5713-b00f-2ae99f36df47","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceType\":\"Transient\",\"bounceSubType\":\"General\",\"bouncedRecipients\":[{\"emailAddress\":\"user1@email.com\"}],\"timestamp\":\"2016-01-05T02:02:50.000Z\",\"feedbackId\":\"000001520f85c54e-6cec4fdb-b350-11e5-99ca-dd0f7e97cb79-000000\"},\"mail\":{\"timestamp\":\"2016-01-05T02:02:46.000Z\",\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"source\":\"akard@akardcupones.com\",\"messageId\":\"000001520f85b8fe-e22480e3-f4bc-499b-af11-b625a488936c-000000\",\"destination\":[\"ooto@simulator.amazonses.com\"],\"sendingAccountId\":\"274732928163\"}}","Timestamp" : "2016-01-05T02:02:51.290Z","SignatureVersion" : "1","Signature" : "OOK9aZLRY+u9ufhVBHgxNjs8Ro7cExwYs4rRLMT9k4DzD99OEOSJrbnXhug8bzSx889BMOkRA/+d2fB/o7R9TCtiVmJhDLfnS5uzjBhNRZEaR7+gvOZarzPI/qUG1f1Wq3rzv0jADGkt0zpX1XQJ4qsUS6bJLOc47WT3mA9FmsbcoFQfGfFIHFIrtit1fVtgOSJSI5WSKsNuiF+RAwq17p1EL9pZMm9Hz3Ug+qMMAWjY/bIShquvrlzLKPIKQNKsXlpGOWn2KOI6wvkpwgwVobW48Fn+M6CO1/lrlA7WxXz6K2dRAYO9PSv2/hQQ2qPPMWecldJtTO6hwNdWCJ+OJQ==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }
	let(:bounce_permanent_json) { '{"Type" : "Notification","MessageId" : "ad9ab6db-2d51-5c9f-bc10-f9de0b5fe4a9","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceType\":\"Permanent\",\"bounceSubType\":\"General\",\"reportingMTA\":\"dsn; a27-23.smtp-out.us-west-2.amazonses.com\",\"bouncedRecipients\":[{\"action\":\"failed\",\"emailAddress\":\"user1@email.com\",\"status\":\"5.1.1\",\"diagnosticCode\":\"smtp; 550 5.1.1 user unknown\"}],\"timestamp\":\"2016-01-05T02:00:46.132Z\",\"feedbackId\":\"000001520f83dce2-81dd2518-2f5b-44bb-951d-881eb49d4bdf-000000\"},\"mail\":{\"timestamp\":\"2016-01-05T02:00:44.000Z\",\"source\":\"akard@akardcupones.com\",\"sendingAccountId\":\"274732928163\",\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"messageId\":\"000001520f83d783-476e4077-97ee-4e4e-81aa-902ebaeb42dd-000000\",\"destination\":[\"bounce@simulator.amazonses.com\"]}}","Timestamp" : "2016-01-05T02:00:46.167Z","SignatureVersion" : "1","Signature" : "Zj1VXKgJpN9PWAa5ZAKTDIe+YyEzvypLZF0FYgpt1Yz1asX1P7ylD31/7Hf1UWxmgrAnwr4rYxB12APVcPW+460nCh+LzTqJc8bJZJAKj5d3FlvdNMOfSmNIPCET6vwIssIN+5vrQlOIKH0mj4moLV/AFd7CccPiRU9/wPifYgWii0g2a81FHXe7gCUNxjWUFh5W8HzckcOAAHKGQtKs+CNnTLYHCobY5oa2i0GuE78prFp9M02uSpV+TARDp0GStgZTUJSsH0MBzqyoLzhq/nGRBeT01N6L7w8V5bogV9K0ukZ6xNUW4YYwKv4iTvhOWoI2IpP5AWugxKAQb1Dwgw==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }
	let(:suppressed_json) { '{"Type" : "Notification","MessageId" : "49d0cb19-d113-5023-92a6-8410c44cb10b","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceSubType\":\"Suppressed\",\"bounceType\":\"Permanent\",\"reportingMTA\":\"dns; amazonses.com\",\"bouncedRecipients\":[{\"emailAddress\":\"user1@email.com\",\"status\":\"5.1.1\",\"diagnosticCode\":\"Amazon SES has suppressed sending to this address because it has a recent history of bouncing as an invalid address. For more information about how to remove an address from the suppression list, see the Amazon SES Developer Guide: http://docs.aws.amazon.com/ses/latest/DeveloperGuide/remove-from-suppressionlist.html \",\"action\":\"failed\"}],\"timestamp\":\"2016-01-08T01:38:39.487Z\",\"feedbackId\":\"000001521ee2b29b-8ae191be-b5a8-11e5-8905-df2dd2a3ada2-000000\"},\"mail\":{\"timestamp\":\"2016-01-08T01:38:39.487Z\",\"messageId\":\"000001521ee2ad04-dc8ffaca-6a86-4a8c-aaa5-67c8c7c8a2f1-000000\",\"destination\":[\"suppressionlist@simulator.amazonses.com\"],\"source\":\"akard@akardcupones.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"sendingAccountId\":\"274732928163\"}}","Timestamp" : "2016-01-08T01:38:39.527Z","SignatureVersion" : "1","Signature" : "EOh2wMDr63Y8Va49dp7EQS8N09LeL0gbnNz6KWTC/Vk5wgI9IGyfSyzhNxaK1gcIVjiPSbloieY/WabN8TMoBZ2ndv6IV4ETkRXbNUcb5bwGTJFolyj2kTpfwR8NoR1ERV9shmHnj0spPTbLT+K1zZurOUILSx2S/HroRmWHBem6uZvhxvJKba43v95yHJ7RQ+ty4ARhPcLKa5aMOtEY2XxsxqRCh7kfttoQb0VpUpYT94JlejsrlHCHlEIzKXGoJwi+qHSC1+yhRxk+LvkJwRQ+emzE2AD4hAk9NFO78K8Mqp6/Yed3G5cZf7L6RO463U4wBBIn+amFSz+K2VxxmQ==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }
	let(:complaint_json) { '{"Type" : "Notification","MessageId" : "3b7c3ab7-42a7-5fc8-9390-75b26472203a","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Complaint\",\"complaint\":{\"complainedRecipients\":[{\"emailAddress\":\"user1@email.com\"}],\"complaintFeedbackType\":\"abuse\",\"userAgent\":\"Amazon SES Mailbox Simulator\",\"timestamp\":\"2016-01-05T02:04:04.000Z\",\"feedbackId\":\"000001520f86e6e2-991c1cb0-b350-11e5-8957-29f751352319-000000\"},\"mail\":{\"timestamp\":\"2016-01-05T02:04:05.354Z\",\"source\":\"akard@akardcupones.com\",\"messageId\":\"000001520f86df49-979ad059-c403-48a8-8235-02518b5fb101-000000\",\"destination\":[\"complaint@simulator.amazonses.com\"],\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"sendingAccountId\":\"274732928163\"}}","Timestamp" : "2016-01-05T02:04:05.370Z","SignatureVersion" : "1","Signature" : "EBC12wbPnvZHHrN25qxWm7jK1y4LAN6qQ48kfLVbRjaC/TJ4nlo5tm7UUc7WldPGZp2s7rbiiYxD2ggccQURFlf6zczGivlP35EaRGHHckgQXIoqsxhmXPfkd/9bBesKjpugbZRpQR2nlI1spQKlwYqLWLT11nkjT6utsMtPvO7PaUhS1vu+HkbR6wvj+oHE33zqV6K99P/Dzw1KdtibVdPP3FOe6fZdi4tpief4S02N3I7hqqgTR42ZiCkI9qPwpBAuPAdff0cdkTlf1m5HdcpyaSHXPAvUIIjYI5nRZCL+DWkTUDFWzZreupkipq6Nd55qwa1LZn+vXtY2MNx1Mg==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }

	def parse_sqs_json(json) 
		hash = JSON.parse json
		hash['Message'] = JSON.parse(hash['Message'])
		return hash
	end

	def new_updater(body = delivery_json, version = 1, master_email = emails(:registration))
		EmailSqsUpdater.new(body, version, master_email)
	end

	describe '#build' do
		context 'valid arguments' do 
			it 'receives version as number' do
				updater = new_updater(delivery_json, 1, emails(:registration))
				expect(new_updater().build).to be true
			end

			it 'receives version as string' do
				updater = new_updater(delivery_json, ' 1 ', emails(:registration))
				expect(new_updater().build).to be true
			end

			it 'parses body into a hash' do
				updater = new_updater()
				updater.build()
				expect(updater.body_hash).to be_a(Hash)
				expect(updater.body_hash).to eq parse_sqs_json(delivery_json)
			end
		end

		context 'invalid arguments' do 
			it 'raises TypeError if body is not a String' do
				updater = new_updater(Object.new, 1, emails(:registration))
				expect {updater.build}.to raise_error(TypeError)
			end

			it 'raises ArgumentError if body is empty' do
				updater = new_updater('', 1, emails(:registration))
				expect {updater.build}.to raise_error(ArgumentError)
			end

			it 'raises TypeError if version is not a Fixnum or String' do
				updater = new_updater(delivery_json, Object.new, emails(:registration))
				expect {updater.build}.to raise_error(TypeError)
			end

			it 'raises TypeError if master_email is not an Admin::Email' do
				updater = new_updater(delivery_json, 1, Object.new)
				expect {updater.build}.to raise_error(TypeError)
			end
		end

		context 'anomalies' do 
			it "raises an error if body can't be parsed into a hash" do
				allow(JSON).to receive(:parse) { raise StandardError, "Test Parse" }
				updater = new_updater()
				expect {updater.build}.to raise_error(StandardError, "Test Parse")
			end
		end
	end


	describe '#update' do
		context 'valid arguments' do 
			let(:email_sent) { emails_sent(:registration_successful) }

			it 'calls build()' do
				updater = new_updater()
				allow(updater).to receive(:validate_signature_version)
				allow(updater).to receive(:update_delivery)

				expect(updater).to receive(:build)
				updater.update()
			end

			it 'updates email_sent correctly with json "delivery_json"' do
				hash = parse_sqs_json(delivery_json)
				updater = new_updater(delivery_json)

				expect(email_sent.status).not_to eq('delivery')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).not_to eq(hash)

				res = updater.update()
				expect(res).to eq(true)

				email_sent.reload

				expect(email_sent.status).to eq('delivery')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).to eq(hash)
			end

			it 'updates "email_sent" correctly with json "bounce_transient_json"' do
				hash = parse_sqs_json(bounce_transient_json)
				updater = new_updater(bounce_transient_json)

				expect(email_sent.status).not_to eq('temporary_bounce')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).not_to eq(hash)

				res = updater.update()
				expect(res).to eq(true)

				email_sent.reload

				expect(email_sent.status).to eq('temporary_bounce')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).to eq(hash)
			end

			it 'updates "email_sent" correctly with json "bounce_permanent_json"' do
				hash = parse_sqs_json(bounce_permanent_json)
				updater = new_updater(bounce_permanent_json)

				expect(email_sent.status).not_to eq('permanent_bounce')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).not_to eq(hash)

				res = updater.update()
				expect(res).to eq(true)

				email_sent.reload

				expect(email_sent.status).to eq('permanent_bounce')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).to eq(hash)
			end

			it 'updates "email_sent" correctly with json "suppressed_json"' do
				hash = parse_sqs_json(suppressed_json)
				updater = new_updater(suppressed_json)

				expect(email_sent.status).not_to eq('suppressed')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).not_to eq(hash)

				res = updater.update()
				expect(res).to eq(true)

				email_sent.reload

				expect(email_sent.status).to eq('suppressed')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).to eq(hash)
			end

			it 'updates "email_sent" correctly with json "complaint_json"' do
				hash = parse_sqs_json(complaint_json)
				updater = new_updater(complaint_json)

				expect(email_sent.status).not_to eq('complained')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).not_to eq(hash)

				res = updater.update()
				expect(res).to eq(true)

				email_sent.reload

				expect(email_sent.status).to eq('complained')
				response_update = email_sent.response.select {|k,v| k.to_s.match(/\d+_aws_sqs\z/)}
				expect(response_update.values.first).to eq(hash)
			end
		end
	end

end
