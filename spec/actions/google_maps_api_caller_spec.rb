require 'rails_helper'

RSpec.describe GoogleMapsApiCaller do
	let(:latitude) { 23.1234567 }
	let(:longitude) { -105.1234567 }
	let(:location) { "#{latitude},#{longitude}" }
	let(:size) { '300x300' }
	let(:zoom_level) { 16 }
	let(:valid_caller) { GoogleMapsApiCaller.new(latitude, longitude) }

	def uri_query_hash(string) 
		uri = URI.parse string
		URI.decode_www_form_component(uri.query).split('&').map {|x| x.split('=')}.to_h		
	end

	describe '#build' do
		context 'valid attributes' do 
			it 'instantiates correctly' do
				caller = valid_caller
				expect(caller.build).to be true
			end

			it 'removes leading and trailing whitespace fron latitude' do
				lat = ' 25.1234567 '
				caller = GoogleMapsApiCaller.new(lat, longitude)
				caller.build
				expect(caller.latitude).to eq(lat.strip)
			end

			it 'removes leading and trailing whitespace fron longitude' do
				long = ' -105.1234567 '
				caller = GoogleMapsApiCaller.new(latitude, long)
				caller.build
				expect(caller.longitude).to eq(long.strip)
			end
		end

		context 'invalid attributes' do 
			it 'receives a latitude that does not respond to "to_f"' do
				caller = GoogleMapsApiCaller.new(Object.new, longitude)
				expect { caller.build }.to raise_error(TypeError)
			end

			it 'receives a longitude that does not respond to "to_f"' do
				caller = GoogleMapsApiCaller.new(latitude, Object.new)
				expect { caller.build }.to raise_error(TypeError)
			end
		end
	end

	describe '#static_map_uri' do
		let(:host) { 'maps.googleapis.com' }
		let(:path) { '/maps/api/staticmap' }
		let(:uri) { URI.parse valid_caller.static_map_uri(size, zoom_level) }

		context 'valid attributes' do 
			specify { expect(uri.scheme).to eq('https') }
			specify { expect(uri.host).to eq(host) }
			specify { expect(uri.path).to eq(path) }

			context 'builds query correct' do
				let(:query_h) { uri_query_hash(uri.to_s) }

				specify { expect(query_h['center']).to eq(location) }
				specify { expect(query_h['key']).to match(/[\w\d_]+/i) }
				specify { expect(query_h['language']).to eq("es") }
				specify { expect(query_h['markers']).to eq(location) }
				specify { expect(query_h['size']).to eq(size) }
				specify { expect(query_h['zoom']).to eq(zoom_level.to_s) }
			end

			it 'removes leading and trailing whitespace from size' do
				caller = valid_caller
				caller.build
				new_size = ' 200x200 '
				res = caller.static_map_uri(new_size, zoom_level)
				query_h = uri_query_hash(res)
				expect(query_h['size']).to eq(new_size.strip)
			end

			it 'removes leading and trailing whitespace from zoom_level' do
				caller = valid_caller
				caller.build
				new_zoom = ' 15 '
				res = caller.static_map_uri(size, new_zoom)
				query_h = uri_query_hash(res)
				expect(query_h['zoom']).to eq(new_zoom.strip)
			end
		end

		context 'invalid attributes' do 
			it 'raises error if size if of incorrect type' do
				caller = valid_caller
				expect { caller.static_map_uri(Object.new, zoom_level) }.to raise_error(TypeError)
			end

			it 'raises error if zoom_level if of incorrect type' do
				caller = valid_caller
				expect { caller.static_map_uri(size, Object.new) }.to raise_error(TypeError)
			end
		end
	end

	describe '#uri_to_maps_in_google' do
		let(:host) { 'www.google.com' }
		let(:path) { "/maps/place/#{location()}/@#{location()},#{zoom_level}z" }
		let(:uri) { URI.parse valid_caller.uri_to_maps_in_google(zoom_level) }

		context 'valid attributes' do 
			specify { expect(uri.scheme).to eq('https') }
			specify { expect(uri.host).to eq(host) }
			specify { expect(uri.path).to eq(path) }

			it 'removes leading and trailing whitespace from zoom_level' do
				caller = valid_caller
				caller.build
				new_zoom = ' 15 '
				res = caller.static_map_uri(size, new_zoom)
				query_h = uri_query_hash(res)
				expect(query_h['zoom']).to eq(new_zoom.strip)
			end
		end

		context 'invalid attributes' do 
			it 'raises error if zoom level is of incorrect type' do
			 	caller = valid_caller
			 	caller.build
			 	expect { caller.uri_to_maps_in_google(Object.new) }.to raise_error(TypeError)
			end 		
		end	
	end

end
