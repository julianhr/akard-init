require 'rails_helper'

RSpec.describe ImageExplorer do
	f_bacon = FFaker::BaconIpsum

	def ie_instance(dir = ImageExplorer::DIR.keys.first, id = 1)
		ImageExplorer.new(dir, id)
	end

	describe 'DIR' do
		specify { expect(ImageExplorer::DIR).to be_a(Hash) }
	end

	describe 'FILE_TYPES' do
		specify { expect(ImageExplorer::FILE_TYPES).to be_an(Array) }
	end

	describe '#build' do
		context 'valid attributes' do 
			it 'builds correctly' do
				ie = ie_instance
				expect( ie.built() ).to be false
				expect { ie.build() }.not_to raise_error
				expect( ie.built() ).to be true
			end
		end

		context 'invalid attributes' do 
			before(:example) { expect(ExceptionHandler).to receive(:log) }

			it "raises TypeError unless id.respond_to? :to_sym and logs it" do
				expect { ie_instance(Object.new, 1).build }.to raise_error(TypeError)
			end

			it 'raises ArgumentError if invalid directory and logs it' do
				expect { ie_instance('invalid', 1).build }.to raise_error(ArgumentError)
			end

			it 'raises ArgumentError if invalid directory and logs it' do
				dir = ImageExplorer::DIR.keys.first
				expect { ie_instance(dir,'invalid').build }.to raise_error(ArgumentError)
			end

			it 'raises TyperError unless id.respond_to? :to_i and logs it' do
				expect { ie_instance('businesses', Object.new).build }.to raise_error(TypeError)
			end
		end

		context 'anomalies' do 
			before(:example) { expect(ExceptionHandler).to receive(:log) }

			it "raises IOError if system can't make directory and logs it" do
				allow(Dir).to receive(:exist?) {false}
				allow(Dir).to receive(:mkdir) {raise IOError}
				ie = ie_instance
				expect { ie.build() }.to raise_error(IOError, /.{2,}/)
			end
		end
	end

	describe '#get_image_paths' do
		before(:example) { allow(Dir).to receive(:exist?) {true} }

		context 'valid attributes' do 
			let(:image_paths) { ['1/image_3.jpg', '1/image_1.jpg'] }
			let(:file_paths) { ["dir/dir/#{image_paths[0]}", "dir/dir/#{image_paths[1]}"] }

			context 'returns a valid array' do
				let(:ie) { ie_instance() }

				before(:example) do
					allow(Dir).to receive(:[]) { file_paths }
				end

				specify { expect(ie.get_image_paths).to be_an(Array) }
				specify { expect(ie.get_image_paths.size).to eq(file_paths.size) }
				specify { expect(ie.get_image_paths[0]).to eq(image_paths[1]) }
				specify { expect(ie.get_image_paths[1]).to eq(image_paths[0]) }
			end
		end
	end

	describe '#get_image_file_names' do
		let(:image_names) { ['image_3.jpg', 'image_1.jpg'] }
		let(:file_paths) { ["dir/dir/1/#{image_names[0]}", "dir/dir/1/#{image_names[1]}"] }
		let(:ie) { ie_instance() }

		before(:example) { allow(Dir).to receive(:exist?) {true} }

		context 'valid attributes' do 

			context 'returns a valid array' do
				before(:example) do
					allow(ie).to receive(:get_image_paths) { file_paths }
				end

				specify { expect(ie.get_image_file_names).to be_an(Array) }
				specify { expect(ie.get_image_file_names.size).to eq(file_paths.size) }
				specify { expect(ie.get_image_file_names[0]).to eq(image_names[1]) }
				specify { expect(ie.get_image_file_names[1]).to eq(image_names[0]) }
			end

			it 'returns an empty array if no images found' do
				allow(ie).to receive(:get_image_paths) { [] }
				expect(ie.get_image_file_names).to be_an(Array)
				expect(ie.get_image_file_names.empty?).to be true
			end
		end

		context 'anomalies' do 
			it "raises passed exception if it exists" do
				allow(ie).to receive(:get_image_paths) {raise ArgumentError}
				expect {ie.get_image_file_names}.to raise_error(ArgumentError)
			end
		end
	end

	describe '#upload_image' do
		let(:img_name) { 'test_1.jpg' }
		let(:uploaded_file) { instance_double("ActionDispatch::Http::UploadedFile") }
		let(:ie) { ie_instance() }

		before(:example) do
			allow(uploaded_file).to receive(:read)
			allow(ie).to receive(:build_directory_path) { 'dir/dir' }
		end

		context 'valid attributes' do
			it 'uploads the image' do
				image = File.open("spec/photos/#{img_name}", 'r')
				file = class_double("File")

				expect(file).to receive(:open) { true }
				res = ie.upload_image(img_name, uploaded_file, file)
				expect(res).to be true
			end 
		end

		context 'anomalies' do 
			it 'raises IOError if image upload fails' do
				image = File.open("spec/photos/#{img_name}", 'r')
				file = class_double("File")

				expect(file).to receive(:open) { raise StandardError }
				expect(ExceptionHandler).to receive(:log)
				expect {ie.upload_image(img_name, uploaded_file, file)}.to raise_error(IOError, /.{2,}/)
			end 
		end
	end

	describe 'delete_image()' do
		let(:ie) { ie_instance() }

		context 'valid attributes' do
			it 'deletes the image' do
				image_file_name = 'test_1.jpg'
				file = class_double("File")
				
				expect(ie).to receive(:build_directory_path) { 'dir/dir' }
				expect(file).to receive(:exist?) { true }
				expect(file).to receive(:delete) { 5 }
				expect(ie.delete_image(image_file_name, file)).to eq(5)
			end 
		end

		context 'anomalies' do 
			it "raises ArgumentError if image file name does not exist" do
				file = class_double("File")
				
				expect(ie).to receive(:build_directory_path) { 'dir/dir' }
				expect(file).to receive(:exist?) { false }
				expect(ExceptionHandler).to receive(:log)
				expect {ie.delete_image('fake_image.jpg', file)}.to raise_error(ArgumentError, /.{2,}/)
			end 

			it 'raises IOError if delete fails' do
				file = class_double("File")
				
				expect(ie).to receive(:build_directory_path) { 'dir/dir' }
				expect(file).to receive(:exist?) { true }
				expect(file).to receive(:delete) { raise StandardError }
				expect(ExceptionHandler).to receive(:log)
				expect {ie.delete_image('fake_image.jpg', file)}.to raise_error(IOError, /.{2,}/)
			end
		end
	end

end
