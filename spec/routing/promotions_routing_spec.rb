require "rails_helper"

RSpec.describe PromotionsController, type: :routing do
	describe "routing" do
		it "routes to #index" do
			expect(:get => "promociones").to route_to("promotions#index")
		end

		it "routes to #show" do
			expect(:get => "promociones/1").to route_to("promotions#show", id: "1")
		end

		it "routes to #claim" do
			expect(:post => "promociones/1").to route_to("promotions#claim", id: "1")
		end

		it "routes to #winner" do
			expect(:get => "promociones/1/ganador").to route_to("promotions#winner", id: "1")
		end
	end
end
