require "rails_helper"

RSpec.describe RegistrationController, type: :routing do
	describe "routing" do
		it "routes to #new" do
			expect(:get => "registro/nuevo").to route_to("registration#new")
		end

		it "routes to #create" do
			expect(:post => "registro/crear").to route_to("registration#create")
		end

		it "routes to #success" do
			expect(:get => "registro/exito").to route_to("registration#success")
		end

		it "routes to #survey" do
			expect(:post => "registro/encuesta").to route_to("registration#survey")
		end
	end
end
