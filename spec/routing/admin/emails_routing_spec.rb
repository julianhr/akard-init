require "rails_helper"

RSpec.describe Admin::EmailsController, type: :routing do
	describe "routing" do

		it "routes to #index" do
			expect(:get => "/admin/emails").to route_to("admin/emails#index")
		end

		it "routes to #new" do
			expect(:get => "/admin/emails/new").to route_to("admin/emails#new")
		end

		it "routes to #show" do
			expect(:get => "/admin/emails/1").to route_to("admin/emails#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "/admin/emails/1/edit").to route_to("admin/emails#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "/admin/emails").to route_to("admin/emails#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "/admin/emails/1").to route_to("admin/emails#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "/admin/emails/1").to route_to("admin/emails#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "/admin/emails/1").to route_to("admin/emails#destroy", :id => "1")
		end

	end
end
