require "rails_helper"

RSpec.describe Admin::BusinessesController, type: :routing do
	describe "routing" do

		it "routes to #index" do
			expect(:get => "/admin/businesses").to route_to("admin/businesses#index")
		end

		it "routes to #new" do
			expect(:get => "/admin/businesses/new").to route_to("admin/businesses#new")
		end

		it "routes to #show" do
			expect(:get => "/admin/businesses/1").to route_to("admin/businesses#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "/admin/businesses/1/edit").to route_to("admin/businesses#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "/admin/businesses").to route_to("admin/businesses#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "/admin/businesses/1").to route_to("admin/businesses#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "/admin/businesses/1").to route_to("admin/businesses#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "/admin/businesses/1").to route_to("admin/businesses#destroy", :id => "1")
		end

		it "routes to #upload_logo" do
			expect(:post => "/admin/businesses/1/upload_logo").to route_to("admin/businesses#upload_logo", :id => "1")
		end

		it "routes to #delete_logo" do
			expect(:delete => "/admin/businesses/1/delete_logo").to route_to("admin/businesses#delete_logo", :id => "1")
		end

	end
end
