require "rails_helper"

RSpec.describe Admin::CouponWinnersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/coupon_winners").to route_to("admin/coupon_winners#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/coupon_winners/new").to route_to("admin/coupon_winners#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/coupon_winners/1").to route_to("admin/coupon_winners#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/coupon_winners/1/edit").to route_to("admin/coupon_winners#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/coupon_winners").to route_to("admin/coupon_winners#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/coupon_winners/1").to route_to("admin/coupon_winners#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/coupon_winners/1").to route_to("admin/coupon_winners#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/coupon_winners/1").to route_to("admin/coupon_winners#destroy", :id => "1")
    end

  end
end
