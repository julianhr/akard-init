require "rails_helper"

RSpec.describe Admin::SurveysDispensedController, type: :routing do
	describe "routing" do

		it "routes to #index" do
			expect(:get => "admin/surveys_dispensed")
				.to route_to("admin/surveys_dispensed#index")
		end

		it "routes to #new" do
			expect(:get => "admin/surveys_dispensed/new")
				.to route_to("admin/surveys_dispensed#new")
		end

		it "routes to #show" do
			expect(:get => "admin/surveys_dispensed/1")
				.to route_to("admin/surveys_dispensed#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "admin/surveys_dispensed/1/edit")
				.to route_to("admin/surveys_dispensed#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "admin/surveys_dispensed")
				.to route_to("admin/surveys_dispensed#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "admin/surveys_dispensed/1")
				.to route_to("admin/surveys_dispensed#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "admin/surveys_dispensed/1")
				.to route_to("admin/surveys_dispensed#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "admin/surveys_dispensed/1")
				.to route_to("admin/surveys_dispensed#destroy", :id => "1")
		end

	end
end
