require "rails_helper"

RSpec.describe Admin::EmailsSentController, type: :routing do
	describe "routing" do

		it "routes to #index" do
			expect(:get => "admin/emails_sent").to route_to("admin/emails_sent#index")
		end

		it "routes to #new" do
			expect(:get => "admin/emails_sent/new").to route_to("admin/emails_sent#new")
		end

		it "routes to #show" do
			expect(:get => "admin/emails_sent/1").to route_to("admin/emails_sent#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "admin/emails_sent/1/edit").to route_to("admin/emails_sent#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "admin/emails_sent").to route_to("admin/emails_sent#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "admin/emails_sent/1").to route_to("admin/emails_sent#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "admin/emails_sent/1").to route_to("admin/emails_sent#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "admin/emails_sent/1").to route_to("admin/emails_sent#destroy", :id => "1")
		end

	end
end
