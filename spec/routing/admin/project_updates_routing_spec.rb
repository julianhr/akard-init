require "rails_helper"

RSpec.describe Admin::ProjectUpdatesController, type: :routing do
	it "routes to #index" do
		expect(:get => "/admin/project_updates").to route_to("admin/project_updates#index")
	end

	it "routes to #new" do
		expect(:get => "/admin/project_updates/new").to route_to("admin/project_updates#new")
	end

	it "routes to #show" do
		expect(:get => "/admin/project_updates/1").to route_to("admin/project_updates#show", :id => "1")
	end

	it "routes to #edit" do
		expect(:get => "/admin/project_updates/1/edit").to route_to("admin/project_updates#edit", :id => "1")
	end

	it "routes to #create" do
		expect(:post => "/admin/project_updates").to route_to("admin/project_updates#create")
	end

	it "routes to #update via PUT" do
		expect(:put => "/admin/project_updates/1").to route_to("admin/project_updates#update", :id => "1")
	end

	it "routes to #update via PATCH" do
		expect(:patch => "/admin/project_updates/1").to route_to("admin/project_updates#update", :id => "1")
	end

	it "routes to #destroy" do
		expect(:delete => "/admin/project_updates/1").to route_to("admin/project_updates#destroy", :id => "1")
	end
end
