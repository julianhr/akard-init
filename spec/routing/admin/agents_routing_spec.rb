require "rails_helper"

RSpec.describe Admin::AgentsController, type: :routing do
	describe "routing" do
		it "routes to #index" do
			expect(:get => "admin/agents").to route_to("admin/agents#index")
		end

		it "routes to #new" do
			expect(:get => "admin/agents/new").not_to route_to("admin/agents#new")
		end

		it "routes to #show" do
			expect(:get => "admin/agents/1").to route_to("admin/agents#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "admin/agents/1/edit").to route_to("admin/agents#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "admin/agents").not_to route_to("admin/agents#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "admin/agents/1").to route_to("admin/agents#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "admin/agents/1").to route_to("admin/agents#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "admin/agents/1").to route_to("admin/agents#destroy", :id => "1")
		end
	end
end
