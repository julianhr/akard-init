require "rails_helper"

RSpec.describe Admin::BusinessBranchesController, type: :routing do
	describe "routing" do

		it "routes to #index" do
			expect(:get => "/admin/business_branches").to route_to("admin/business_branches#index")
		end

		it "routes to #new" do
			expect(:get => "/admin/business_branches/new").to route_to("admin/business_branches#new")
		end

		it "routes to #show" do
			expect(:get => "/admin/business_branches/1").to route_to("admin/business_branches#show", :id => "1")
		end

		it "routes to #edit" do
			expect(:get => "/admin/business_branches/1/edit").to route_to("admin/business_branches#edit", :id => "1")
		end

		it "routes to #create" do
			expect(:post => "/admin/business_branches").to route_to("admin/business_branches#create")
		end

		it "routes to #update via PUT" do
			expect(:put => "/admin/business_branches/1").to route_to("admin/business_branches#update", :id => "1")
		end

		it "routes to #update via PATCH" do
			expect(:patch => "/admin/business_branches/1").to route_to("admin/business_branches#update", :id => "1")
		end

		it "routes to #destroy" do
			expect(:delete => "/admin/business_branches/1").to route_to("admin/business_branches#destroy", :id => "1")
		end

	end
end
