# Preview all emails at http://localhost:3000/rails/mailers/mailer_general
class MailerGeneralPreview < ActionMailer::Preview

	# Preview this email at http://localhost:3000/rails/mailers/mailer_general/user_registration
	def user_registration
		es = Admin::EmailSent.first
		MailerGeneral.user_registration(
			es.user_id, 
			Admin::Email.where(purpose: 'registro_exito').first,
			es.id
		)
	end

	# Preview this email at http://localhost:3000/rails/mailers/mailer_general/user_coupon_winner
	def user_coupon_winner
		es = Admin::EmailSent.where(email_id: 2).first
		MailerGeneral.user_coupon_winner(
			es.user_id, 
			Admin::Email.where(purpose: 'usuario_gano_cupon').first,
			es.id,
			Admin::Coupon.first.id
		)
	end

end
