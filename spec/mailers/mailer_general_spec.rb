require "rails_helper"
require 'support/model_attributes'

RSpec.describe MailerGeneral, type: :mailer do
	include_context "model_attributes"

	fixtures :emails_sent

	let(:new_msg) { Message.new message_valid_attributes }

	describe 'internal' do
		context 'valid params' do 
			it 'sends a message' do
				aws_email = 'success@simulator.amazonses.com'
				new_msg.email = aws_email

				# deliver_now will not use AWS since ENV is not setup in Test environment
				email = MailerGeneral.internal(new_msg, :contact).deliver_now

				expect(ActionMailer::Base.deliveries.empty?).to be false
				expect(email.from.count).to eq 1
				expect(email.from.first).to eq(MailerGeneral::SENDERS[:internal])
				expect(email.to.first).to eq(MailerGeneral::RECIPIENTS[:contact])
				expect(email.reply_to.count).to eq 1
				expect(email.reply_to.first).to eq(aws_email)
				expect(email.subject).to eq(message_valid_attributes[:subject])
				expect(email.body.to_s).to eq(message_valid_attributes[:body])
			end
		end

		context 'invalid arguments' do 
			it 'returns nil if msg is not of type Message' do
				expect(ExceptionHandler).to receive(:log)
				email = MailerGeneral.internal(Object.new, :contact).deliver_now
				expect(email).to be_nil
			end

			it 'returns nil if recipient does not respond to #to_sym' do
				expect(ExceptionHandler).to receive(:log)
				email = MailerGeneral.internal(new_msg, Object.new).deliver_now
				expect(email).to be_nil
			end

			it 'returns nil if recipient is not in RECIPIENTS' do
				expect(ExceptionHandler).to receive(:log)
				email = MailerGeneral.internal(new_msg, :invalid_arg).deliver_now
				expect(email).to be_nil
			end
		end
	end

	describe 'user_registration', skip: true do
		let(:email_sent) { emails_sent(:registration_successful) }
		let(:user) { email_sent.user }
		let(:email_template) { Admin::Email.where(purpose: 'registro_exito').first }
		let(:sender) { 'akard@akard.club' }

		let(:mail) do
			MailerGeneral.user_registration(
				user.id, 
				email_template.id,
				email_sent.id
			)
		end

		before(:example) { ActionMailer::Base.deliveries.clear }

		it "renders the headers" do
			expect(mail.subject).to eq(email_template.subject)
			expect(mail.to).to eq([user.email])
			expect(mail.from).to eq([sender])
		end

		it "renders the body" do
			expect(mail.body.encoded).to include('Akard')
		end
	end

	describe 'user_coupon_winner' do
		pending
	end

end
