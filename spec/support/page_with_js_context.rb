=begin
	This context creates the global variable $page_with_js and assigns it
	the HTML resulting for visiting a page with JavaScript enabled.

	The purpose for the global variable is so that the tests run faster by
	vising the target page with Capybara only once, and making the resulting
	HTML available to all tests in the suite.
=end

RSpec.shared_context 'page_with_js_context' do

	include Capybara::DSL

	fixtures :users

	let(:capybara_page) do 
		visit new_user_session_path
		fill_in 'Email', with: '321@noemail.com' # from fixtures
		fill_in 'Password', with: '123123' # from fixtures
		click_on 'Log in'
		# Define a let(:page_in_test) in the suite with the helper path
		# to the page that requires to be rendered
		visit page_in_test
	end

	shared_examples 'page_with_js' do
		it 'page capybara page with js enabled to global variable', js: true do
			capybara_page
			$page_with_js = page.html
			# use the global variable $page_with_js to access the rendered page
			expect($page_with_js).to have_css('html.js')
		end
	end

	# Add the following to the suite where this context is needed
	# context 'set up page_with_js_context' do 
	# 	let(:page_in_test) { products_path }
	# 	it_behaves_like 'page_with_js'
	# end

	# You may want to set the global variable created to nil by inserting
	# the following in the suite
	# 	after(:all) { $page_with_js = nil }

end
