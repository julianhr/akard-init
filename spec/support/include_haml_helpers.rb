RSpec.shared_context 'include_haml_helpers' do
	include Haml
	include Haml::Helpers

	before(:example) { init_haml_helpers }
end
