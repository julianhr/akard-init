RSpec.shared_context 'fake_view_call_context' do

	include ActionView::TestCase::Behavior
	include Capybara::RSpecMatchers
	
	def view_setup
		init_haml_helpers

		# fake haml to behave as if called from view
		view.class.__send__(:attr_accessor, "haml_buffer")
		view.haml_buffer = @haml_buffer
	end

end
