def error_msg_invalid_opt(error_value)
	error_msg = ErrorMsg::MSG[:invalid_option]
	error_msg.sub!('%{value}', "#{error_value}")
	return error_msg
end

def set_flash(key, value) 
	flash_hash = ActionDispatch::Flash::FlashHash.new
	flash_hash[key.to_sym] = value
	session['flash'] = flash_hash.to_session_value
end

RSpec.shared_examples "controller_tests" do
	context 'Not signed in' do
		it 'returns redirected if not signed in' do
			sign_out agents(:admin_julian)
			get :index
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to('/agents/acceder')
		end
	end
end

RSpec.shared_examples "record_foreign_id" do |foreign_id_col, can_be_empty|
	# let(:new_record) {  }
	# let(:fixture_id) {  }

	let(:error_msg) { ErrorMsg::MSG }

	context 'valid attributes' do 
		it 'accepts a well-formed entry' do
			new_record[foreign_id_col] = fixture_id
			expect(new_record).to be_valid
		end
	end

	context 'invalid attributes' do 
		it 'cannot be empty', unless: can_be_empty do
			new_record[foreign_id_col] = ''
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[foreign_id_col]).to include(error_msg[:empty_field])
		end

		it 'cannot be a string' do
			new_record[foreign_id_col] = 'abc'
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[foreign_id_col]).to include(error_msg[:number_digits_positive])
		end

		it 'has a decimal part' do
			new_record[foreign_id_col] = 123.567
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[foreign_id_col]).to include(error_msg[:number_digits_positive])
		end
	end
end

RSpec.shared_examples "accepts_string" do |attribute, can_be_empty|
	# let(:new_record) {  }
	# let(:test_string) {  }

	context 'valid attributes' do 
		it 'accepts a well-formed entry' do
			new_record[attribute] = test_string
			expect(new_record).to be_valid
		end

		it 'can be empty', if: can_be_empty do
			new_record[attribute] = ''
			expect(new_record).to be_valid
		end
	end

	context 'invalid attributes' do 
		it 'cannot be empty', unless: can_be_empty do
			new_record[attribute] = ''
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg[:empty_field])
		end
	end
end

RSpec.shared_examples "email_tests" do |attribute|
	# let(:new_record) {  }
	# let(:error_msg_email_missing) {  }

	let(:error_msg) { ErrorMsg::MSG }
	let(:f_bacon) { FFaker::BaconIpsum }

	context 'valid attributes' do 
		it 'accepts a well-formed entry' do
			new_record[attribute] = '123@email.com'
			expect(new_record).to be_valid
		end
	end

	context 'invalid attributes' do 
		it 'cannot be empty' do
			new_record[attribute] = ''
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg_email_missing)
		end

		it 'cannot be nil' do
			new_record[attribute] = nil
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg_email_missing)
		end

		it 'cannot be > 254 characters in length' do
			new_record[attribute] = '@email.com'
			new_record[attribute].prepend f_bacon.characters(255 - new_record.email.length)
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg[:email_too_long])
		end

		it 'rejects strings' do
			new_record.email = 'this is a string'
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg[:email_invalid])
		end

		it 'rejects email address without @' do
			new_record.email = 'myemail.com'
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg[:email_invalid])
		end

		it 'rejects email address without domain extension' do
			new_record.email = 'myemail@email'
			expect(new_record).to be_invalid
			expect(new_record.errors.messages[attribute]).to include(error_msg[:email_invalid])
		end
	end
end
