RSpec.shared_context "model_attributes" do

	# Admin::Agents
	let(:admin_agent_valid_attributes) do
		{
			first_name: 'John',
			last_name: 'Smith',
			email: '123@email.com',
			encrypted_password: Agent.new.send(:password_digest, '123456789')
		}
	end
	let(:admin_agent_invalid_attributes) { {email: 'invalid email'} }

	# Admin::Business
	let(:business_valid_attributes) do
		{
			name: 'My Print Shop',
			line_of_business: 'Printing',
			url: 'http://www.myprintshop.com',
			description: 'We cover all your print needs.',
			metadata: {info: 'more info'}
		}
	end
	let(:business_invalid_attributes) do
		{
			name: '',
			url: 'my_invalid_url'
		}
	end


	# Admin::BusinessBranch
	let(:business_branch_valid_attributes) do
		{
			business_id: businesses(:restaurant).id,
			type_of_branch: 'sucursal',
			street_name: 'Avenida Vallarta',
			street_no: 123456,
			inner_no: '50B',
			street_context: 'Esquina con Enrique Díaz',
			borough: 'Americana',
			city: 'Zapopan',
			zip: 12345,
			state_id: states(:jalisco).id,
			latitude: 20.6744508,
			longitude: -103.3730668
		}
	end
	let(:business_branch_invalid_attributes) do
		{
			type_of_branch: 'invalid_type',
			street_name: ''
		}
	end


	# Admin::Coupons
	let(:coupon_valid_attributes) do
		{
			business_id: 					businesses(:restaurant).id,
			business_branch_id: 			business_branches(:americas).id,
			type_of_coupon: 				Admin::Coupon::TYPE_OF_COUPON_OPT.first,
			publish_active: 				true,
			publish_start_datetime: 	(DateTime.now - 2.days),
			publish_end_datetime: 		(DateTime.now - 2.days) + 5.days,
			valid_start_datetime: 		(DateTime.now - 2.days),
			valid_end_datetime: 			(DateTime.now - 2.days) + 2.months,
			exchanged_datetime: 			(DateTime.now - 2.days) + 10.days,
			status: 							'All correct',
			title: 							'This is the coupon title.',
			description: 					'This is the coupon description.',
			price_regular: 				599.99,
			price_promo: 					499.99,
			quantity: 						10,
			available: 						5,
			conditions: 					'These are the coupon conditions.'
		}
	end
	let(:coupon_invalid_attributes) { {type_of_coupon: 'invalid option'} }


	#Admin::CouponWinners
	let(:coupon_winner_valid_attributes) do
		{
			coupon_id: coupons(:sushi).id,
			user_id: users(:pedro).id,
			token: 'ABCDEFGHOJKLMN',
			email_sent_id: emails_sent(:coupon_sushi).id,
			status: Admin::CouponWinner::STATUS_OPT.first,
			redeemed_at: Time.now,
			metadata: {user: 'user information'}
		}
	end
	let(:coupon_winner_invalid_attributes) do
		{
			status: 'invalid status'
		}
	end


	# Admin::Email
	let(:email_valid_attributes) do
		{
			model: Admin::Email::MODEL_OPT.last,
			purpose: 'test purpose string',
			subject: 'Enjoy 2x1 on all beer.',
			template_name: 'template_1'
		}
	end
	let(:email_invalid_attributes) do
		{
			model: 'new_model',
			purpose: 'new_purpose',
		}
	end


	# Admin::EmailSent
	let(:email_sent_valid_attributes) do
		{
			email_id: emails(:registration).id,
			user_id: users(:pedro).id,
			sent: true,
			status: Admin::EmailSent::STATUS_OPT.keys.first.to_s,
			attempts: 1,
			response: '{"status":"successful"}'
		}
	end
	let(:email_sent_invalid_attributes) { {status: 'invalid status'} }

	# Admin::ProjectUpdate
	let(:project_update_valid_attributes) do
		{
			user_id: users(:pedro).id,
			email_status: Admin::ProjectUpdate::EMAIL_STATUS_OPT.first,
			metadata: {test: 'testing test'}
		}
	end
	let(:project_update_invalid_attributes) { {user_id: nil} }

	# Admin::State
	let(:state_valid_attributes) { {name: 'New State'} }
	let(:state_invalid_attributes) { {name: ''} }

	# Admin::Survey
	let(:survey_valid_attributes) do
		{
			purpose: 'signup_success',
			audience: 'users',
			template_name: '_survey_3',
		}
	end
	let(:survey_invalid_attributes) { {audience: 'everyone'} }


	# Admin::SurveysDispensed
	let(:survey_dispensed_valid_attributes) do
		{
			survey_id: surveys(:signup).id,
			user_id: users(:pedro).id,
			where: 'registration_success',
			response: '{"response":"success"}'
		}
	end
	let(:survey_dispensed_invalid_attributes) { {where: ''} }

	# Agents
	let(:agent_valid_attributes) do
		{
			first_name: 'John',
			last_name: 'Smith',
			email: '123@email.com',
			password: '123456789'
		}
	end
	let(:agent_invalid_attributes) { {email: 'invalid email'} }

	# Message
	let(:message_valid_attributes) do
		{
			name: 'John Smith',
			email: 'julian1@cimarron.me',
			subject: 'Testing the subject line',
			body: 'Testing the body message'
		}
	end
	let(:message_invalid_attributes) { {email: 'invalid_email'} }

	# Users
	let(:user_valid_attributes) do
		{
			email: '123@email.com'
		}
	end
	let(:user_invalid_attributes) { {email: 'invalid email'} }

end
