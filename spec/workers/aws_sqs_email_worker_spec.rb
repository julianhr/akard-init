require 'rails_helper'

RSpec.describe AwsSqsEmailWorker do
	fixtures :emails

	f_bacon = FFaker::BaconIpsum

	let(:delivery_json) { '{"Type" : "Notification","MessageId" : "effbeef5-a638-5032-a678-a1f1a9018fc0","TopicArn" : "arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent","Message" : "{\"notificationType\":\"Delivery\",\"delivery\":{\"timestamp\":\"2016-01-04T06:11:05.317Z\",\"processingTimeMillis\":8998,\"recipients\":[\"julian1@cimarron.me\"],\"smtpResponse\":\"250 OK id=1aFyM6-0005Aj-1O\",\"reportingMTA\":\"a27-30.smtp-out.us-west-2.amazonses.com\"},\"mail\":{\"timestamp\":\"2016-01-04T06:10:56.319Z\",\"source\":\"akard@akardcupones.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:274732928163:identity/akardcupones.com\",\"sendingAccountId\":\"274732928163\",\"messageId\":\"000001520b428abf-b7012cda-fe4f-4dad-b8ee-59f139d55e85-000000\",\"destination\":[\"julian1@cimarron.me\"]}}","Timestamp" : "2016-01-04T06:11:05.434Z","SignatureVersion" : "1","Signature" : "UqYh1x+AYGvqGUXZ6DTdd1SPpAG7Tg3GnaEq9sboabkYytaytVAFp1xz+4ar2DFCe/tMLup+TlVnLnTUjtUan8pThs0RGfiy6hZe9+5mNQG+rD1HaQSxvHnHme/p7/MGHzbhydK5LvJ3IO00obwQGK1bSshtVGO00synp+bOlujvxalNvVk/GQfVvzDaPfIVlENv72uy8HqaJX/UBgNKzNNjyMYwxe2kJVFr6KxoExLGlweCjV867819wmtL2iUUtM7d4dKdZXH4M0hYH9YDgRrE3u7Hb5b5bcoWsx/r6sJlcuV7/K7IZEQ7KVd5r9sg/osajVMxIWCoB/YWZw1jXg==","SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-bb750dd426d95ee9390147a5624348ee.pem","UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:274732928163:sns_prelaunch_email_sent:a12c6a41-d8f2-46ed-b022-b51ac81ee686"}' }

	describe '#perform' do
		let(:sqs_msg_double) { double "SqsMsg" }
		let(:worker) { AwsSqsEmailWorker.new }

		before(:example) do
			allow(sqs_msg_double).to receive(:delete) { true }
			allow_any_instance_of(EmailSqsUpdater).to receive(:update) { true }
		end

		context 'valid arguments' do 
			it 'calls EmailSqsUpdater#update' do
				expect_any_instance_of(EmailSqsUpdater).to receive(:update)
				worker.perform(sqs_msg_double, delivery_json)
			end

			it 'calls SqsMsg#delete' do
				expect(sqs_msg_double).to receive(:delete)
				worker.perform(sqs_msg_double, delivery_json)
			end
		end

		context 'anomalies' do 
			it 'rescues exception and logs it if EmailSqsUpdater#update fails' do
				allow_any_instance_of(EmailSqsUpdater).to receive(:update) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				worker.perform(sqs_msg_double, delivery_json)
			end

			it 'rescues exception and logs it if EmailSqsUpdater#update fails' do
				allow(sqs_msg_double).to receive(:delete) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				worker.perform(sqs_msg_double, delivery_json)
			end
		end
	end

end
