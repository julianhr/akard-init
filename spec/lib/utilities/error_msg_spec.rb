require 'rails_helper'

RSpec.describe "lib/utilities/error_msg.rb" do

	describe 'MSG' do
		it 'is a hash' do
			expect(ErrorMsg::MSG).to be_a(Hash)
		end

		it 'has many messages' do
			expect(ErrorMsg::MSG.count).to be > 1
		end

		it 'has keys that are symbols' do
			expect(ErrorMsg::MSG.keys.sample).to be_a(Symbol)
		end

		it 'has values that are strings' do
			expect(ErrorMsg::MSG.values.sample).to be_a(String)
		end
	end

end
