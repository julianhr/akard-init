FactoryGirl.define do

	factory :admin_julian, class: Agent do
		first_name 'Julian'
		last_name 'Hernandez'
		email 'admin@email.com'
		password '123456789'
		approved true
		confirmed_at Time.now
	end

end
