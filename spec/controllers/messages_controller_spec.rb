require 'rails_helper'
require 'support/model_attributes'

RSpec.describe MessagesController, type: :controller do
	include_context "model_attributes"

	let(:http_referrer) { root_url(testing: 'testing_text') }

	describe "GET #new" do
		it "assigns a new Message as @message" do
			get :new
			expect(assigns(:message)).to be_a(Message)
		end

		it "assigns false to @message_sent if no params[sent]" do
			get :new
			expect(assigns(:message_sent)).to be false
		end

		it "assigns @message_sent=true if params[sent]=true" do
			get :new, {sent: 'true'}
			expect(assigns(:message_sent)).to be true
		end
	end

	describe "POST #create" do
		before(:example) do
			allow_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_now) { true }
		end

		context 'valid params' do 
			it 'assigns built message as @message' do
				post :create, {message: message_valid_attributes}
				expect(assigns(:message)).to be_a(Message)
				expect(assigns(:message).email).to eq(message_valid_attributes[:email])
			end

			it 'calls #deliver_now on email' do
				expect_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_now)
				post :create, {message: message_valid_attributes}
			end

			it "if everything checks out, redirects to new with query" do
				post :create, {message: message_valid_attributes}
				url_query = {sent: true}
				expect(response).to redirect_to contact_url(url_query)
			end
		end

		context 'invalid params' do 
			it "renders new if message is invalid" do
				post :create, {message: message_invalid_attributes}
				expect(response).to render_template(:new)
			end

			it "adds message to flash if message is invalid" do
				post :create, {message: message_invalid_attributes}
				expect(flash[:alert]).not_to be_empty
			end
		end

		context 'anomalies' do 
			before(:example) { request.env["HTTP_REFERER"] = http_referrer }

			it 'if email delivery raises error, system logs the error' do
				allow_any_instance_of(ActionMailer::MessageDelivery)
					.to receive(:deliver_now) {raise Exception}
				expect(ExceptionHandler).to receive(:log)
				post :create, {message: message_valid_attributes}
			end

			it 'if email delivery raises error, user redirected to back' do
				allow_any_instance_of(ActionMailer::MessageDelivery)
					.to receive(:deliver_now) {raise Exception}
				post :create, {message: message_valid_attributes}
				expect(response).to redirect_to http_referrer
			end
		end
	end

end
