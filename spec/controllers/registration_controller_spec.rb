require 'rails_helper'
require 'support/shared_tests'

RSpec.describe RegistrationController, type: :controller do

	fixtures :users
	fixtures :surveys_dispensed
	fixtures :surveys
	fixtures :emails
	fixtures :emails_sent

	let(:user_fixture) { users(:pedro) }
	let(:http_referrer) { root_url(query: 'test query') }

	before(:example) { request.env["HTTP_REFERER"] = http_referrer }

	describe "GET #new" do
		specify { get :new; expect(assigns(:user)).to be_a_new(User) }

		it 'assigns @errors = {} if no flash[new_user_errors]' do
			get :new
			expect(assigns(:errors)).to eq( {} )
		end

		it 'assigns errors hash to @errors if flash[new_user_errors]' do
			errors_hash = {email: 'test text'}
			set_flash(:new_user_errors, errors_hash)
			get :new
			expect(assigns(:errors)).to eq( errors_hash )
		end
	end

	describe "POST #create" do
		let(:valid_attributes) { {email: '123@email.com'} }
		let(:invalid_attributes) { {email: '123'} }

		before(:each) do 
			mail_message_double = instance_double("Mail::Message")
			allow(mail_message_double).to receive(:errors) { [] }
			allow_any_instance_of(ActionMailer::MessageDelivery)
				.to receive(:deliver_later) { mail_message_double }
		end

		context "valid params" do
			it "assigns a newly created User as @user" do
				post :create, {:user => valid_attributes}
				expect(assigns(:user)).to be_a(User)
				expect(assigns(:user)).to be_persisted
			end

			it "creates a newly created User" do
				expect {
					post :create, {:user => valid_attributes}
				}.to change(User, :count).by(1)
			end

			it 'strips email address from params for @user' do
				attributes = {email: ' 123@email.com '}
				post :create, {:user => attributes}
				expect(assigns(:user)).to be_a(User)
				expect(assigns(:user)).to be_persisted
			end

			it "assigns a newly created ProjectUpdate as @project_update" do
				post :create, {:user => valid_attributes}
				expect(assigns(:project_update)).to be_an(Admin::ProjectUpdate)
				expect(assigns(:project_update)).to be_persisted
			end

			it "creates a new ProjectUpdate" do
				expect {
					post :create, {:user => valid_attributes}
				}.to change(Admin::ProjectUpdate, :count).by(1)
			end

			it "assigns a newly created SurveysDispensed as @survey" do
				post :create, {:user => valid_attributes}
				expect(assigns(:survey)).to be_a(Admin::SurveysDispensed)
				expect(assigns(:survey)).to be_persisted
			end

			it "creates a new SurveysDispensed" do
				expect {
					post :create, {:user => valid_attributes}
				}.to change(Admin::SurveysDispensed, :count).by(1)
			end

			it "assigns email as @email" do
				post :create, {:user => valid_attributes}
				expect(assigns(:email)).to be_a(Admin::Email)
				expect(assigns(:email)).to be_persisted
			end

			it "assigns a newly created EmailSent as @email_sent" do
				post :create, {:user => valid_attributes}
				expect(assigns(:email_sent)).to be_a(Admin::EmailSent)
				expect(assigns(:email_sent)).to be_persisted
			end

			it "creates a new EmailSent" do
				expect {
					post :create, {:user => valid_attributes}
				}.to change(Admin::EmailSent, :count).by(1)
			end

			it 'queues up @email_sent for asynchroneous delivery' do
				allow_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_later) { true }
				post :create, {:user => valid_attributes}					
			end

			it "redirects to the successful registration page" do
				post :create, {:user => valid_attributes}
				user = User.last
				opt_h = {email: user.email, id: user.id}
				expect(response).to redirect_to( registro_exito_path(opt_h) )
			end
		end

		context "invalid params" do
			it "redirects to registration if @user is invalid" do
				post :create, {:user => invalid_attributes}
				expect(assigns(:user)).not_to be_persisted
				expect(flash[:new_user_errors]).to be_an(ActiveModel::Errors)
				expect(response).to redirect_to(root_url)
			end

			it "assigns @survey to nil" do
				post :create, {:user => invalid_attributes}
				expect(assigns(:survey)).to be_nil
			end

			it "assigns @email_sent to nil" do
				post :create, {:user => invalid_attributes}
				expect(assigns(:email_sent)).to be_nil
			end

			it "redirects to root if email is not well-formed" do
				post :create, {:user => invalid_attributes}
				expect(response).to redirect_to(root_url)
			end
		end

		context 'anomalies' do 
			it "redirects to root if @user.save! raises exception" do
				allow_any_instance_of(User).to receive(:save!) { raise Exception }

				expect(ExceptionHandler).to receive(:log)
				post :create, {:user => valid_attributes}
				expect(assigns(:user)).not_to be_persisted
				expect(response).to redirect_to(root_url)
				expect(flash[:alert]).to be_a(String)
			end

			it "redirects to registration if @project_update.save! raises exception" do
				allow_any_instance_of(Admin::ProjectUpdate).to receive(:save!) { raise Exception }

				expect(ExceptionHandler).to receive(:log)
				post :create, {:user => valid_attributes}
				expect(assigns(:project_update)).not_to be_persisted
				expect(response).to redirect_to(root_url)
				expect(flash[:notice]).to be_a(String)
			end

			it "redirects to registration if @survey.save! raises exception" do
				allow_any_instance_of(Admin::SurveysDispensed).to receive(:save!) { raise Exception }

				expect(ExceptionHandler).to receive(:log)
				post :create, {:user => valid_attributes}
				expect(assigns(:survey)).not_to be_persisted
				expect(response).to redirect_to(root_url)
				expect(flash[:notice]).to be_a(String)
			end

			it "redirects to registration if @email_sent.save! raises exception" do
				allow_any_instance_of(Admin::ProjectUpdate).to receive(:save!) { raise Exception }

				expect(ExceptionHandler).to receive(:log)
				post :create, {:user => valid_attributes}
				expect(assigns(:email_sent)).not_to be_persisted
				expect(response).to redirect_to(root_url)
				expect(flash[:notice]).to be_a(String)
			end
		end
	end

	describe "GET #success" do
		context 'valid attributes' do 
			it "assigns the requested user as @user" do
				get :success, {id: user_fixture.id, email: user_fixture.email}
				expect(assigns(:user)).to eq(user_fixture)
			end

			it "assigns @survey_dispensed to correspoding survey dispensed" do
				sd = Admin::SurveysDispensed.where(user_id: user_fixture.id).first
				get :success, {id: user_fixture.id, email: user_fixture.email}
				expect(assigns(:survey_dispensed)).to eq(sd)
			end
		end

		context 'invalid attributes' do 
			it 'redirects to root if invalid params[id]' do
				user_fixture.id = 999
				get :success, {id: user_fixture.id, email: user_fixture.email}
				expect(response).to redirect_to(root_url)
			end

			it 'redirects to root if id is empty' do
				user_fixture.id = ''
				get :success, {id: user_fixture.id, email: user_fixture.email}
				expect(response).to redirect_to(root_url)
			end

			it 'redirects to root if params[email] is empty' do
				get :success, {id: user_fixture.id, email: ''}
				expect(response).to redirect_to(root_url)
			end

			it 'redirects to root if parmas[email] does not match @user.email' do
				user_fixture.email = 'invalid@email.com'
				get :success, {id: user_fixture.id, email: user_fixture.email}
				expect(response).to redirect_to(root_url)
			end
		end
	end

	describe "POST #survey" do
		let(:sd_fixture) { surveys_dispensed(:signup) }
		let(:request_valid_attributes) do
			{
				user_id: users(:pedro).id,
				sd_id: surveys_dispensed(:signup).id,
				survey: {
					category: "laptops, electronics",
					subscribed: "Groupon, Groupalia",
					apps_use: "Seguido",
					age_range: "16 - 25",
					sex: "Hombre",
					comments: "Estoy ansioso de recibir descuentos."
				}
			}
		end

		context "valid params" do
			it "assigns corresponding User as @user" do
				post :survey, request_valid_attributes
				expect(assigns(:user)).to be_a(User)
			end

			it "assigns corresponding SurveysDispensed as @survey_dispensed" do
				post :survey, request_valid_attributes
				expect(assigns(:survey_dispensed)).to be_a(Admin::SurveysDispensed)
			end

			it "assigns survey response as @survey" do
				post :survey, request_valid_attributes
				expect(assigns(:survey)).to be_a(ActionController::Parameters)
			end

			it "assigns a SurveySignupProcessor to @ssp" do
				post :survey, request_valid_attributes
				expect(assigns(:ssp)).to be_a(SurveySignupProcessor)
			end

			it "redirects to promotions with notice message" do
				user = user_fixture
				post :survey, request_valid_attributes
				opt_h = {email: user.email, id: user.id}
				expect(response).to redirect_to(promotions_url)
				expect(flash[:referrer]).to eq('signup_survey')
			end
		end

		context "invalid params" do
			it "redirects to root if params[user_id]=nil" do
				attributes = request_valid_attributes
				attributes[:user_id] = nil

				post :survey, attributes
				expect(response).to redirect_to(root_url)
			end

			it "redirects to :back if params[sd_id] is nil" do
				attributes = request_valid_attributes
				attributes[:sd_id] = nil

				post :survey, attributes
				expect(response).to redirect_to(http_referrer)
			end

			it "redirects to :back if params[survey] is nil" do
				attributes = request_valid_attributes
				attributes[:survey] = nil

				post :survey, attributes
				expect(response).to redirect_to(http_referrer)
			end

			it "redirects to :back if params[user_id] != @survey_designated.user_id" do
				attributes = request_valid_attributes
				attributes[:user_id] = users(:maria).id

				post :survey, attributes
				expect(response).to redirect_to(http_referrer)
			end
		end

		context 'anomalies' do 
			it 'redirects to :back if @ssp.verify_responses() raises TypeError' do
				allow_any_instance_of(SurveySignupProcessor)
					.to receive(:verify_responses) { raise TypeError }
				expect(ExceptionHandler).to receive(:log)
				post :survey, request_valid_attributes
				expect(response).to redirect_to(http_referrer)
			end

			it 'redirects to :back if @ssp.verify_responses() raises IndexError' do
				allow_any_instance_of(SurveySignupProcessor)
					.to receive(:verify_responses) { raise IndexError }
				expect(ExceptionHandler).to receive(:log)
				post :survey, request_valid_attributes
				expect(response).to redirect_to(http_referrer)
			end

			it 'redirects to :back if @ssp.verify_responses() raises Exception' do
				allow_any_instance_of(SurveySignupProcessor)
					.to receive(:verify_responses) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				post :survey, request_valid_attributes
				expect(response).to redirect_to(http_referrer)
			end

			it 'redirects to :back if @ssp.save() raises Exception' do
				allow_any_instance_of(SurveySignupProcessor)
					.to receive(:verify_responses) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				post :survey, request_valid_attributes
				expect(response).to redirect_to(http_referrer)
			end
		end
	end

end
