require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

  describe "GET #faq" do
    it "returns http success" do
      get :faq
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #site_terms" do
    it "returns http success" do
      get :site_terms
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #about" do
    it "returns http success" do
      get :about
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #instructions" do
    it "returns http success" do
      get :instructions
      expect(response).to have_http_status(:success)
    end
  end

end
