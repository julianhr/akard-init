require 'rails_helper'

RSpec.describe DeliverEmail do

	fixtures :users
	fixtures :emails
	fixtures :emails_sent

	before(:example) do
		class FakeController < ApplicationController
			include DeliverEmail
		end
	end

	after(:example) { Object.send :remove_const, :FakeController }

	let(:user) { users(:pedro) }
	let(:email) { emails(:coupon_winner) }
	let(:email_sent) { emails_sent(:coupon_winner) }
	let(:message_delivery) { MailerGeneral.user_coupon_winner(user.id, email.id, email_sent.id) }
	let(:fc) { FakeController.new }
	
	describe '#deliver_emailsent' do
		let(:delivery_job) { double(ActionMailer::DeliveryJob) }
		let(:run_method) { fc.deliver_emailsent(message_delivery, :deliver_later, email_sent) }

		before(:example) do
			allow_any_instance_of(ActionMailer::MessageDelivery)
				.to receive(:deliver_now) { delivery_job }
			allow_any_instance_of(ActionMailer::MessageDelivery)
				.to receive(:deliver_later) { delivery_job }
		end

		context 'valid arguments' do 
			before(:example) do
				allow(delivery_job).to receive(:errors) { [] }
			end
			
			it 'adds sent timestamp to email_sent[response]' do
				run_method
				expect(email_sent[:response]).to be_present
			end

			it 'sets email_sent.status = sent' do
				email_sent.status = 'staged_for_send'
				run_method
				expect(email_sent.status).to eq('sent')
			end

			it 'saves email_sent' do
				expect_any_instance_of(Admin::EmailSent).to receive(:save!)
				run_method
			end

			it 'returns true if operation successful' do
				expect(run_method).to be true
			end
		end

		context 'anomalies' do 
			before(:example) do
				allow(delivery_job).to receive(:errors) { ['there is one error'] }
			end

			it 'adds errors to email_sent[response]' do
				run_method
				expect(email_sent[:response].keys.include? :errors).to be true
			end

			it 'sets email_sent.status = errors' do
				email_sent.status = 'staged_for_send'
				run_method
				expect(email_sent.status).to eq('errors')
			end
		end
	end

end
