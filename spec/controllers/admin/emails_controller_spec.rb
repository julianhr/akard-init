require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::EmailsController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :agents
	fixtures :emails

	let(:email_fixture) { emails(:new_coupon) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_emails_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all emails as @emails" do
			emails = Admin::Email.all
			get :index
			expect(assigns(:emails).order(:id)).to eq(emails.order(:id))
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested email as @email" do
				email = email_fixture
				get :show, {:id => email.id}
				expect(assigns(:email)).to eq(email)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new email as @email" do
			get :new
			expect(assigns(:email)).to be_a_new(Admin::Email)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested email as @email" do
				email = email_fixture
				get :edit, {:id => email.id}
				expect(assigns(:email)).to eq(email)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new Email" do
				expect {
					post :create, {:admin_email => email_valid_attributes}
				}.to change(Admin::Email, :count).by(1)
			end

			it "assigns a newly created email as @email" do
				post :create, {:admin_email => email_valid_attributes}
				expect(assigns(:email)).to be_a(Admin::Email)
				expect(assigns(:email)).to be_persisted
			end

			it "redirects to the created email" do
				post :create, {:admin_email => email_valid_attributes}
				expect(response).to redirect_to(Admin::Email.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved email as @email" do
				post :create, {:admin_email => email_invalid_attributes}
				expect(assigns(:email)).to be_a_new(Admin::Email)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_email => email_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {subject: 'new subject'} }

			it "updates the requested email" do
				email = email_fixture
				put :update, {:id => email.id, :admin_email => new_attributes}
				email.reload
				expect(email.subject).to eq new_attributes[:subject]
			end

			it "assigns the requested email as @email" do
				email = email_fixture
				put :update, {:id => email.id, :admin_email => new_attributes}
				expect(assigns(:email)).to eq(email)
			end

			it "redirects to the email" do
				email = email_fixture
				put :update, {:id => email.id, :admin_email => new_attributes}
				expect(response).to redirect_to(email)
			end
		end

		context "with invalid params" do
			it "assigns the email as @email" do
				email = email_fixture
				put :update, {:id => email.id, :admin_email => email_invalid_attributes}
				expect(assigns(:email)).to eq(email)
			end

			it "re-renders the 'edit' template" do
				email = email_fixture
				put :update, {:id => email.id, :admin_email => email_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested email" do
				email = email_fixture
				email_sent = 
				expect {
					delete :destroy, {:id => email.id}
				}.to change(Admin::Email, :count).by(-1)
			end

			it "destroys fk elements in emails_sent" do
				email = email_fixture
				expect {
					delete :destroy, {:id => email.id}
				}.to change(Admin::EmailSent, :count).by(-3)
			end

			it "redirects to the emails list" do
				email = email_fixture
				delete :destroy, {:id => email.id}
				expect(response).to redirect_to(admin_emails_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
