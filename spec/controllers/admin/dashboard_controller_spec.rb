require 'rails_helper'
require 'support/shared_tests'

RSpec.describe Admin::DashboardController, type: :controller do
	include_examples "controller_tests"
	
	fixtures :agents

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"
		
	describe "GET #index" do
		it "assigns all users as @users" do
			get :index
			expect(response).to have_http_status(:success)
		end
	end

end
