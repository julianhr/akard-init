require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::ProjectUpdatesController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :project_updates
	fixtures :users
	fixtures :agents

	let(:pu_fixture) { project_updates(:can_email) }

	before(:example) { sign_in agents(:admin_julian) } 

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_project_updates_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all admin_project_updates as @admin_project_updates" do
			project_updates = Admin::ProjectUpdate.all
			get :index
			expect(assigns(:project_updates)).to eq(project_updates)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested admin_project_update as @admin_project_update" do
				get :show, {:id => pu_fixture.id}
				expect(assigns(:project_update)).to eq(pu_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new admin_project_update as @admin_project_update" do
			get :new
			expect(assigns(:project_update)).to be_a_new(Admin::ProjectUpdate)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested admin_project_update as @admin_project_update" do
				get :edit, {:id => pu_fixture.id}
				expect(assigns(:project_update)).to eq(pu_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "valid params" do
			it "creates a new Admin::ProjectUpdate" do
				expect {
					post :create, {:project_update => project_update_valid_attributes}
				}.to change(Admin::ProjectUpdate, :count).by(1)
			end

			it "assigns a newly created project_update as @project_update" do
				post :create, {:project_update => project_update_valid_attributes}
				expect(assigns(:project_update)).to be_a(Admin::ProjectUpdate)
				expect(assigns(:project_update)).to be_persisted
			end

			it "redirects to the created admin_project_update" do
				post :create, {:project_update => project_update_valid_attributes}
				expect(response).to redirect_to(Admin::ProjectUpdate.last)
			end
		end

		context "invalid params" do
			it "assigns a newly created but unsaved admin_project_update as @admin_project_update" do
				post :create, {:project_update => project_update_invalid_attributes}
				expect(assigns(:project_update)).to be_a_new(Admin::ProjectUpdate)
			end

			it "re-renders the 'new' template" do
				post :create, {:project_update => project_update_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "valid params" do
			let(:new_attributes) { {email_status: Admin::ProjectUpdate::EMAIL_STATUS_OPT.last} }

			it "updates the requested admin_project_update" do
				put :update, {:id => pu_fixture.id, :project_update => new_attributes}
				pu_fixture.reload
				expect(pu_fixture.email_status).to eq(new_attributes[:email_status])
			end

			it "assigns the requested admin_project_update as @admin_project_update" do
				put :update, {:id => pu_fixture.id, :project_update => new_attributes}
				expect(assigns(:project_update)).to eq(pu_fixture)
			end

			it "redirects to the admin_project_update" do
				put :update, {:id => pu_fixture.id, :project_update => new_attributes}
				expect(response).to redirect_to(pu_fixture)
			end
		end

		context "invalid params" do
			it "assigns the project_update as @project_update" do
				put :update, {:id => pu_fixture.id, :project_update => project_update_invalid_attributes}
				expect(assigns(:project_update)).to eq(pu_fixture)
			end

			it "re-renders the 'edit' template" do
				put :update, {:id => pu_fixture.id, :project_update => project_update_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		it "destroys the requested project_update" do
			expect {
				delete :destroy, {:id => pu_fixture.id}
			}.to change(Admin::ProjectUpdate, :count).by(-1)
		end

		it "redirects to the admin_project_updates list" do
			delete :destroy, {:id => pu_fixture.id}
			expect(response).to redirect_to(admin_project_updates_url)
		end
	end

end
