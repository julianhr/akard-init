require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::BusinessBranchesController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :business_branches
	fixtures :businesses
	fixtures :states
	fixtures :agents

	let(:branch_fixture) { business_branches(:americas) }

	def get_states_opt
		Admin::State.all.map { |state| [state.name, state.id] }
	end

	def set_referring_business_id_to_flash(business)
		flash_hash = ActionDispatch::Flash::FlashHash.new
		flash_hash[:referring_business_id] = business.id
		session['flash'] = flash_hash.to_session_value
	end

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to business_branches_url if business_branch not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_business_branches_url)
			expect(flash[:notice]).not_to be nil
		end

		it 'redirects to referring business if business_branch not found' do
			invalid_id = 999
			business = businesses(:restaurant)
			self.send verb, route, {id: invalid_id, referring_business_id: business.id}
			expect(response).to redirect_to(admin_business_url(business))
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all business_branches as @business_branches" do
			business_branches = Admin::BusinessBranch.all
			get :index
			expect(assigns(:business_branches)).to eq(business_branches)
		end
	end

	describe "GET #show" do
		it "assigns the requested business_branch as @business_branch" do
			business_branch = branch_fixture
			get :show, {:id => business_branch.id}
			expect(assigns(:business_branch)).to eq(business_branch)
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new business_branch as @business_branch" do
			get :new
			expect(assigns(:business_branch)).to be_a_new(Admin::BusinessBranch)
		end

		it "assigns business to @referring_business if params[referring_business_id]" do
			business = businesses(:restaurant)
			get :new, referring_business_id: business.id
			expect(assigns(:referring_business)).to eq(business)
		end

		it "assigns all states as @states" do
			states = get_states_opt()
			get :new
			expect(assigns(:states_opt)).to eq(states)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested business_branch as @business_branch" do
				business_branch = branch_fixture
				get :edit, {:id => business_branch.id}
				expect(assigns(:business_branch)).to eq(business_branch)
			end

			it "assigns business as @referring_business if params[referring_business_id]" do
				business_branch = branch_fixture
				business = businesses(:restaurant)
				get :edit, {:id => business_branch.id, referring_business_id: business.id}			
				expect(assigns(:referring_business)).to eq(business)
			end

			it "assigns all states as @states" do
				business_branch = branch_fixture
				states = get_states_opt()
				get :edit, {id: business_branch.id}
				expect(assigns(:states_opt)).to eq(states)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		let(:referring_business) { businesses(:restaurant) }
		let(:business_from_params) do
			Admin::Business.where(id: referring_business.id).first
		end

		context "with valid params" do
			it "creates a new BusinessBranch" do
				expect { 
					post :create, {:admin_business_branch => business_branch_valid_attributes}
				}.to change(Admin::BusinessBranch, :count).by(1)
			end

			it "assigns a newly created business_branch as @business_branch" do
				post :create, {:admin_business_branch => business_branch_valid_attributes}
				expect(assigns(:business_branch)).to be_a(Admin::BusinessBranch)
				expect(assigns(:business_branch)).to be_persisted
			end

			it "assigns business as @referring_business if flash[referring_business_id]" do
				business = business_from_params
				set_referring_business_id_to_flash(business)
				post :create, {:admin_business_branch => business_branch_valid_attributes}
				expect(assigns(:referring_business)).to eq(business)
			end

			it "redirects to the created business_branch if no flash[referring_business_id]" do
				post :create, {:admin_business_branch => business_branch_valid_attributes}
				expect(response).to redirect_to(Admin::BusinessBranch.last)
			end

			it "redirects to the referring business if flash[referring_business_id]" do
				business = business_from_params
				set_referring_business_id_to_flash(business)
				post :create, {:admin_business_branch => business_branch_valid_attributes}
				expect(response).to redirect_to(business)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved business_branch as @business_branch" do
				post :create, {:admin_business_branch => business_branch_invalid_attributes}
				expect(assigns(:business_branch)).to be_a_new(Admin::BusinessBranch)
			end

			it "renders the 'new' template" do
				post :create, {:admin_business_branch => business_branch_invalid_attributes}
				expect(response).to render_template(:new)
			end

			it "renders the 'new' template and assigns @referring_business if flash[referring_business_id]" do
				business = businesses(:restaurant)
				set_referring_business_id_to_flash(business)
				invalid_attributes = business_branch_invalid_attributes
				invalid_attributes[:business_id] = business.id

				post :create, {:admin_business_branch => invalid_attributes}

				expect(response).to render_template(:new)
				expect(assigns(:referring_business)).to eq(business)
			end

			it "redirects to businesses if referring_business and business_branch.business don't match" do
				business = businesses(:dentist)
				set_referring_business_id_to_flash(business)

				post :create, {:admin_business_branch => business_branch_valid_attributes}
				expect(response).to redirect_to(admin_businesses_url)
			end
		end

		context 'anomalies' do 
			context 'business_branch.save! raises unkown error' do 
				before(:example) do
					bb_instance = Admin::BusinessBranch.new business_branch_valid_attributes
					allow(Admin::BusinessBranch).to receive(:new) { bb_instance }
					allow(bb_instance).to receive(:save!) { raise IOError, "Test Save" }
				end

				it 'renders edit template' do
					post :create, {:admin_business_branch => business_branch_valid_attributes}
					expect(response).to render_template("new")
				end

				it 'assigns bussiness as @business if flash[referring_busienss_id]' do
					business = businesses(:restaurant)
					set_referring_business_id_to_flash(business)
					post :create, {:admin_business_branch => business_branch_valid_attributes}
					expect(assigns(:referring_business)).to eq(business)
				end
				
				it 'assigns a newly created but unsaved business_branch as @business_branch' do
					post :create, {:admin_business_branch => business_branch_valid_attributes}
					expect(assigns(:business_branch)).to be_a_new(Admin::BusinessBranch)
				end

				it 'logs error' do
					expect(ExceptionHandler).to receive(:log)
					post :create, {:admin_business_branch => business_branch_valid_attributes}
				end
			end
		end
	end

	describe "PUT #update" do
		let(:new_valid_attributes) { {street_name: 'Modified Street Name'} }
		let(:new_invalid_attributes) { {street_name: ''} }

		context "with valid params" do
			it "updates the requested business_branch" do
				business_branch = branch_fixture
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				business_branch.reload
				expect(business_branch.street_name).to eq(new_valid_attributes[:street_name])
			end

			it "assigns the requested business_branch as @business_branch" do
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				expect(assigns(:business_branch)).to eq(branch_fixture)
			end

			it "assigns business as @referring_business if flash[referring_business_id]" do
				business = businesses(:restaurant)
				set_referring_business_id_to_flash(business)
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				expect(assigns(:referring_business)).to eq(business)
			end

			it "redirects to the business_branch if no flash[referring_business_id]" do
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				expect(response).to redirect_to(branch_fixture)
			end

			it "redirects to the business if flash[referring_busienss_id]" do
				business = businesses(:restaurant)
				set_referring_business_id_to_flash(business)
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				expect(response).to redirect_to(business)
			end
		end

		context "with invalid params" do
			it "assigns the business_branch as @business_branch" do
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_invalid_attributes}
				expect(assigns(:business_branch)).to eq(branch_fixture)
			end

			it "renders the 'edit' template" do
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it "renders the 'edit' template and assigns @referring_business if flash[referring_business_id]" do
				business = businesses(:restaurant)
				set_referring_business_id_to_flash(business)
				invalid_attributes = business_branch_invalid_attributes
				invalid_attributes[:business_id] = business.id

				put :update, {:id => branch_fixture.id, :admin_business_branch => new_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it "redirects to businesses if referring_business and business_branch.business don't match" do
				business = businesses(:dentist)
				set_referring_business_id_to_flash(business)
				put :update, {:id => branch_fixture.id, :admin_business_branch => new_invalid_attributes}
				expect(response).to redirect_to(admin_businesses_url)
			end

			it_behaves_like "invalid_id", 'put', :update
		end

		context 'anomalies' do 
			context 'business_branch.update! raises unknown error' do 
				before(:example) do
					bb_instance = branch_fixture
					allow(Admin::BusinessBranch).to receive(:where) { [bb_instance] }
					allow(bb_instance).to receive(:update!) { raise IOError, "Test Save" }
				end

				it 'renders edit template' do
					put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
					expect(response).to render_template(:edit)
				end

				it 'assigns bussiness as @business if flash[referring_busienss_id]' do
					business = businesses(:restaurant)
					set_referring_business_id_to_flash(business)
					put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
					expect(assigns(:referring_business)).to eq(business)
				end
				
				it 'assigns a newly created but unsaved business_branch as @business_branch' do
					put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
					expect(assigns(:business_branch)).to eq(branch_fixture)
				end

				it 'logs error' do
					expect(ExceptionHandler).to receive(:log)
					put :update, {:id => branch_fixture.id, :admin_business_branch => new_valid_attributes}
				end
			end
		end
	end

	describe "DELETE #destroy" do
		context "valid params" do
			it "destroys the requested business_branch" do
				expect {
					delete :destroy, {:id => branch_fixture.id}
				}.to change(Admin::BusinessBranch, :count).by(-1)
			end

			it "assigns business_branch as @business_branch" do
				delete :destroy, {:id => branch_fixture.id}
				expect(assigns(:business_branch)).to be_a(Admin::BusinessBranch)
			end

			it "assigns business as @referring_business if params[referring_business_id]" do
				business = businesses(:restaurant)
				delete :destroy, {:id => branch_fixture.id, referring_business_id: business.id}
				expect(assigns(:referring_business)).to eq(business)
			end

			it "redirects to business_branches" do
				delete :destroy, {:id => branch_fixture.id}
				expect(response).to redirect_to(admin_business_branches_url)
			end

			it "redirects to the business if valid params[referring_business_id]" do
				business = businesses(:restaurant)
				delete :destroy, {:id => branch_fixture.id, referring_business_id: business.id}
				expect(response).to redirect_to(business)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end

		context 'anomalies' do 
			context 'business_branch.update! raises unkown error' do 
				before(:example) do
					bb_instance = branch_fixture
					allow(Admin::BusinessBranch).to receive(:find) { bb_instance }
					allow(bb_instance).to receive(:destroy!) { raise IOError, "Test Save" }
				end

				it 'redirects to business branches' do
					delete :destroy, {:id => branch_fixture.id}
					expect(response).to redirect_to admin_business_branches_url
				end

				it 'redirects to business if valid params[referring_business_id]' do
					business = businesses(:restaurant)
					delete :destroy, {:id => branch_fixture.id, referring_business_id: business.id}
					expect(response).to redirect_to business
				end
			end
		end
	end

end
