require 'rails_helper'
require 'support/shared_tests'

RSpec.describe Admin::AgentsController, type: :controller do
	include_examples "controller_tests"

	fixtures :agents
	fixtures :admin_agents

	let(:valid_attributes) { {email: 'new_user@email.com'} }
	let(:invalid_attributes) { {email: '123'} }

	before(:example) { sign_in agents(:admin_julian) } 
	
	it_behaves_like "controller_tests"

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_agents_url)
			expect(flash[:notice]).not_to be nil
		end
	end
		
	describe "GET #index" do
		it "assigns all agents as @agents" do
			agents = Admin::Agent.all
			get :index
			expect(assigns(:agents)).to eq(agents)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested agent as @agent" do
				agent = admin_agents(:julian)
				get :show, {:id => agent.id}
				expect(assigns(:agent)).to eq(agent)
			end
		end
		
		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested agent as @agent" do
				agent = admin_agents(:julian)
				get :edit, {:id => agent.id}
				expect(assigns(:agent)).to eq(agent)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {email: 'new_email@email.com'} }

			it "updates the requested agent" do
				agent = admin_agents(:julian)
				put :update, {:id => agent.id, :admin_agent => new_attributes}
				agent.reload
				expect(agent.email).to eq(new_attributes[:email])
			end

			it "assigns the requested agent as @agent" do
				agent = admin_agents(:julian)
				put :update, {:id => agent.id, :admin_agent => valid_attributes}
				expect(assigns(:agent)).to eq(agent)
			end

			it "redirects to the agent" do
				agent = admin_agents(:julian)
				put :update, {:id => agent.id, :admin_agent => valid_attributes}
				expect(response).to redirect_to(agent)
			end
		end

		context "with invalid params" do
			it "assigns the agent as @agent" do
				agent = admin_agents(:julian)
				put :update, {:id => agent.id, :admin_agent => invalid_attributes}
				expect(assigns(:agent)).to eq(agent)
			end

			it "re-renders the 'edit' template" do
				agent = admin_agents(:julian)
				put :update, {:id => agent.id, :admin_agent => invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested agent" do
				agent = admin_agents(:julian)
				expect {
					delete :destroy, {:id => agent.id}
				}.to change(Agent, :count).by(-1)
			end

			it "redirects to the agents list" do
				agent = admin_agents(:julian)
				delete :destroy, {:id => agent.id}
				expect(response).to redirect_to(admin_agents_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
