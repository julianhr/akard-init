require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::SurveysController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :agents
	fixtures :surveys
	fixtures :surveys_dispensed

	let(:survey_fixtures) { surveys(:signup) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_surveys_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all surveys as @surveys" do
			surveys = Admin::Survey.all
			get :index
			expect(assigns(:surveys).to_a).to eq(surveys.to_a)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested survey as @survey" do
				get :show, {:id => survey_fixtures.id}
				expect(assigns(:survey)).to eq(survey_fixtures)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new survey as @survey" do
			get :new
			expect(assigns(:survey)).to be_a_new(Admin::Survey)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested survey as @survey" do
				get :edit, {:id => survey_fixtures.id}
				expect(assigns(:survey)).to eq(survey_fixtures)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new Survey" do
				expect {
					post :create, {:admin_survey => survey_valid_attributes}
				}.to change(Admin::Survey, :count).by(1)
			end

			it "assigns a newly created survey as @survey" do
				post :create, {:admin_survey => survey_valid_attributes}
				expect(assigns(:survey)).to be_a(Admin::Survey)
				expect(assigns(:survey)).to be_persisted
			end

			it "redirects to the created survey" do
				post :create, {:admin_survey => survey_valid_attributes}
				expect(response).to redirect_to(Admin::Survey.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved survey as @survey" do
				post :create, {:admin_survey => survey_invalid_attributes}
				expect(assigns(:survey)).to be_a_new(Admin::Survey)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_survey => survey_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {purpose: 'we have a new purpose'} }

			it "updates the requested survey" do
				survey = survey_fixtures
				put :update, {:id => survey.id, :admin_survey => new_attributes}
				survey.reload
				expect(survey.purpose).to eq(new_attributes[:purpose])
			end

			it "assigns the requested survey as @survey" do
				survey = survey_fixtures
				put :update, {:id => survey.id, :admin_survey => survey_valid_attributes}
				expect(assigns(:survey)).to eq(survey)
			end

			it "redirects to the survey" do
				survey = survey_fixtures
				put :update, {:id => survey.to_param, :admin_survey => survey_valid_attributes}
				expect(response).to redirect_to(survey)
			end
		end

		context "with invalid params" do
			it "assigns the survey as @survey" do
				survey = survey_fixtures
				put :update, {:id => survey.id, :admin_survey => survey_invalid_attributes}
				expect(assigns(:survey)).to eq(survey)
			end

			it "re-renders the 'edit' template" do
				survey = survey_fixtures
				put :update, {:id => survey.to_param, :admin_survey => survey_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested survey" do
				survey = survey_fixtures
				expect {
					delete :destroy, {:id => survey.id}
				}.to change(Admin::Survey, :count).by(-1)
			end

			it "destroys fk entries in surveys_sent" do
				survey = survey_fixtures
				expect {
					delete :destroy, {:id => survey.id}
				}.to change(Admin::SurveysDispensed, :count).by(-1)
			end

			it "redirects to the surveys list" do
				survey = survey_fixtures
				delete :destroy, {:id => survey.id}
				expect(response).to redirect_to(admin_surveys_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
