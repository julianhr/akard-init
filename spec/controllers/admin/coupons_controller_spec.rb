require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::CouponsController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :coupons
	fixtures :businesses
	fixtures :business_branches
	fixtures :agents

	let(:coupon_fixture) { coupons(:sushi) }
	let(:test_image) { 'folder/1/image.jpg' }

	before(:example) do
		allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [test_image] }
	end

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_coupons_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	shared_examples "variables_from_business_temp" do |verb, route, post_params|
		let(:request_params) do
			hash = {id: coupon_fixture.id}
			hash[:admin_coupon] = coupon_valid_attributes if post_params
			return hash
		end

		context 'valid params' do 
			it 'assigns @business_temp=business if valid params[coupon_business_id]' do
				business = Admin::Business.where(id: coupon_fixture.business_id).first
				request_params[:coupon_business_id] = business.id
				self.send verb, route, request_params
				expect(assigns(:business_temp)).to eq(business)
			end

			it 'assigns @business_temp=business if @coupon.business_id and params[:coupon_business_id].nil?', unless: (route == :new)  do
				business = Admin::Business.where(id: coupon_fixture.business_id).first
				self.send verb, route, request_params
				expect(assigns(:business_temp)).to eq(business)
			end

			it 'assigns corresponding branches to @branches if @business_temp has business' do
				business = businesses(:restaurant)
				branches = Admin::BusinessBranch.where(business_id: business.id)
				request_params[:coupon_business_id] = business.id
				self.send verb, route, request_params
				expect(assigns(:branches)).to eq(branches)
			end

			it 'assigns corresponding business branches to @branches if coupon is persisted and @business_temp is nil', unless: (route == :new) do
				branches = Admin::BusinessBranch.where(business_id: coupon_fixture.business_id)
				self.send verb, route, request_params
				expect(assigns(:branches)).to eq(branches)
			end

			it 'assigns empty array to @branches_short_address if @branches is nil' do
				allow(controller).to receive(:set_branches) { nil }
				self.send verb, route, request_params
				expect(assigns(:branches_short_address)).to eq([])
			end

			it 'assigns array with short adddresses to @branches_short_address if @branches has branches' do
				branches = Admin::BusinessBranch.where(id: coupon_fixture.business_branch_id).to_a
				self.send verb, route, request_params
				expect(assigns(:branches_short_address)).to be_an(Array)
			end
		end

		context 'invalid params' do 
			it 'assigns @business_temp = {} if invalid params[coupon_business_id]' do
				invalid_business_id = 999
				request_params[:coupon_business_id] = invalid_business_id
				self.send verb, route, request_params
				expect(assigns(:business_temp)).to eq({})
			end
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all coupons as @coupons" do
			coupons = Admin::Coupon.all
			get :index
			expect(assigns(:coupons)).to eq(coupons)
		end
	end

	describe "GET #show" do
		let(:image_paths) { ['test1', 'test2'] }

		before(:example) do
			allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { image_paths }
		end

		it_behaves_like "variables_from_business_temp", 'get', :show, false

		context 'valid params' do 
			it "assigns the requested coupon as @coupon" do
				get :show, {:id => coupon_fixture.id}
				expect(assigns(:coupon)).to eq(coupon_fixture)
			end

			it 'assigns an array to @image_paths' do
				get :show, {:id => coupon_fixture.id}
				expect(assigns(:image_paths)).to eq(image_paths)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end

		context 'anomalies' do 
			it "redirects to admin if exception is raised" do
				allow(ImageExplorer).to receive(:new) { raise IOError }
				get :show, {:id => coupon_fixture.id}
				expect(response).to redirect_to(admin_path)
				expect(flash[:notice]).to be_a(String)
			end
		end
	end

	describe 'GET #preview' do
		context 'valid params' do 
			it "assigns the requested coupon as @coupon" do
				get :preview, {:id => coupon_fixture.id}
				expect(assigns(:coupon)).to eq(coupon_fixture)
			end

			it 'assigns ImageExplorer instance to @business_img_path' do
				image_paths = ['business/1/test_image1.jpg', 'business/1/test_image2.jpg'] 
				allow_any_instance_of(ImageExplorer)
					.to receive(:get_image_paths) { image_paths }
				get :preview, {id: coupon_fixture.id}
				expect(assigns(:business_img_path)).to eq(image_paths.last)
			end

			it 'assigns ImageExplorer instance to @coupon_img_path' do
				image_paths = ['coupon/1/test_image1.jpg', 'coupon/1/test_image2.jpg'] 
				allow_any_instance_of(ImageExplorer)
					.to receive(:get_image_paths) { image_paths }
				get :preview, {id: coupon_fixture.id}
				expect(assigns(:coupon_img_paths)).to eq(image_paths)
			end

			it 'assigns @errors = {} if flash[cw_errors] is nil' do
				get :preview, {id: coupon_fixture.id}
				expect(assigns(:errors)).to eq( {} )
			end

			it 'assigns flash[cw_errors] to @errors if present' do
				error_hash = {error: 'test error'}
				set_flash(:cw_errors, error_hash)

				get :preview, {id: coupon_fixture.id}
				expect(assigns(:errors)).to eq( error_hash )
			end
		end

		context 'invalid params' do 
		end
	end

	describe "GET #new" do
		it_behaves_like "variables_from_business_temp", 'get', :new, false

		context 'valid params' do 
			it "assigns a new coupon as @coupon" do
				get :new
				expect(assigns(:coupon)).to be_a_new(Admin::Coupon)
			end

			it 'assigns all businesses as @businesses' do
				get :new, {:id => coupon_fixture.id}
				expect(assigns(:businesses)).to eq(Admin::Business.order(:name))
			end

			it 'assigns @business_temp = {} if no params[coupon_business_id] and coupon not persisted' do
				get :new
				expect(assigns(:business_temp)).to eq({})
			end

			it 'assigns empty hash to @branches if @business_temp is nil and coupon not persisted' do
				get :new
				expect(assigns(:branches)).to eq({})
			end
		end
	end

	describe "GET #edit" do
		it_behaves_like "variables_from_business_temp", 'get', :edit, false

		context 'valid params' do 
			it "assigns the requested coupon as @coupon" do
				get :edit, {:id => coupon_fixture.id}
				expect(assigns(:coupon)).to eq(coupon_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		it_behaves_like "variables_from_business_temp", 'post', :create, true

		context "with valid params" do
			it "creates a new Coupon" do
				expect {
					post :create, {:admin_coupon => coupon_valid_attributes}
				}.to change(Admin::Coupon, :count).by(1)
			end

			it "assigns a newly created coupon as @coupon" do
				post :create, {:admin_coupon => coupon_valid_attributes}
				expect(assigns(:coupon)).to be_a(Admin::Coupon)
				expect(assigns(:coupon)).to be_persisted
			end

			it "redirects to the created coupon" do
				post :create, {:admin_coupon => coupon_valid_attributes}
				expect(response).to redirect_to(Admin::Coupon.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved coupon as @coupon" do
				post :create, {:admin_coupon => coupon_invalid_attributes}
				expect(assigns(:coupon)).to be_a_new(Admin::Coupon)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_coupon => coupon_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		it_behaves_like "variables_from_business_temp", 'post', :create, true
		
		context "with valid params" do
			let(:new_attributes) { {title: 'This is a modified title.'} }

			it "updates the requested coupon" do
				put :update, {:id => coupon_fixture.id, :admin_coupon => new_attributes}
				coupon_fixture.reload
				expect(coupon_fixture.title).to eq(new_attributes[:title])
			end

			it "assigns the requested coupon as @coupon" do
				put :update, {:id => coupon_fixture.id, :admin_coupon => coupon_valid_attributes}
				expect(assigns(:coupon)).to eq(coupon_fixture)
			end

			it "redirects to the coupon" do
				put :update, {:id => coupon_fixture.id, :admin_coupon => coupon_valid_attributes}
				expect(response).to redirect_to(coupon_fixture)
			end
		end

		context "with invalid params" do
			it "assigns the coupon as @coupon" do
				put :update, {:id => coupon_fixture.id, :admin_coupon => coupon_invalid_attributes}
				expect(assigns(:coupon)).to eq(coupon_fixture)
			end

			it "re-renders the 'edit' template" do
				put :update, {:id => coupon_fixture.id, :admin_coupon => coupon_invalid_attributes}
				expect(response).to render_template("edit")
			end
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested coupon" do
				expect {
					delete :destroy, {:id => coupon_fixture.id}
				}.to change(Admin::Coupon, :count).by(-1)
			end

			it "redirects to the coupons list" do
				delete :destroy, {:id => coupon_fixture.id}
				expect(response).to redirect_to(admin_coupons_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

	describe 'PUT #upload_image' do
		let(:open_image) { fixture_file_upload('photos/test_1.jpg', 'image/jpg') }

		before(:example) do
			allow(ExceptionHandler).to receive(:log) {true}
			allow_any_instance_of(ImageExplorer).to receive(:get_image_file_names) { [test_image] }
		end

		context 'valid params' do 
			before(:example) do
				expect_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
				post :upload_image, {id: coupon_fixture.id, img: open_image}
			end

			it '@image_io is of the correct type' do
				expect(assigns(:image_io).respond_to? :original_filename).to be true
			end

			it 'assigns an instance of ImageExplorer to @img_explorer' do
				expect(assigns(:img_explorer)).to be_a(ImageExplorer)
			end

			it 'redirects to business' do
				expect(response).to redirect_to(coupon_fixture)
			end
		end

		context 'invalid params' do 
			before(:example) { allow(ExceptionHandler).to receive(:log) {true} }

			it 'redirects to businesses if business not found' do
				invalid_id = 999
				post :upload_image, {id: invalid_id, logo: open_image}
				expect(response).to redirect_to(admin_coupons_url)
			end

			it 'redirects to business if @image_io is of incorrect type' do
				allow_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
				image = File.open('spec/photos/test_1.jpg', 'r')
				post :upload_image, {id: coupon_fixture.id, logo: image}
				expect(assigns(:image_io).respond_to? :original_filename).to be false
				expect(response).to redirect_to(coupon_fixture)
			end
		end

		context 'anomalies' do 
			it 'logs the exception if raised by @img_explorer.upload_image' do
				allow_any_instance_of(ImageExplorer).to receive(:upload_image) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				post :upload_image, {id: coupon_fixture.id, img: open_image}
			end

			it 'redirects to business if an exception occurs when uploading the image' do
				expect_any_instance_of(ImageExplorer).to receive(:upload_image) { raise Exception}
				post :upload_image, {id: coupon_fixture.id, img: open_image}
				expect(response).to redirect_to(coupon_fixture)
			end
		end
	end

	describe 'DELETE #delete_image' do
		before(:example) { allow(ExceptionHandler).to receive(:log) {true}  }

		context 'valid params' do 
			before(:example) do
				expect_any_instance_of(ImageExplorer).to receive(:delete_image) { true }
				delete :delete_image, {id: coupon_fixture.id}
			end

			it 'assigns an instance of ImageExplorer to @img_explorer' do
				expect(assigns(:img_explorer)).to be_a(ImageExplorer)
			end

			it 'redirects to business' do
				expect(response).to redirect_to(coupon_fixture)
			end
		end

		context 'invalid params' do 
			before(:example) { allow(ExceptionHandler).to receive(:log) {true} }

			it 'redirects to businesses if business not found' do
				invalid_id = 999
				delete :delete_image, {id: invalid_id}
				expect(response).to redirect_to(admin_coupons_url)
			end
		end

		context 'anomalies' do 
			it 'logs the error if raised by @img_explorer.delete_image' do
				allow_any_instance_of(ImageExplorer).to receive(:delete_image) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				delete :delete_image, {id: coupon_fixture.id}
			end

			it 'redirects to business if an exception occurs when uploading the image' do
				expect_any_instance_of(ImageExplorer).to receive(:delete_image) { raise Exception}
				delete :delete_image, {id: coupon_fixture.id}
				expect(response).to redirect_to(coupon_fixture)
			end
		end
	end

end
