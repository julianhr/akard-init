require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::BusinessesController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :agents
	fixtures :businesses

	let(:business_fixture) { businesses(:restaurant) }

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_businesses_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	describe "GET #index" do
		it "assigns all businesses as @businesses" do
			business = Admin::Business.all
			get :index
			expect(assigns(:businesses)).to eq(business)
		end
	end

	describe "GET #show" do
		let(:image_paths) { ['test1', 'test2'] }

		before(:example) do
			allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { image_paths }
		end

		context 'valid params' do 
			it "assigns the requested business as @business" do
				get :show, {:id => business_fixture.to_param}
				expect(assigns(:business)).to eq(business_fixture)
			end

			it 'assigns array of images to @image_paths' do
				get :show, {:id => business_fixture.to_param}
				expect(assigns(:image_paths)).to eq(image_paths)
			end

			it 'assigns @referrer_opt a formatted hash' do
				hash = {referring_business_id: business_fixture.id}
				get :show, {id: business_fixture.id}
				expect(assigns(:referrer_opt)).to eq(hash)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new business as @business" do
			get :new
			expect(assigns(:business)).to be_a_new(Admin::Business)
		end

		it 'assigns @referrer_opt = {} if not params[business_id]' do
			empty_hash = {}
			get :new
			expect(assigns(:referrer_opt)).to eq(empty_hash)
		end

		it 'assigns @referrer_opt a formatted hash if params[business_id]' do
			business_id = business_fixture.id.to_s
			hash = {business_id: business_id}
			get :new, hash
			expect(assigns(:referrer_opt)).to eq(hash)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested business as @business" do
				business = business_fixture
				get :edit, {:id => business.to_param}
				expect(assigns(:business)).to eq(business)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new Business" do
				expect {
					post :create, {:admin_business => business_valid_attributes}
				}.to change(Admin::Business, :count).by(1)
			end

			it "assigns a newly created business as @business" do
				post :create, {:admin_business => business_valid_attributes}
				expect(assigns(:business)).to be_a(Admin::Business)
				expect(assigns(:business)).to be_persisted
			end

			it "redirects to the created business" do
				post :create, {:admin_business => business_valid_attributes}
				expect(response).to redirect_to(Admin::Business.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved business as @business" do
				post :create, {:admin_business => business_invalid_attributes}
				expect(assigns(:business)).to be_a_new(Admin::Business)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_business => business_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {name: 'My New Name'} }

			it "updates the requested business" do
				business = business_fixture
				put :update, {:id => business.to_param, :admin_business => new_attributes}
				business.reload
				expect(business.name).to eq(new_attributes[:name])
			end

			it "assigns the requested business as @business" do
				business = business_fixture
				put :update, {:id => business.to_param, :admin_business => business_valid_attributes}
				expect(assigns(:business)).to eq(business)
			end

			it "redirects to the business" do
				business = business_fixture
				put :update, {:id => business.to_param, :admin_business => business_valid_attributes}
				expect(response).to redirect_to(business)
			end
		end

		context "with invalid params" do
			it "assigns the business as @business" do
				business = business_fixture
				put :update, {:id => business.to_param, :admin_business => business_invalid_attributes}
				expect(assigns(:business)).to eq(business)
			end

			it "re-renders the 'edit' template" do
				business = business_fixture
				put :update, {:id => business.to_param, :admin_business => business_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested business" do
				business = business_fixture
				expect {
					delete :destroy, {:id => business.to_param}
				}.to change(Admin::Business, :count).by(-1)
			end

			it "redirects to the businesses list" do
				business = business_fixture
				delete :destroy, {:id => business.to_param}
				expect(response).to redirect_to(admin_businesses_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

	describe 'POST #upload_logo' do
		let(:open_image) { fixture_file_upload('photos/test_1.jpg', 'image/jpg') }

		before(:example) { allow(ExceptionHandler).to receive(:log) {true}  }

		context 'valid params' do 
			before(:example) do
				expect_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
				post :upload_logo, {id: business_fixture.id, logo: open_image}
			end

			it '@image_io is of the correct type' do
				expect(assigns(:image_io).respond_to? :original_filename).to be true
			end

			it 'assigns an instance of ImageExplorer to @img_explorer' do
				expect(assigns(:img_explorer)).to be_a(ImageExplorer)
			end

			it 'redirects to business' do
				expect(response).to redirect_to(business_fixture)
			end
		end

		context 'invalid params' do 
			before(:example) { allow(ExceptionHandler).to receive(:log) {true} }

			it 'redirects to businesses if business not found' do
				invalid_id = 999
				post :upload_logo, {id: invalid_id, logo: open_image}
				expect(response).to redirect_to(admin_businesses_url)
			end

			it 'redirects to business if @image_io is of incorrect type' do
				allow_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
				image = File.open('spec/photos/test_1.jpg', 'r')
				post :upload_logo, {id: business_fixture.id, logo: image}
				expect(assigns(:image_io).respond_to? :original_filename).to be false
				expect(response).to redirect_to(business_fixture)
			end
		end

		context 'anomalies' do 
			it 'logs the exception if raised by @img_explorer.upload_image' do
				allow_any_instance_of(ImageExplorer).to receive(:upload_image) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				post :upload_logo, {id: business_fixture.id, logo: open_image}
			end

			it 'redirects to business if an exception occurs when uploading the image' do
				expect_any_instance_of(ImageExplorer).to receive(:upload_image) { raise Exception }
				post :upload_logo, {id: business_fixture.id, logo: open_image}
				expect(response).to redirect_to(business_fixture)
			end
		end
	end

	describe 'DELETE #delete_logo' do
		before(:example) { allow(ExceptionHandler).to receive(:log) {true}  }

		context 'valid params' do 
			before(:example) do
				expect_any_instance_of(ImageExplorer).to receive(:delete_image) { true }
				delete :delete_logo, {id: business_fixture.id}
			end

			it 'assigns an instance of ImageExplorer to @img_explorer' do
				expect(assigns(:img_explorer)).to be_a(ImageExplorer)
			end

			it 'redirects to business' do
				expect(response).to redirect_to(business_fixture)
			end
		end

		context 'invalid params' do 
			before(:example) { allow(ExceptionHandler).to receive(:log) {true} }

			it 'redirects to businesses if business not found' do
				invalid_id = 999
				delete :delete_logo, {id: invalid_id}
				expect(response).to redirect_to(admin_businesses_url)
			end
		end

		context 'anomalies' do 
			it 'logs the error if raised by @img_explorer.delete_image' do
				allow_any_instance_of(ImageExplorer).to receive(:delete_image) { raise Exception }
				expect(ExceptionHandler).to receive(:log)
				delete :delete_logo, {id: business_fixture.id}
			end

			it 'redirects to business if an exception occurs when uploading the image' do
				expect_any_instance_of(ImageExplorer).to receive(:delete_image) { raise Exception}
				delete :delete_logo, {id: business_fixture.id}
				expect(response).to redirect_to(business_fixture)
			end
		end
	end

end
