require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::StatesController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :agents
	fixtures :states

	let(:state_fixture) { states(:jalisco) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_states_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all states as @states" do
			states = Admin::State.all
			get :index
			expect(assigns(:states).to_a).to eq(states.to_a)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested state as @state" do
				state = state_fixture
				get :show, {:id => state.to_param}
				expect(assigns(:state)).to eq(state)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested state as @state" do
				state = state_fixture
				get :edit, {:id => state.to_param}
				expect(assigns(:state)).to eq(state)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {name: 'Modified State'} }

			it "updates the requested state" do
				state = state_fixture
				put :update, {:id => state.to_param, :admin_state => new_attributes}
				state.reload
				expect(state.name).to eq new_attributes[:name]
			end

			it "assigns the requested state as @state" do
				state = state_fixture
				put :update, {:id => state.to_param, :admin_state => state_valid_attributes}
				expect(assigns(:state)).to eq(state)
			end

			it "redirects to the state" do
				state = state_fixture
				put :update, {:id => state.to_param, :admin_state => state_valid_attributes}
				expect(response).to redirect_to(state)
			end
		end

		context "with invalid params" do
			it "assigns the state as @state" do
				state = state_fixture
				put :update, {:id => state.to_param, :admin_state => state_invalid_attributes}
				expect(assigns(:state)).to eq(state)
			end

			it "re-renders the 'edit' template" do
				state = state_fixture
				put :update, {:id => state.to_param, :admin_state => state_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

end
