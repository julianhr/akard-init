require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::SurveysDispensedController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :surveys_dispensed
	fixtures :surveys
	fixtures :users
	fixtures :agents

	let(:sd1) { surveys_dispensed(:signup) }
	let(:sd2) { surveys_dispensed(:followup) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_surveys_dispensed_index_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all surveys_dispensed as @surveys_dispensed" do
			sds = Admin::SurveysDispensed.order(:id)
			get :index
			expect(assigns(:surveys_dispensed).to_a).to eq(sds.to_a)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested surveys_dispensed as @surveys_dispensed" do
				get :show, {:id => sd1.id}
				expect(assigns(:surveys_dispensed)).to eq(sd1)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new surveys_dispensed as @surveys_dispensed" do
			get :new
			expect(assigns(:surveys_dispensed)).to be_a_new(Admin::SurveysDispensed)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested surveys_dispensed as @surveys_dispensed" do
				sd = sd1
				get :edit, {:id => sd.id}
				expect(assigns(:surveys_dispensed)).to eq(sd)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new SurveysDispensed" do
				expect {
					post :create, {:admin_surveys_dispensed => survey_dispensed_valid_attributes}
				}.to change(Admin::SurveysDispensed, :count).by(1)
			end

			it "assigns a newly created surveys_dispensed as @surveys_dispensed" do
				post :create, {:admin_surveys_dispensed => survey_dispensed_valid_attributes}
				expect(assigns(:surveys_dispensed)).to be_a(Admin::SurveysDispensed)
				expect(assigns(:surveys_dispensed)).to be_persisted
			end

			it "redirects to the created surveys_dispensed" do
				post :create, {:admin_surveys_dispensed => survey_dispensed_valid_attributes}
				expect(response).to redirect_to(Admin::SurveysDispensed.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved surveys_dispensed as @surveys_dispensed" do
				post :create, {:admin_surveys_dispensed => survey_dispensed_invalid_attributes}
				expect(assigns(:surveys_dispensed)).to be_a_new(Admin::SurveysDispensed)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_surveys_dispensed => survey_dispensed_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {where: 'new_location'} }

			it "updates the requested surveys_dispensed" do
				sd = sd1
				put :update, {:id => sd.id, :admin_surveys_dispensed => new_attributes}
				sd.reload
				expect(sd.where).to eq(new_attributes[:where])
			end

			it "assigns the requested surveys_dispensed as @surveys_dispensed" do
				sd = sd1
				put :update, {:id => sd.id, :admin_surveys_dispensed => survey_dispensed_valid_attributes}
				expect(assigns(:surveys_dispensed)).to eq(sd)
			end

			it "redirects to the surveys_dispensed" do
				sd = sd1
				put :update, {:id => sd.id, :admin_surveys_dispensed => survey_dispensed_valid_attributes}
				expect(response).to redirect_to(sd)
			end
		end

		context "with invalid params" do
			it "assigns the surveys_dispensed as @surveys_dispensed" do
				sd = sd1
				put :update, {:id => sd.id, :admin_surveys_dispensed => survey_dispensed_invalid_attributes}
				expect(assigns(:surveys_dispensed)).to eq(sd)
			end

			it "re-renders the 'edit' template" do
				sd = sd1
				put :update, {:id => sd.id, :admin_surveys_dispensed => survey_dispensed_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested surveys_dispensed" do
				sd = sd1
				expect {
					delete :destroy, {:id => sd.id}
				}.to change(Admin::SurveysDispensed, :count).by(-1)
			end

			it "redirects to the surveys_dispensed list" do
				sd = sd1
				delete :destroy, {:id => sd.id}
				expect(response).to redirect_to(admin_surveys_dispensed_index_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
