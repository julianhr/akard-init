require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::CouponWinnersController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :coupon_winners
	fixtures :coupons
	fixtures :users
	fixtures :emails_sent
	fixtures :agents

	let(:cw_fixture) { coupon_winners(:pedro) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_coupon_winners_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all admin_coupon_winners as @admin_coupon_winners" do
			coupon_winners = Admin::CouponWinner.order(:id)
			get :index
			expect(assigns(:coupon_winners).order(:id)).to eq(coupon_winners)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested admin_coupon_winner as @admin_coupon_winner" do
				get :show, {:id => cw_fixture.to_param}
				expect(assigns(:coupon_winner)).to eq(cw_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new admin_coupon_winner as @admin_coupon_winner" do
			get :new
			expect(assigns(:coupon_winner)).to be_a_new(Admin::CouponWinner)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested admin_coupon_winner as @admin_coupon_winner" do
				get :edit, {:id => cw_fixture.to_param}
				expect(assigns(:coupon_winner)).to eq(cw_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new Admin::CouponWinner" do
				expect {
					post :create, {:admin_coupon_winner => coupon_winner_valid_attributes}
				}.to change(Admin::CouponWinner, :count).by(1)
			end

			it "assigns a newly created admin_coupon_winner as @admin_coupon_winner" do
				post :create, {:admin_coupon_winner => coupon_winner_valid_attributes}
				expect(assigns(:coupon_winner)).to be_a(Admin::CouponWinner)
				expect(assigns(:coupon_winner)).to be_persisted
			end

			it "redirects to the created admin_coupon_winner" do
				post :create, {:admin_coupon_winner => coupon_winner_valid_attributes}
				expect(response).to redirect_to(Admin::CouponWinner.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved admin_coupon_winner as @coupon_winner" do
				post :create, {:admin_coupon_winner => coupon_winner_invalid_attributes}
				expect(assigns(:coupon_winner)).to be_a_new(Admin::CouponWinner)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_coupon_winner => coupon_winner_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {token: 'NEWTOKEN'} }

			it "updates the requested admin_coupon_winner" do
				put :update, {:id => cw_fixture.id, :admin_coupon_winner => new_attributes}
				cw_fixture.reload
				expect(cw_fixture.token).to eq(new_attributes[:token])
			end

			it "assigns the requested admin_coupon_winner as @admin_coupon_winner" do
				put :update, {:id => cw_fixture.id, :admin_coupon_winner => new_attributes}
				expect(assigns(:coupon_winner)).to eq(cw_fixture)
			end

			it "redirects to the admin_coupon_winner" do
				put :update, {:id => cw_fixture.id, :admin_coupon_winner => new_attributes}
				expect(response).to redirect_to(cw_fixture)
			end
		end

		context "with invalid params" do
			it "assigns the admin_coupon_winner as @admin_coupon_winner" do
				coupon_winner = cw_fixture
				put :update, {:id => coupon_winner.to_param, :admin_coupon_winner => coupon_winner_invalid_attributes}
				expect(assigns(:coupon_winner)).to eq(coupon_winner)
			end

			it "re-renders the 'edit' template" do
				coupon_winner = cw_fixture
				put :update, {:id => coupon_winner.to_param, :admin_coupon_winner => coupon_winner_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested admin_coupon_winner" do
				expect {
					delete :destroy, {:id => cw_fixture.to_param}
				}.to change(Admin::CouponWinner, :count).by(-1)
			end

			it "redirects to the admin_coupon_winners list" do
				delete :destroy, {:id => cw_fixture.to_param}
				expect(response).to redirect_to(admin_coupon_winners_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
