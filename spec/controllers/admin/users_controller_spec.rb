require 'rails_helper'
require 'support/shared_tests'

RSpec.describe Admin::UsersController, type: :controller do
	include_examples "controller_tests"

	fixtures :admin_users
	fixtures :agents

	let(:valid_attributes) do
		{
			email: 'new_user@email.com', 
		} 
	end
	let(:invalid_attributes) { {email: '123'} }
	let(:user_fixture) { admin_users(:pedro) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to agents_url if agent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_users_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 
	
	it_behaves_like "controller_tests"
		
	describe "GET #index" do
		it "assigns all users as @users" do
			users = Admin::User.all
			get :index
			expect(assigns(:users).to_a).to eq(users.to_a)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested user as @user" do
				get :show, {:id => user_fixture.id}
				expect(assigns(:user)).to eq(user_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new user as @user" do
			get :new
			expect(assigns(:user)).to be_a_new(Admin::User)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested user as @user" do
				get :edit, {:id => user_fixture.id}
				expect(assigns(:user)).to eq(user_fixture)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new User" do
				expect {
					post :create, {:admin_user => valid_attributes}
				}.to change(Admin::User, :count).by(1)
			end

			it "assigns a newly created user as @user" do
				post :create, {:admin_user => valid_attributes}
				expect(assigns(:user)).to be_an(Admin::User)
				expect(assigns(:user)).to be_persisted
			end

			it "redirects to the created user" do
				post :create, {:admin_user => valid_attributes}
				expect(response).to redirect_to(Admin::User.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved user as @user" do
				post :create, {:admin_user => invalid_attributes}
				expect(assigns(:user)).to be_a_new(Admin::User)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_user => invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { 
				{
					email: 'new_email@email.com', 
				} 
			}

			it "updates the requested user" do
				user = user_fixture
				put :update, {:id => user.id, :admin_user => new_attributes}
				user.reload
				expect(user.email).to eq(new_attributes[:email])
			end

			it "assigns the requested user as @user" do
				put :update, {:id => user_fixture.id, :admin_user => valid_attributes}
				expect(assigns(:user)).to eq(user_fixture)
			end

			it "redirects to the user" do
				put :update, {:id => user_fixture.id, :admin_user => valid_attributes}
				expect(response).to redirect_to(user_fixture)
			end
		end

		context "with invalid params" do
			it "assigns the user as @user" do
				put :update, {:id => user_fixture.id, :admin_user => invalid_attributes}
				expect(assigns(:user)).to eq(user_fixture)
			end

			it "re-renders the 'edit' template" do
				put :update, {:id => user_fixture.id, :admin_user => invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested user" do
				expect {
					delete :destroy, {:id => user_fixture.id}
				}.to change(Admin::User, :count).by(-1)
			end

			it "redirects to the users list" do
				delete :destroy, {:id => user_fixture.id}
				expect(response).to redirect_to(admin_users_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
