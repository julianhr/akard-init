require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe Admin::EmailsSentController, type: :controller do
	include_context 'model_attributes'
	include_examples "controller_tests"

	fixtures :emails_sent
	fixtures :emails
	fixtures :users
	fixtures :agents

	let(:email_sent1) { emails_sent(:registration_successful) }
	let(:email_sent2) { emails_sent(:update_1) }

	shared_examples 'invalid_id' do |verb, route|
		it 'redirects to emails_sent_url if email_sent not found' do
			invalid_id = 999
			self.send verb, route, {id: invalid_id}
			expect(response).to redirect_to(admin_emails_sent_url)
			expect(flash[:notice]).not_to be nil
		end
	end

	before(:example) { sign_in agents(:admin_julian) } 

	it_behaves_like "controller_tests"

	describe "GET #index" do
		it "assigns all email_sent as @email_sent" do
			emails = Admin::EmailSent.all
			get :index
			expect(assigns(:emails_sent).to_a).to eq(emails.to_a)
		end
	end

	describe "GET #show" do
		context 'valid params' do 
			it "assigns the requested email_sent as @email_sent" do
				email_sent = email_sent1
				get :show, {:id => email_sent.id}
				expect(assigns(:email_sent)).to eq(email_sent)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :show
		end
	end

	describe "GET #new" do
		it "assigns a new email_sent as @email_sent" do
			get :new
			expect(assigns(:email_sent)).to be_a_new(Admin::EmailSent)
		end
	end

	describe "GET #edit" do
		context 'valid params' do 
			it "assigns the requested email_sent as @email_sent" do
				email_sent = email_sent1
				get :edit, {:id => email_sent.id}
				expect(assigns(:email_sent)).to eq(email_sent)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'get', :edit
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new EmailSent" do
				expect {
					post :create, {:admin_email_sent => email_sent_valid_attributes}
				}.to change(Admin::EmailSent, :count).by(1)
			end

			it "assigns a newly created email_sent as @email_sent" do
				post :create, {:admin_email_sent => email_sent_valid_attributes}
				expect(assigns(:email_sent)).to be_a(Admin::EmailSent)
				expect(assigns(:email_sent)).to be_persisted
			end

			it "redirects to the created email_sent" do
				post :create, {:admin_email_sent => email_sent_valid_attributes}
				expect(response).to redirect_to(Admin::EmailSent.last)
			end
		end

		context "with invalid params" do
			it "assigns a newly created but unsaved email_sent as @email_sent" do
				post :create, {:admin_email_sent => email_sent_invalid_attributes}
				expect(assigns(:email_sent)).to be_a_new(Admin::EmailSent)
			end

			it "re-renders the 'new' template" do
				post :create, {:admin_email_sent => email_sent_invalid_attributes}
				expect(response).to render_template("new")
			end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) { {status: Admin::EmailSent::STATUS_OPT.keys.last.to_s} }

			it "updates the requested email_sent" do
				email_sent = email_sent1
				put :update, {:id => email_sent.id, :admin_email_sent => new_attributes}
				email_sent.reload
				expect(email_sent.status).to eq new_attributes[:status]
			end

			it "assigns the requested email_sent as @email_sent" do
				email_sent = email_sent1
				put :update, {:id => email_sent.id, :admin_email_sent => email_sent_valid_attributes}
				expect(assigns(:email_sent)).to eq(email_sent)
			end

			it "redirects to the email_sent" do
				email_sent = email_sent1
				put :update, {:id => email_sent.id, :admin_email_sent => email_sent_valid_attributes}
				expect(response).to redirect_to(email_sent)
			end
		end

		context "with invalid params" do
			it "assigns the email_sent as @email_sent" do
				email_sent = email_sent1
				put :update, {:id => email_sent.id, :admin_email_sent => email_sent_invalid_attributes}
				expect(assigns(:email_sent)).to eq(email_sent)
			end

			it "re-renders the 'edit' template" do
				email_sent = email_sent1
				put :update, {:id => email_sent.id, :admin_email_sent => email_sent_invalid_attributes}
				expect(response).to render_template("edit")
			end

			it_behaves_like "invalid_id", 'put', :update
		end
	end

	describe "DELETE #destroy" do
		context 'valid params' do 
			it "destroys the requested email_sent" do
				email_sent = email_sent1
				expect {
					delete :destroy, {:id => email_sent.id}
				}.to change(Admin::EmailSent, :count).by(-1)
			end

			it "redirects to the email_sent list" do
				email_sent = email_sent1
				delete :destroy, {:id => email_sent.id}
				expect(response).to redirect_to(admin_emails_sent_url)
			end
		end

		context 'invalid params' do 
			it_behaves_like "invalid_id", 'delete', :destroy
		end
	end

end
