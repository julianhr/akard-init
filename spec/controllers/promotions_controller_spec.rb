require 'rails_helper'
require 'support/model_attributes'
require 'support/shared_tests'

RSpec.describe PromotionsController, type: :controller do
	include_context "model_attributes"

	fixtures :coupons
	fixtures :coupon_winners
	fixtures :users
	fixtures :businesses
	fixtures :business_branches
	fixtures :emails

	def create_coupon(publish_start_datetime, published_days)
		coupon = Admin::Coupon.new coupon_valid_attributes

		coupon.publish_start_datetime = publish_start_datetime
		coupon.publish_end_datetime 	= publish_start_datetime + published_days
		coupon.valid_start_datetime 	= publish_start_datetime
		coupon.valid_end_datetime 		= publish_start_datetime + published_days + 30.days
		coupon.exchanged_datetime 		= nil

		coupon.save
		return coupon
	end

	let(:promotion_fixture) { coupons(:sushi) }

	describe "GET #index" do
		it 'assigns correct promotions as @promotions' do
			get :index
			expect(assigns(:promotions).first).to be_a(Admin::Coupon)
		end

		it 'displays only coupons that should be published' do
			Admin::CouponWinner.delete_all
			Admin::Coupon.delete_all

			datetime_now = DateTime.now
			coupon1 = create_coupon(datetime_now - 40.days, 10.days)
			coupon2 = create_coupon(datetime_now - 5.days, 10.days)
			coupon3 = create_coupon(datetime_now - 1.days, 10.days)
			coupon4 = create_coupon(datetime_now + 1.days, 10.days)

			get :index			
			expect(assigns(:promotions).count).to eq(2)
			expect(assigns(:promotions)).to include(coupon2, coupon3)
			expect(assigns(:promotions)).not_to include(coupon1, coupon4)

			coupon2.publish_active = false
			coupon2.save
			get :index

			expect(assigns(:promotions).count).to eq(1)
			expect(assigns(:promotions)).not_to include(coupon2)
		end

		it 'assigns @referrer=flash[referrer] if present' do
			text = 'signup_survey'
			set_flash(:referrer, text)
			get :index
			expect(assigns(:referrer)).to eq text
		end
	end

	describe "GET #show" do
		before(:example) do
			allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
		end

		context 'valid attributes' do 
			before(:example) { get :show, {id: promotion_fixture.id} }
	
			specify { expect(assigns(:promotion)).to eq(promotion_fixture) }
			specify { expect(assigns(:promotion)).to be_persisted }
			specify { expect(assigns(:business_img_path)).to be_a(String) }
			specify { expect(assigns(:promotion_img_paths)).to be_an(Array) }
		end

		context 'invalid attributes' do 
			let(:params) { {id: promotion_fixture.id} }

			it 'redirects to promotions if promotion id is invalid' do
				invalid_id = 999
				get :show, {id: invalid_id}
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end
		end

		context 'anomalies' do 
			it 'catches exception if raised when setting @business_img_path or @promotion_img_paths' do
				allow_any_instance_of(ImageExplorer)
					.to receive(:get_image_paths) { raise Exception }

				expect(ExceptionHandler).to receive(:log) {true}
				get :show, {id: promotion_fixture.id}
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end
		end
	end


	describe "POST #claim" do
		let(:http_referrer) { promotion_path(promotion_fixture.id) }
		let(:winner) { users(:maria) }
		let(:existing_user_valid_params) do
			{
				id: promotion_fixture.id,
				ganador: {correo: winner.email}
			}
		end

		before(:example) do
			mail_message_double = instance_double("Mail::Message")
			allow(mail_message_double).to receive(:errors) { [] }
			allow_any_instance_of(ActionMailer::MessageDelivery)
				.to receive(:deliver_later) { mail_message_double }
			allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
		end

		shared_examples 'claim_valid_params' do
			it { post :claim, valid_params
				expect(assigns(:promotion)).to eq(promotion_fixture) }
			it { post :claim, valid_params
				expect(assigns(:user)).to be_a(User)
				expect(assigns(:user)).to be_persisted }
			it { post :claim, valid_params
				expect(assigns(:promotion_winner)).to be_an(Admin::CouponWinner) }
			it { expect {post :claim, valid_params}
				.to change(Admin::CouponWinner, :count).by(1) }
			it { post :claim, valid_params
				expect(assigns(:email)).to be_a(Admin::Email) }
			it { post :claim, valid_params
				expect(assigns(:email_sent)).to be_a(Admin::EmailSent)
				expect(assigns(:email_sent)).to be_persisted }

			it 'decreases availability by 1 if everything checks out' do
				available_old = promotion_fixture.available
				post :claim, valid_params
				promotion_fixture.reload
				expect(promotion_fixture.available).to eq(available_old - 1)
			end

			it 'sends email to winner via #deliver_user_emailsent' do
				expect(controller).to receive(:deliver_emailsent)
				post :claim, valid_params
			end

			it 'redirects to promotion#winner if everything checks out' do
				post :claim, valid_params
				cw = Admin::CouponWinner.last
				redirect_params = {token: cw.token, email: cw.user.email}
				expect(response).to redirect_to(promotion_winner_path(promotion_fixture.id, redirect_params)) 
			end

			it 'redirects to :back if availability is 0 or less' do
				promotion_fixture.update(available: 0)
				post :claim, valid_params
				notice = ErrorMsg::MSG[:promo_depleted]
				expect(response).to redirect_to(http_referrer)
				expect(flash[:notice]).to be_a(String)
			end

			it 'renders :show if repeat winner' do
				cw = Admin::CouponWinner.where(coupon_id: promotion_fixture.id).first
				params = valid_params
				params[:ganador][:correo] = cw.user.email
				post :claim, params
				expect(response).to render_template(:show)
				expect(flash[:notice]).to be_a(String)
			end
		end

		before(:example) do
			request.env["HTTP_REFERER"] = http_referrer
		end

		context 'existing user valid parameters' do 
			let(:valid_params) do
				{
					id: promotion_fixture.id,
					ganador: {correo: winner.email}
				}
			end

			it_behaves_like "claim_valid_params"

			it { post :claim, valid_params
				expect(assigns(:user)).to eq winner }
			it { post :claim, valid_params
				expect(assigns(:promotion_winner).user.email).to eq winner.email }
			it { post :claim, valid_params
				expect(assigns(:survey)).to be nil }
		end

		context 'new user valid parameters' do 
			let(:new_email) { 'new_user@email.com' }
			let(:valid_params) do
				{
					id: promotion_fixture.id,
					ganador: {correo: new_email}
				}
			end

			it_behaves_like "claim_valid_params"

			it { post :claim, valid_params
				expect(assigns(:user)).to be_a(User) }
			it { expect {post :claim, valid_params}
				.to change(User, :count).by(1) }
			it { post :claim, valid_params
				expect(assigns(:promotion_winner).user.email).to eq new_email }
			it { post :claim, valid_params
				expect(assigns(:survey)).to be_a(Admin::SurveysDispensed) }
			it { expect {post :claim, valid_params}
				.to change(Admin::SurveysDispensed, :count).by(1) }
		end

		context 'invalid parameters' do 
			it 'redirects to coupons if id is invalid' do
				params = existing_user_valid_params
				params[:id] = 999
				post :claim, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it 'render :show template if email is invalid' do
				params = existing_user_valid_params
				params[:ganador][:correo] = 'invalid_email'
				post :claim, params
				expect(response).to render_template(:show)
			end
		end

		context 'anomalies' do 
			it 'handles exception if raised by @user.save!' do
				allow_any_instance_of(User).to receive(:save!) { raise Exception, "Test text" }
				valid_params = existing_user_valid_params
				valid_params[:ganador][:correo] = 'new_email@email.com'

				expect(ExceptionHandler).to receive(:log)
				post :claim, existing_user_valid_params
				expect(response).to redirect_to(http_referrer)
				expect(flash[:notice]).to be_a(String)
			end

			it 'handles exception if raised by @promotion_winner.save!' do
				allow_any_instance_of(Admin::CouponWinner).to receive(:save!) { raise Exception, "Test text" }
				expect(ExceptionHandler).to receive(:log)
				post :claim, existing_user_valid_params
				expect(response).to redirect_to(http_referrer)
				expect(flash[:alert]).to be_a(String)
			end

			it 'handles exception if raised by @promotion.save!' do
				allow_any_instance_of(Admin::Coupon).to receive(:save!) { raise Exception, "Test text" }
				expect(ExceptionHandler).to receive(:log)
				post :claim, existing_user_valid_params
				expect(response).to redirect_to(http_referrer)
				expect(flash[:alert]).to be_a(String)
			end
		end
	end

	describe "GET #winner" do
		let(:valid_params) do
			params = {
				id: coupons(:sushi).id,
				email: coupon_winners(:pedro).user.email,
				token: coupon_winners(:pedro).token
			}
		end

		before(:example) do
			allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
		end

		context "valid attributes" do
			before(:example) { get :winner, valid_params }

			specify { expect(assigns(:qr)).to be_a(RQRCode::QRCode) }
			specify { expect(assigns(:promotion_winner)).to be_a(Admin::CouponWinner) }
			specify { expect(assigns(:promotion)).to be_a(Admin::Coupon) }
			specify { expect(assigns(:promotion)).to be_persisted }
			specify { expect(assigns(:promotion_img_path)).to be_nil }
			specify { expect(assigns(:logo_img_path)).to be_nil }
		end

		context 'invalid attributes' do 
			it "redirects to promotions if params[id] doesn't exist" do
				params = valid_params
				params[:id] = 999
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it 'redirects to promotions if params[email] is incorrectly formatted or is empty' do
				params = valid_params
				params[:email] = 'invalid_email'
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it "redirects to promotions if params[email] can't be found for @user" do
				params = valid_params
				params[:email] = 'unrecorded@email.com'
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it 'redirects to promotions if params[token] is empty' do
				params = valid_params
				params[:token] = nil
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it "redirects to promotions if params[id] can't be found for @promotion_winner" do
				params = valid_params
				params[:id] = coupons(:dentist).id
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it "redirects to promotions if params[email] can't be found for @promotion_winner" do
				params = valid_params
				params[:email] = users(:maria).email
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end

			it 'redirects to promotions if params[token] does not match @promotion_winner.token' do
				params = valid_params
				params[:token] = 'incorrect_token'
				get :winner, params
				expect(response).to redirect_to(promotions_url)
				expect(flash[:notice]).to be_a(String)
			end
		end
	end

end
