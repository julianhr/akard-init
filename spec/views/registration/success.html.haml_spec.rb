require 'rails_helper'

RSpec.describe "registration/success.html.haml", type: :view do
	fixtures :users
	fixtures :surveys_dispensed

	context 'survey not filled out yet' do 
		before(:example) do
			sd = surveys_dispensed(:signup)
			sd.update(response: nil)
			assign(:user, users(:pedro))
			assign(:survey_dispensed, sd)
		end

		specify { expect(render).to have_css("#registration-success", count: 1) }
		specify { expect(render).to have_css("#logo_inside > a > img[alt='Akard Logo']", count: 1) }
		specify { expect(render).to have_css("#description", text: /.+{2,}/) }
		specify { expect(render).to have_css("#survey form[action='#{registro_encuesta_path}']", count: 1) }
		specify { expect(render).to have_css("#survey input[type='submit']", count: 1) }
	end

	context 'survey filled out' do 
		before(:example) do
			assign(:user, users(:pedro))
			assign(:survey_dispensed, surveys_dispensed(:signup))
		end

		specify { expect(render).not_to have_css("#survey form[action='#{registro_encuesta_path}']") }
	end

end
