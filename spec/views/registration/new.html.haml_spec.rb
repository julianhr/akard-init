require 'rails_helper'

RSpec.describe "registration/new.html.haml", type: :view do
	fixtures :agents

	before(:example) do
		assign(:user, User.new)
		assign(:errors, {})
	end

	context 'logo area' do 
		specify { expect(render)
			.to have_css('#logo_new > img', count: 1) }
	end

	context 'email form' do
		specify { expect(render)
			.to have_css('form#new_user[action="/registro/crear"]', count: 1) }
		specify { expect(render)
			.to have_css('input#user_email[type="text"]', count: 1) }
		specify { expect(render)
			.to have_css('input[type="submit"]', count: 1) }
	end

	context 'Facebook widget' do
		specify { expect(render).to have_css('div.fb-like', count: 1) }
	end

	context 'displays explanations' do
		specify { expect(render).to have_css('div.explanation', count: 3) }
		specify { expect(render).to have_css('div.explanation-small', count: 3) }
	end
end
