require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "messages/new.html.haml", type: :view do
	include_context "model_attributes"

	let(:error_msg) { ErrorMsg::MSG }

	context 'valid entries' do 
		before(:example) do
			assign(:message, Message.new)
			assign(:message_sent, nil)
			render
		end

		it 'renders main elements correctly' do
			expect(rendered).to have_css(".title-generic", count: 1)
			expect(rendered).to have_css("form[action='#{contact_path}']", count: 1)
			expect(rendered).to have_css("input#message_name", count: 1)
			expect(rendered).to have_css("input[type='text']")
			expect(rendered).to have_css("textarea#message_body")
			expect(rendered).to have_css("input[type='submit']")
		end

	end

	context 'invalid entries' do 
		before(:example) do
			message = Message.new message_valid_attributes
			message.name = nil
			message.email = 'invalid_email'
			message.valid?
			assign(:message, message)
			assign(:message_sent, nil)
			render
		end

		it 'renders main elements correctly' do
			expect(rendered).to have_css(".field-error > label")
			expect(rendered).to have_css(".field-error > input")
			expect(render).to have_css(".field-error > span", text: error_msg[:empty_field])
			expect(rendered).to have_css(".field-error > span", text: error_msg[:email_invalid])
		end
	end

	context 'form is successully submitted' do 
		before(:example) do
			assign(:message, Message.new)
			assign(:message_sent, true)
			render
		end

		it 'renders main elements correctly' do
			expect(rendered).to have_css(".message-sent", text: /.{2,}/)
			expect(rendered).not_to have_css("form")
		end
	end

end
