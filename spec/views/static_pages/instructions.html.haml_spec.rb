require 'rails_helper'

RSpec.describe "static_pages/instructions.html.haml", type: :view do
	it 'renders main elements correctly' do
		render
		expect(rendered).to have_css(".title-static", count: 1)
	end
end
