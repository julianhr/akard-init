require 'rails_helper'

RSpec.describe "layouts/application.html.haml", type: :view do

	fixtures :agents

	let(:path_admin) {  }

	describe 'admin panel' do 
		context 'request fullpath does not start with "admin" ' do 
			before(:example) do
				allow_any_instance_of(ActionDispatch::Request).to receive(:fullpath) {'/promociones'}
			end
			
			it 'not displayed if agent not logged in' do
				expect(render).not_to have_css('#admin-dashboard')
			end

			it 'displayed if agent logged in' do
				sign_in agents(:admin_julian)
				expect(render).to have_css('#admin-dashboard')
			end
		end

		context 'request fullpath does starts with "admin" ' do 
			before(:example) do
				allow_any_instance_of(ActionDispatch::Request).to receive(:fullpath) {'/admin'}
			end
			
			it 'not displayed if agent not logged in' do
				expect(render).not_to have_css('#admin-dashboard')
			end

			it 'displayed if agent logged in' do
				sign_in agents(:admin_julian)
				expect(render).not_to have_css('#admin-dashboard')
			end
		end
	end
end



