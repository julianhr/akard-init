require 'rails_helper'

RSpec.describe "promotions/show.html.haml", type: :view do

	fixtures :coupons

	let(:promotion_fixture) { coupons(:sushi) }

	describe 'promotion is available' do
		before(:example) do
			assign(:promotion, promotion_fixture)
			assign(:promotion_img_paths, ['promotion/1/image_1.jpg'])
			assign(:business_img_path, 'business/1/image_1.jpg')
			assign(:errors, {})
		end

		context 'promotion body' do 
			specify { expect(render).to have_css("a > img[alt='Logo']") }
			specify { expect(render).to have_css(".title", text: /.{2,}/) }
			specify { expect(render).to have_css(".countdown span", count: 1) }
			specify { expect(render).to have_text(promotion_fixture.available) }
			specify { expect(view).to receive(:wrap_lines_with_tag).twice; render }
			specify { expect(render).to have_css("img[alt='Producto']") }
			specify { expect(render).to have_css("form[action='#{promotion_path(promotion_fixture)}']") }
			specify { expect(render).to have_css("form input#ganador_correo[type='text']") }
			specify { expect(render).to have_css("form input[type='submit']") }
		end

		context 'business info' do 
			specify { expect(render).to have_css(".business-info img") }
			specify { expect(view).to receive(:business_address); render }
			specify { expect(view).to receive(:linked_business_map); render }
		end

		context 'conditions' do 
			specify { expect(render).to have_css(".conditions-info > ul > li", text: /.{2,}/) }
			specify { expect(view).to receive(:wrap_lines_with_tag).twice; render }
		end
	end

	describe 'promotion is depleted' do
		before(:example) do
			promotion_fixture.available = 0
			assign(:promotion, promotion_fixture)
			assign(:promotion_img_paths, ['promotion/1/image_1.jpg'])
			assign(:business_img_path, 'business/1/image_1.jpg')
			assign(:errors, {})
		end

		specify { expect(render).to have_css("a[href='#{registro_nuevo_path}']", text: /aquí/) }
	end

end
