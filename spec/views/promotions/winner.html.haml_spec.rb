require 'rails_helper'

RSpec.describe "promotions/winner.html.haml", type: :view do

	fixtures :coupons
	fixtures :coupon_winners
	fixtures :users

	let(:promotion_fixture) { coupons(:sushi) }
	let(:promotion_img_path) { 'promotion/1/image_1.jpg' }
	let(:logo_img_path) { 'business/1/image_1.jpg' }
	let(:rqr_code) do
		RQRCode::QRCode.new("http://akard.club/promociones/#{promotion_fixture.id}", :size => 3, :level => :m)
	end

	before(:example) do
		assign(:promotion, promotion_fixture)
		assign(:user, users(:pedro))
		assign(:qr, rqr_code)
		assign(:promotion_winner, coupon_winners(:pedro))
		assign(:promotion_img_path, promotion_img_path)
		assign(:logo_img_path, logo_img_path)
	end

	context 'renders main elements corrects' do
		specify { expect(render).to have_css("a > img[alt='Logo']", count: 1) }
		specify { expect(render).to have_css(".instructions", count: 1) }
		specify { expect(render).to have_css(".qr", count: 1) }
		specify { expect(render).to have_css(".coupon-winner", count: 1) }
		specify { expect(render).to have_css(".coupon-winner", text: promotion_fixture.title) }
		specify { expect(render).to have_css(".coupon-winner img[alt='Producto'][src='/images/#{promotion_img_path}']") }
		specify { expect(render).to have_css(".coupon-winner img[alt='Logo'][src='/images/#{logo_img_path}']") }
		specify { expect(view).to receive(:business_address); render }
		specify { expect(render).to have_text(promotion_fixture.conditions.split("\n").first) }
		specify { expect(render).to have_css(".map", count: 1) }
		specify { expect(view).to receive(:linked_business_map); render }
	end

	context 'no javascript' do 
		#qr code in html
		specify { expect(render).to have_css(".qr > noscript > table > tr > td.white") }
		specify { expect(render).to have_css(".qr > noscript > table > tr > td.black") }
	end

end
