require 'rails_helper'

RSpec.describe "promotions/index.html.haml", type: :view do

	fixtures :coupons
	fixtures :business_branches

	let(:promo1) { coupons(:sushi) }
	let(:promo2) { coupons(:dentist) }

	before(:example) do
		ie_double = instance_double("ImageExplorer")
		allow(ie_double).to receive(:get_image_paths) { ['folder/folder/1/image.jpg'] }
		allow(ImageExplorer).to receive(:new) { ie_double }
		assign(:promotions, [promo1, promo2])
		render
	end

	it 'renders main elements correctly' do
		expect(rendered).to have_css("#promotions-index", count: 1)
		expect(rendered).to have_css("#akard-logo", count: 1)
		expect(rendered).to have_css(".title", count: 1)
		expect(rendered).to have_css(".promotions .promotion", count: 2)
		expect(rendered).to have_css(".promotion", text: promo1.title, count: 1)
		expect(rendered).to have_css(".promotion", text: promo2.business_branch.city, count: 1)
		expect(rendered).to have_css(".promotion img", count: 2)
	end

	it 'renders welcome message if referred from signup survey' do
		assign(:referrer, 'signup_survey')
		expect(render).to have_css(".welcome", count: 1)
		expect(render).not_to have_css(".title")
	end

end
