require 'rails_helper'

RSpec.describe "admin/business_branches/edit", type: :view do
	
	fixtures :business_branches
	fixtures :businesses

	let(:business_branch) { business_branches(:americas) }

	before(:each) { assign(:business_branch, business_branch) }
	before(:each) { assign(:states_opt, Admin::State.all.collect{|state| [state.name,state.id]}) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/business_branches/#{business_branch.id}']", count: 1) }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Colonia', count: 1) }
	specify { expect(render).to have_css("input#admin_business_branch_borough[value='#{business_branch.borough}']", count: 1) }
	specify { expect(render).to have_css("select#admin_business_branch_state_id", count: 1) }
	specify { expect(render).to have_css("select#admin_business_branch_state_id > option", text: /.{2,}/, count: 2) }
	specify { expect(render).to have_css("input[type='submit']") }
	specify { expect(render).to have_css('a', text: 'Mostrar') }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
