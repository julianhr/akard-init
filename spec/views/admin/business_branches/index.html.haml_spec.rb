require 'rails_helper'

RSpec.describe "admin/business_branches/index", type: :view do
	fixtures :business_branches

	let(:branch_1) { business_branches(:americas) }
	let(:branch_2) { business_branches(:moctezuma) }

	before(:example) { assign(:business_branches, [branch_1, branch_2]) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }

	specify { expect(render).to have_css('thead > tr > th', count: 9) }
	specify { expect(render).to include('Calle') }

	specify { expect(render).to have_css('tbody > tr', count: 2) }
	specify { expect(render).to include(branch_1.borough) }
	specify { expect(render).to include(branch_2.borough) }

	specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
	specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
	specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

	specify { expect(render).to have_css('a', text: 'Nueva Sucursal', count: 1) }
end
