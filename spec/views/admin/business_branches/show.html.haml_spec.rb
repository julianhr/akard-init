require 'rails_helper'

RSpec.describe "admin/business_branches/show", type: :view do
	fixtures :business_branches
	fixtures :businesses
	fixtures :states

	let(:business_branch) { business_branches(:americas) }

	before(:each) { assign(:business_branch, business_branch) }
	before(:each) { assign(:states_opt, Admin::State.all.collect{|state| [state.name,state.id]}) }


	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new survey information' do
		specify { expect(render).to have_css('p > b', text: /.{2,}/) }
		specify { expect(render).to include('Calle') }

		specify { expect(render).to have_css('p', text: /.{2,}/) }
		specify { expect(render).to have_css('p', text: business_branch.street_name, count: 1) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
