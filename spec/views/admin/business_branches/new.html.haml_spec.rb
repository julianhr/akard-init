require 'rails_helper'

RSpec.describe "admin/business_branches/new", type: :view do
	before(:each) { assign(:business_branch, Admin::BusinessBranch.new) }
	before(:each) { assign(:states_opt, Admin::State.all.collect{|state| [state.name,state.id]}) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/business_branches']") }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Calle', count: 1) }
	specify { expect(render).to have_css("select#admin_business_branch_state_id", count: 1) }
	specify { expect(render).to have_css("select#admin_business_branch_state_id > option", text: /.{2,}/, count: 2) }
	specify { expect(render).to have_css("input#admin_business_branch_street_name", count: 1) }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
