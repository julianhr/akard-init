require 'rails_helper'

RSpec.describe "admin/coupon_winners/index", type: :view do
	fixtures :coupon_winners
	fixtures :users

	let(:cw_1) { coupon_winners(:pedro) }
	let(:cw_2) { coupon_winners(:maria) }

	before(:example) do
		assign(:coupon_winners, [cw_1, cw_2])
		render
	end

	specify { expect(rendered).to have_css('.title', text: /.{2,}/, count: 1) }

	specify { expect(rendered).to have_css('thead > tr > th', count: 8) }
	specify { expect(rendered).to include('Usuario') }

	specify { expect(rendered).to have_css('tbody > tr', count: 2) }
	specify { expect(rendered).to have_link(cw_1.user.email, href: admin_user_path(cw_1.user)) }
	specify { expect(rendered).to have_link(cw_2.user.email, href: admin_user_path(cw_2.user)) }

	specify { expect(rendered).to have_css('a', text: 'Mostrar', count: 2) }
	specify { expect(rendered).to have_css('a', text: 'Editar', count: 2) }
	specify { expect(rendered).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

	specify { expect(rendered).to have_css('a', text: 'Nuevo Ganador de Cupón', count: 1) }
end
