require 'rails_helper'

RSpec.describe "admin/coupon_winners/edit", type: :view do
	fixtures :coupon_winners

	let(:cw_fixture) { coupon_winners(:pedro) }

	before(:each) { assign(:coupon_winner, cw_fixture) }

	specify { expect(render)
		.to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render)
		.to have_css("form[action='/admin/coupon_winners/#{cw_fixture.id}']", count: 1) }
	specify { expect(render)
		.to have_css("form[method='post']", count: 1) }
	specify { expect(render)
		.to have_css("label", text: 'Token', count: 1) }
	specify { expect(render)
		.to have_css("input#admin_coupon_winner_status[value='#{cw_fixture.status}']", count: 1) }
	specify { expect(render)
		.to have_css("input[type='submit']") }
	specify { expect(render)
		.to have_css('a', text: 'Mostrar') }
	specify { expect(render)
		.to have_css('a', text: 'Listado') }
end
