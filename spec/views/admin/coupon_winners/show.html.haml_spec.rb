require 'rails_helper'

RSpec.describe "admin/coupon_winners/show", type: :view do
	include ActionView::Helpers

	fixtures :coupon_winners

	let(:cw_fixture) { coupon_winners(:pedro) }

	before(:each) { assign(:coupon_winner, cw_fixture) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the main information' do
		specify { expect(render).to have_css('p > b', text: /.{2,}/, count: 8) }
		specify { expect(render).to include('Token') }

		specify { expect(render).to have_css('p', text: /.{2,}/, count: 8) }
		specify { expect(render).to have_css('p', text: cw_fixture.token, count: 1) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
