require 'rails_helper'

RSpec.describe "admin/coupon_winners/new", type: :view do
	before(:each) { assign(:coupon_winner, Admin::CouponWinner.new) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/coupon_winners']") }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Token', count: 1) }
	specify { expect(render).to have_css("input#admin_coupon_winner_token", count: 1) }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
