require 'rails_helper'

RSpec.describe "admin/project_updates/show", type: :view do

	fixtures :project_updates

	let(:pu_fixture) { project_updates(:can_email) }

	before(:example) { assign(:project_update, pu_fixture) }

	describe 'all elements render correctly' do
		specify { expect(render)
			.to have_css(".title", text: /.{2,}/, count: 1) }
		specify { expect(render)
			.to have_text("Usuario") }
		specify { expect(render)
			.to have_text("#{pu_fixture.email_status}") }
		specify { expect(render)
			.to have_link("Editar", href: edit_admin_project_update_path(pu_fixture) ) }
		specify { expect(render)
			.to have_css("a[href='#{admin_project_updates_path}']", text: /.{2,}/) }
	end

end
