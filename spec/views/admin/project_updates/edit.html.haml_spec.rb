require 'rails_helper'

RSpec.describe "admin/project_updates/edit", type: :view do

	fixtures :project_updates

	let(:pu_fixture) { project_updates(:can_email) }

	before(:example) { assign(:project_update, pu_fixture) }

	describe 'all elements render correctly' do
		specify { expect(render).to have_css(".title", text: /.{2,}/, count: 1) }
		specify { expect(render).to have_css("form.edit_admin_project_update[method='post']", count: 1) }
		specify { expect(render).to have_css("form label", text: /.{2,}/) }
		specify { expect(render).to have_css("form input[type='text']") }
		specify { expect(render).to have_css("form input[type='submit']", count: 1) }
		specify { expect(render).to have_css("a[href='#{admin_project_updates_path}']", text: /.{2,}/) }
	end

end
