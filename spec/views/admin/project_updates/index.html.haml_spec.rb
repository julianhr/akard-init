require 'rails_helper'

RSpec.describe "admin/project_updates/index", type: :view do

	fixtures :project_updates

	let(:pu_fixture1) { project_updates(:can_email) }
	let(:pu_fixture2) { project_updates(:cannot_email) }

	before(:example) { assign(:project_updates, [pu_fixture1, pu_fixture2]) }

	describe 'all elements render correctly' do
		specify { expect(render)
			.to have_css(".title", text: /.{2,}/, count: 1) }
		specify { expect(render)
			.to have_css('thead > tr > th', text: 'User') }
		specify { expect(render)
			.to have_css('tbody > tr > td', text: Admin::ProjectUpdate::EMAIL_STATUS_OPT.first) }
		specify { expect(render)
			.to have_link('Mostrar', href: admin_project_update_path(pu_fixture1)) }
		specify { expect(render)
			.to have_css("a[href='#{new_admin_project_update_path}']") }
	end

end
