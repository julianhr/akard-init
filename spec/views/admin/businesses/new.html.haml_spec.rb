require 'rails_helper'

RSpec.describe "admin/businesses/new", type: :view do
	before(:each) { assign(:business, Admin::Business.new) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/businesses']") }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Nombre', count: 1) }
	specify { expect(render).to have_css("input#admin_business_name", count: 1) }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
