require 'rails_helper'

RSpec.describe "admin/businesses/index", type: :view do

	fixtures :businesses

	let(:business_1) { businesses(:restaurant) }
	let(:business_2) { businesses(:dentist) }

	before(:example) { assign(:businesses, [business_1, business_2]) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }

	specify { expect(render).to have_css('thead > tr > th', count: 7) }
	specify { expect(render).to include('Giro') }

	specify { expect(render).to have_css('tbody > tr', count: 2) }
	specify { expect(render).to include(business_1.name) }
	specify { expect(render).to include(business_2.name) }

	specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
	specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
	specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

	specify { expect(render).to have_css('a', text: 'Nuevo Negocio', count: 1) }

end
