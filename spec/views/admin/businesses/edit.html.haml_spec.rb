require 'rails_helper'

RSpec.describe "admin/businesses/edit", type: :view do
	fixtures :businesses

	let(:business) { businesses(:restaurant) }

	before(:each) { assign(:business, business) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/businesses/#{business.id}']", count: 1) }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Nombre', count: 1) }
	specify { expect(render).to have_css("input#admin_business_name[value='#{business.name}']", count: 1) }
	specify { expect(render).to have_css("input[type='submit']") }
	specify { expect(render).to have_css('a', text: 'Mostrar') }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
