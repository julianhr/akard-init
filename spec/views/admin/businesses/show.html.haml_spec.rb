require 'rails_helper'

RSpec.describe "admin/businesses/show", type: :view do
	include ActionView::Helpers

	fixtures :businesses
	fixtures :business_branches

	let(:business) { businesses(:restaurant) }
	let(:branches) { Admin::BusinessBranch.where(business_id: business.id) }

	def conc_address(branch) 
		concatenate_address_from_branch(branch) 
	end

	before(:each) do 
		allow(File).to receive(:size) { 1024 }
		allow(view).to receive(:print_logos_from_paths) { nil }
		
		assign(:business, business)
		assign(:branches, branches)
		assign(:image_paths, ['dir/dir/image1.jpg', 'dir/dir/image2.jpg'])
	end

	it 'displays the title' do
		render
		expect(rendered).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the business information' do
		specify { expect(render).to have_css('p > b', text: /.{2,}/, count: 5) }
		specify { expect(render).to include('Nombre') }

		specify { expect(render).to have_css('p', text: /.{2,}/, count: 5) }
		specify { expect(render).to have_css('p', text: business.name, count: 1) }
	end

	describe 'renders the branches information' do
		specify { expect(render).to have_css('tr', count: 1) }
		specify { expect(render).to have_css('td', text: conc_address(branches.first), count: 1) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end

end
