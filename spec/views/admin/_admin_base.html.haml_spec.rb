require 'rails_helper'

RSpec.describe "admin/_admin_base", type: :view do
	
	context 'top-bar navigation' do 
		specify { expect(render)
			.to have_css('nav.top-bar > ul.title-area > li.logo', count: 1) }
		specify { expect(render).to have_css("li.logo > a > img") }
		specify { expect(render)
		.to have_css("section.top-bar-section a", text: 'Salir', count: 1) }
	end

	context 'side navigation ul.side-nav' do 
		specify { expect(render).to have_css('nav > ul.side-nav', count: 1) }
		specify { expect(render)
			.to have_css('.side-nav > li > a', text: 'Inicio', count: 1) }
		specify { expect(render)
			.to have_css('.side-nav > li > a', text: /\w{2,}/, count: 14) }
	end

end
