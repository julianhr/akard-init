require 'rails_helper'

RSpec.describe "admin/emails/index", type: :view do
	fixtures :emails

	let(:email_1) { emails(:registration) }
	let(:email_2) { emails(:new_coupon) }

	before(:example) { assign(:emails, [email_1, email_2]) }

	describe 'renders a list of emails' do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to have_css('thead > tr > th', count: 8) }
		specify { expect(render).to include('Propósito') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }
		specify { expect(render).to include(email_1.purpose) }
		specify { expect(render).to include(email_2.purpose) }

		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

		specify { expect(render).to have_css('a', text: 'Nuevo Correo', count: 1) }
	end
end
