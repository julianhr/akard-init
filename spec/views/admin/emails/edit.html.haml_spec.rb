require 'rails_helper'

RSpec.describe "admin/emails/edit", type: :view do
	fixtures :emails

	let(:email) { emails(:new_coupon) }

	before(:each) { assign(:email, email) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey form' do
		expect(render).to have_css("form[action='/admin/emails/#{email.id}']")
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Mostrar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end

end
