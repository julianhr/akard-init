require 'rails_helper'

RSpec.describe "admin/emails/_form", type: :view do
	fixtures :emails

	let(:email) { emails(:new_coupon) }

	before(:each) { assign(:email, email) }

	describe 'renders the new email form' do
		specify { expect(render)
			.to have_css("form[method='post']") }

		specify { expect(render).to include('Propósito') }

		specify { expect(render).to have_css("select#admin_email_model") }
		specify { expect(render).to have_css("input#admin_email_template_name") }
		Admin::Email::MODEL_OPT.each do |value|
			specify { expect(render)
				.to have_css("option[value='#{value}']", text: value) }
		end
		specify { expect(render).to have_css("input[type='submit']") }
	end
end
