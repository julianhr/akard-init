require 'rails_helper'

RSpec.describe "admin/emails/show", type: :view do
	fixtures :emails

	let(:email) { emails(:new_coupon) }

	before(:each) { assign(:email, email) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new survey information' do
		specify { expect(render).to have_css('p > b', text: /.{2,}/, count: 5) }
		specify { expect(render).to include('Asunto') }

		specify { expect(render).to have_css('p', text: /.{2,}/, count: 5) }
		specify { expect(render).to have_css('p', text: email.subject, count: 1) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
