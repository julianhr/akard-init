require 'rails_helper'

RSpec.describe "admin/emails/new", type: :view do
	before(:each) { assign(:email, Admin::Email.new) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey form' do
		expect(render).to have_css("form[action='/admin/emails']")
	end

	it 'has a return link' do
		expect(render).to have_css('a', text: 'Listado')
	end
end
