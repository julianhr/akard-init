require 'rails_helper'

RSpec.describe "admin/coupons/index", type: :view do
	fixtures :coupons

	let(:coupon_1) { coupons(:sushi) }
	let(:coupon_2) { coupons(:dentist) }

	before(:example) { assign(:coupons, [coupon_1, coupon_2]) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }

	specify { expect(render).to have_css('thead > tr > th', count: 8) }
	specify { expect(render).to include('Título') }

	specify { expect(render).to have_css('tbody > tr', count: 2) }
	specify { expect(render).to include(coupon_1.title) }
	specify { expect(render).to include(coupon_2.title) }

	specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
	specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
	specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

	specify { expect(render).to have_css('a', text: 'Nuevo Cupón', count: 1) }
end
