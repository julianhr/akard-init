require 'rails_helper'

RSpec.describe "admin/coupons/edit", type: :view do
	fixtures :coupons
	fixtures :businesses
	fixtures :business_branches

	let(:coupon_fixture) { coupons(:sushi) }
	let(:branches) { [business_branches(:americas)] }

	before(:each) do 
		assign(:coupon, coupon_fixture) 
		assign(:businesses, Admin::Business.all) 
		assign(:branches, branches)
		assign(:branches_short_address, ['Some short address']) 
	end

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/coupons/#{coupon_fixture.id}']", count: 1) }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Título', count: 1) }
	specify { expect(render).to have_css("input#admin_coupon_title[value='#{coupon_fixture.title}']", count: 1) }
	specify { expect(render).to have_css("input[type='submit']") }
	specify { expect(render).to have_css('a', text: 'Mostrar') }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
