require 'rails_helper'

RSpec.describe "admin/coupons/show", type: :view do
	fixtures :coupons
	fixtures :businesses
	fixtures :business_branches

	let(:coupon) { coupons(:sushi) }
	let(:branches) { [business_branches(:americas)] }

	before(:each) do
		allow(File).to receive(:size) { 1024 }

		assign(:coupon, coupon)
		assign(:businesses, Admin::Business.all) 
		assign(:business_temp, {})
		assign(:branches, branches)
		assign(:branches_short_address, ['Some short address']) 
		assign(:image_paths, ['dir/dir/image1.jpg', 'dir/dir/image2.jpg'])
	end

	describe 'renders all elements correctly' do
		specify { expect(render).to have_css('ul > li', text: 'ACCIONES:', count: 1) }
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
		specify { expect(render).to have_css('p > b', text: /.{2,}/) }
		specify { expect(render).to include('Título') }
		specify { expect(render).to have_css('p', text: /.{2,}/) }
		specify { expect(render).to have_css('p', text: coupon.title, count: 1) }
		specify { expect(render).to have_css('li > a > img') }
		specify { expect(render).to have_css('input[type="file"]') }
	end
end
