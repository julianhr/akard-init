require 'rails_helper'

RSpec.describe "admin/users/edit", type: :view do
	fixtures :admin_users

	let(:user) { admin_users(:pedro) }
	before(:each) { assign(:user, user) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new user form' do
		expect(render).to have_css("form[action='/admin/users/#{user.id}']")
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Mostrar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
