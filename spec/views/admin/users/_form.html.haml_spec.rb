require 'rails_helper'

RSpec.describe "admin/users/_form", type: :view do
	fixtures :admin_users

	let(:user) { admin_users(:pedro) }

	before(:each) { assign(:user, user) }

	describe 'renders the new user form' do
		specify { expect(render).to have_css("form[method='post']") }
		specify { expect(render).to include('Correo') }
		specify { expect(render).to have_css("input#admin_user_email[value='#{user.email}']") }
		specify { expect(render).to have_css("input[type='submit']") }
	end

end
