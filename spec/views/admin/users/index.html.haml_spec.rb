require 'rails_helper'

RSpec.describe "admin/users/index", type: :view do
	fixtures :admin_users

	let(:user_1) { admin_users(:pedro) }
	let(:user_2) { admin_users(:maria) }
	
	before(:each) { assign(:users, [user_1, user_2]) }

	context "renders a list of users" do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to include('Id:') }
		specify { expect(render).to include('Correo:') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }
		specify { expect(render).to have_css('tr > td', text: user_1.email) }
		specify { expect(render).to have_css('tr > td', text: user_2.email) }
		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Eliminar', count: 2) }

		specify { expect(render).to have_css('a', text: 'Nuevo Usuario', count: 1) }
	end
end
