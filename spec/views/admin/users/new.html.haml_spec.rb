require 'rails_helper'

RSpec.describe "admin/users/new", type: :view do
	before(:each) { assign(:user, Admin::User.new) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new user form' do
		expect(render).to have_css("form[action='/admin/users']")
	end

	it 'has a return link' do
		expect(render).to have_css('a', text: 'Listado')
	end

end
