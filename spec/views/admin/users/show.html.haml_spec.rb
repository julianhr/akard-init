require 'rails_helper'

RSpec.describe "admin/users/show", type: :view do
	fixtures :admin_users

	let(:user) { admin_users(:pedro) }

	before(:each) { assign(:user, user) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new user information' do
		specify { expect(render).to include('Id:') }
		specify { expect(render).to include('Email:') }

		specify { expect(render).to include(user.id.to_s) }
		specify { expect(render).to include(user.email) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
