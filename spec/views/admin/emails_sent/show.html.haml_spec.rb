require 'rails_helper'

RSpec.describe "admin/emails_sent/show", type: :view do
	fixtures :emails_sent

	let(:es) { emails_sent(:registration_successful) }

	before(:each) { assign(:email_sent, es) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the email sent information' do

		specify { expect(render).to have_css('p > b', text: /.{2,}/, count: 7) }
		specify { expect(render).to include('Id:') }
		specify { expect(render).to include('Correo:') }

		specify { expect(render).to include(es.id.to_s) }
		specify { expect(render)
			.to have_link(es.email.purpose, href: admin_email_path(es.email.id)) }
		specify { expect(render)
			.to have_link(es.user.email, href: admin_user_path(es.user.id)) }
		specify { expect(render).to include(es.status) }
		specify { expect(render).to have_css('pre', text: JSON.pretty_generate(es.response)) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
