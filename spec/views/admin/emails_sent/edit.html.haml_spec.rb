require 'rails_helper'

RSpec.describe "admin/emails_sent/edit", type: :view do
	fixtures :emails_sent

	let(:es) { emails_sent(:registration_successful) }

	before(:each) { assign(:email_sent, es) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey form' do
		expect(render).to have_css("form[action='/admin/emails_sent/#{es.id}']")
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Mostrar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
