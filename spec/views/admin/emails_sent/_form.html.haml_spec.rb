require 'rails_helper'

RSpec.describe "admin/emails_sent/_form", type: :view do
	fixtures :emails_sent

	let(:es) { emails_sent(:registration_successful) }

	before(:each) { assign(:email_sent, es) }

	describe 'renders the new survey form' do
		specify { expect(render)
			.to have_css("form[method='post']") }

		specify { expect(render).to include('Correo') }
		specify { expect(render).to include('Enviado?') }

		specify { expect(render)
			.to have_css("input#admin_email_sent_email_id[value='#{es.email.id}']") }
		specify { expect(render)
			.to have_css("input#admin_email_sent_sent[value='#{es.sent? ? 1 : 0}']") }
		specify { expect(render)
			.to have_css("textarea#admin_email_sent_response") }

		specify { expect(render).to have_css("input[type='submit']") }
	end
end
