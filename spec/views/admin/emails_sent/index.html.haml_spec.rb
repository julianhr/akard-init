require 'rails_helper'

RSpec.describe "admin/emails_sent/index", type: :view do
	fixtures :emails_sent

	let(:es1) { emails_sent(:registration_successful) }
	let(:es2) { emails_sent(:update_1) }

	before(:example) { assign(:emails_sent, [es1, es2]) }

	describe "renders a list of emails_sent" do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to have_css('tr > th', count: 10) }
		specify { expect(render).to include('Id') }
		specify { expect(render).to include('Estatus') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }
		specify { expect(render).to include(es1.status) }
		specify { expect(render).to include(es2.status) }
		specify { expect(render).to include(es1.response ? 'Si' : 'No') }

		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

		specify { expect(render).to have_css('a', text: 'Nuevo Correo Enviado', count: 1) }
	end
end
