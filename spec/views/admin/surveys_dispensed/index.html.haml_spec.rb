require 'rails_helper'

RSpec.describe "admin/surveys_dispensed/index", type: :view do
	fixtures :surveys_dispensed

	let(:sd1) { surveys_dispensed(:signup) }
	let(:sd2) { surveys_dispensed(:followup) }

	before(:example) { assign(:surveys_dispensed, [sd1, sd2]) }

	describe "renders a list of surveys_dispensed" do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to include('Id') }
		specify { expect(render).to include('Encuesta') }
		specify { expect(render).to include('Usuario') }
		specify { expect(render).to include('Donde Aparece') }
		specify { expect(render).to include('Respuesta') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }

		specify { expect(render).to include(sd1.id.to_s) }
		specify { expect(render).to include(sd1.survey_id.to_s) }
		specify { expect(render).to include(sd1.user_id.to_s) }
		specify { expect(render).to include(sd1.where) }
		specify { expect(render).to include(CGI.escape_html(sd1.response.to_s)) }

		specify { expect(render).to include(sd2.id.to_s) }

		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

		specify { expect(render).to have_css('a', text: 'Dispensar nueva encuesta', count: 1) }
	end
end
