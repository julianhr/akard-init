require 'rails_helper'

RSpec.describe "admin/surveys_dispensed/show", type: :view do
	fixtures :surveys_dispensed

	let(:sd) { surveys_dispensed(:signup) }

	before(:each) { assign(:surveys_dispensed, sd) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new survey information' do
		specify { expect(render).to include('Id:') }
		specify { expect(render).to include('Encuesta:') }
		specify { expect(render).to include('Usuario:') }
		specify { expect(render).to include('Donde Aparece:') }
		specify { expect(render).to include('Respuesta:') }

		specify { expect(render).to include(sd.id.to_s) }
		specify { expect(render)
			.to have_link(sd.survey.purpose, href: admin_survey_path(sd.survey.id)) }
		specify { expect(render)
			.to have_link(sd.user.email, href: admin_user_path(sd.user.id)) }
		specify { expect(render).to include(sd.where) }
		specify { expect(render).to include(CGI.escapeHTML sd.response.to_json) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
