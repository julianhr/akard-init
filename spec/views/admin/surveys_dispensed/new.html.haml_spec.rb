require 'rails_helper'

RSpec.describe "admin/surveys_dispensed/new", type: :view do
	before(:each) { assign(:surveys_dispensed, Admin::SurveysDispensed.new) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey_dispensed form' do
		expect(render).to have_css("form[action='/admin/surveys_dispensed']")
	end

	it 'has a return link' do
		expect(render).to have_css('a', text: 'Listado')
	end
end
