require 'rails_helper'

RSpec.describe "admin/surveys_dispensed/edit", type: :view do
	fixtures :surveys_dispensed

	let(:sd) { surveys_dispensed(:signup) }

	before(:each) { assign(:surveys_dispensed, sd) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey form' do
		expect(render).to have_css("form[action='/admin/surveys_dispensed/#{sd.id}']")
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Mostrar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
