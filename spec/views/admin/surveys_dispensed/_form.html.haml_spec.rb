require 'rails_helper'

RSpec.describe "admin/surveys_dispensed/_form", type: :view do
	fixtures :surveys_dispensed

	let(:sd) { surveys_dispensed(:signup) }

	before(:each) { assign(:surveys_dispensed, sd) }

	describe 'renders the new survey form' do
		specify { expect(render)
			.to have_css("form[method='post']") }

		specify { expect(render).to include('Encuesta') }
		specify { expect(render).to include('Usuario') }
		specify { expect(render).to include('Donde Aparece') }
		specify { expect(render).to include('Respuesta') }

		specify { expect(render)
			.to have_css("input#admin_surveys_dispensed_survey_id[value='#{sd.survey.id}']") }
		specify { expect(render)
			.to have_css("input#admin_surveys_dispensed_user_id[value='#{sd.user.id}']") }
		specify { expect(render)
			.to have_css("input#admin_surveys_dispensed_where[value='#{sd.where}']") }
		specify { expect(render)
			.to have_css("input#admin_surveys_dispensed_response[value='#{sd.response}']") }

		specify { expect(render).to have_css("input[type='submit']") }
	end
end
