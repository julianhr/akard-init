require 'rails_helper'

RSpec.describe "admin/states/index", type: :view do
	fixtures :states

	let(:state_1) { states(:jalisco) }
	let(:state_2) { states(:colima) }

	before(:example) { assign(:states, [state_1, state_2]) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }

	specify { expect(render).to have_css('thead > tr > th', count: 4) }
	specify { expect(render).to include('Nombre') }

	specify { expect(render).to have_css('tbody > tr', count: 2) }
	specify { expect(render).to include(state_1.name) }
	specify { expect(render).to include(state_2.name) }

	specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
	specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
end
