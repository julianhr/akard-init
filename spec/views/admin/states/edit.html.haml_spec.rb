require 'rails_helper'

RSpec.describe "admin/states/edit", type: :view do
	fixtures :states

	let(:state) { states(:jalisco) }

	before(:each) { assign(:state, state) }

	specify { expect(render).to have_css('.title', text: /.{2,}/, count: 1) }
	specify { expect(render).to have_css("form[action='/admin/states/#{state.id}']", count: 1) }
	specify { expect(render).to have_css("form[method='post']", count: 1) }
	specify { expect(render).to have_css("label", text: 'Nombre', count: 1) }
	specify { expect(render).to have_css("input#admin_state_name[value='#{state.name}']", count: 1) }
	specify { expect(render).to have_css("input[type='submit']") }
	specify { expect(render).to have_css('a', text: 'Mostrar') }
	specify { expect(render).to have_css('a', text: 'Listado') }
end
