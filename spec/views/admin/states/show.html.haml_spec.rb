require 'rails_helper'

RSpec.describe "admin/states/show", type: :view do
	fixtures :states

	let(:state) { states(:jalisco) }

	before(:each) { assign(:state, state) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new survey information' do
		specify { expect(render).to have_css('p > b', text: /.{2,}/, count: 2) }
		specify { expect(render).to include('Nombre') }

		specify { expect(render).to have_css('p', text: /.{2,}/, count: 2) }
		specify { expect(render).to have_css('p', text: state.name, count: 1) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
