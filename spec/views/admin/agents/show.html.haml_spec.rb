require 'rails_helper'

RSpec.describe "admin/agents/show", type: :view do
	fixtures :admin_agents

	let(:agent) { admin_agents(:julian) }

	before(:each) { assign(:agent, agent) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new agent information' do
		specify { expect(render).to include('Nombre:') }
		specify { expect(render).to include('Apellido:') }
		specify { expect(render).to include('Correo:') }
		specify { expect(render).to include('Aprobado:') }

		specify { expect(render).to include(agent.first_name) }
		specify { expect(render).to include(agent.last_name) }
		specify { expect(render).to include(agent.email) }
		specify { expect(render).to include(agent.approved.to_s) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
