require 'rails_helper'

RSpec.describe "admin/agents/edit", type: :view do
	fixtures :admin_agents

	let(:agent) { admin_agents(:julian)}

	before(:each) { assign(:agent, agent) }

	it 'displays the title' do
		expect(render).to include('Editar Agente')
	end

	it 'renders the new agent form' do
		expect(render).to have_css("form[action='/admin/agents/#{agent.id}']")
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Mostrar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
