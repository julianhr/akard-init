require 'rails_helper'

RSpec.describe "admin/agents/_form", type: :view do
	fixtures :admin_agents

	let(:agent) { admin_agents(:julian) }

	before(:each) { assign(:agent, agent) }

	describe 'renders the new user form' do
		specify { expect(render)
			.to have_css("form[method='post']") }

		specify { expect(render).to include('Nombre') }
		specify { expect(render).to include('Apellido') }
		specify { expect(render).to include('Correo') }
		specify { expect(render).to include('Aprobado') }

		specify { expect(render)
			.to have_css("input#admin_agent_first_name[value='#{agent.first_name}']") }
		specify { expect(render)
			.to have_css("input#admin_agent_last_name[value='#{agent.last_name}']") }
		specify { expect(render)
			.to have_css("input#admin_agent_email[value='#{agent.email}']") }
		specify { expect(render)
			.to have_css("input#admin_agent_approved[value='#{agent.approved}']") }

		specify { expect(render).to have_css("input[type='submit']") }
	end

end
