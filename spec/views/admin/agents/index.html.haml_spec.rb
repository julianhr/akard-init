require 'rails_helper'

RSpec.describe "admin/agents/index", type: :view do
	fixtures :admin_agents

	let(:agent_1) { admin_agents(:julian) }
	let(:agent_2) { admin_agents(:john) }
	
	before(:each) { assign(:agents, [agent_1, agent_2]) }

	context "renders a list of agents" do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to include('Nombre:') }
		specify { expect(render).to include('Apellido:') }
		specify { expect(render).to include('Correo:') }
		specify { expect(render).to include('Aprobado?') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }
		specify { expect(render).to have_css('tr > td', text: agent_1.first_name) }
		specify { expect(render).to have_css('tr > td', text: agent_2.first_name) }
		specify { expect(render).to have_css('tr > td', text: agent_1.last_name) }
		specify { expect(render).to have_css('tr > td', text: agent_2.last_name) }
		specify { expect(render).to have_css('tr > td', text: agent_1.email) }
		specify { expect(render).to have_css('tr > td', text: agent_2.email) }
		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Eliminar', count: 2) }
	end
end
