require 'rails_helper'

RSpec.describe "admin/surveys/show", type: :view do
	fixtures :surveys

	let(:survey) { surveys(:signup) }

	before(:each) { assign(:survey, survey) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	describe 'renders the new survey information' do
		specify { expect(render).to include('Id:') }
		specify { expect(render).to include('Propósito:') }
		specify { expect(render).to include('Audiencia:') }
		specify { expect(render).to include('Plantilla (partial):') }

		specify { expect(render).to include(survey.id.to_s) }
		specify { expect(render).to include(survey.purpose) }
		specify { expect(render).to include(survey.audience) }
		specify { expect(render).to include(survey.template_name) }
	end

	describe 'renders action links' do
		specify { expect(render).to have_css('a', text: 'Editar') }
		specify { expect(render).to have_css('a', text: 'Listado') }
	end
end
