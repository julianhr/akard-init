require 'rails_helper'

RSpec.describe "admin/surveys/_form", type: :view do
	fixtures :surveys

	let(:survey) { surveys(:signup) }

	before(:each) { assign(:survey, survey) }

	describe 'renders the new survey form' do
		specify { expect(render)
			.to have_css("form[method='post']") }

		specify { expect(render).to include('Propósito') }
		specify { expect(render).to include('Audiencia') }
		specify { expect(render).to include('Plantilla (partial)') }

		specify { expect(render).to have_css("input#admin_survey_purpose[value='#{survey.purpose}']") }
		specify { expect(render).to have_css("select#admin_survey_audience") }
		Admin::Survey::AUDIENCE_OPT.each do |opt|
			specify { expect(render)
				.to have_css("option[value='#{opt}']", text: opt, count: 1) }
		end
		specify { expect(render).to have_css("select#admin_survey_audience") }
		specify { expect(render).to have_css("input#admin_survey_template_name[value='#{survey.template_name}']") }

		specify { expect(render).to have_css("input[type='submit']") }
	end
end
