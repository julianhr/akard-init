require 'rails_helper'

RSpec.describe "admin/surveys/new", type: :view do
	before(:each) { assign(:survey, Admin::Survey.new) }

	it 'displays the title' do
		expect(render).to have_css('.title', text: /.{2,}/, count: 1)
	end

	it 'renders the new survey form' do
		expect(render).to have_css("form[action='/admin/surveys']")
	end

	it 'has a return link' do
		expect(render).to have_css('a', text: 'Listado')
	end

end
