require 'rails_helper'

RSpec.describe "admin/surveys/index", type: :view do
	fixtures :surveys

	let(:survey_1) { surveys(:signup) }
	let(:survey_2) { surveys(:signup_follow_up) }

	before(:example) { assign(:surveys, [survey_1, survey_2]) }

	describe 'renders a list of surveys' do
		specify { expect(render).to have_css('.title', text: /.{2,}/) }

		specify { expect(render).to include('Id') }
		specify { expect(render).to include('Propósito') }
		specify { expect(render).to include('Audiencia') }
		specify { expect(render).to include('Plantilla (partial)') }

		specify { expect(render).to have_css('tbody > tr', count: 2) }

		specify { expect(render).to include(survey_1.id.to_s) }
		specify { expect(render).to include(survey_1.purpose) }
		specify { expect(render).to include(survey_1.audience) }
		specify { expect(render).to include(survey_1.template_name) }

		specify { expect(render).to include(survey_2.id.to_s) }
		specify { expect(render).to include(survey_2.purpose) }
		specify { expect(render).to include(survey_2.audience) }
		specify { expect(render).to include(survey_2.template_name) }

		specify { expect(render).to have_css('a', text: 'Mostrar', count: 2) }
		specify { expect(render).to have_css('a', text: 'Editar', count: 2) }
		specify { expect(render).to have_css('a[data-method="delete"]', text: 'Eliminar', count: 2) }

		specify { expect(render).to have_css('a', text: 'Nueva Encuesta', count: 1) }
	end

end
