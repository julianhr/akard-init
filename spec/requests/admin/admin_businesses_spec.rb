require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::Businesses", type: :request do
	include_context 'model_attributes'

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :businesses

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:business_fixture) { businesses(:restaurant) }

	before(:example) do
		allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
		allow_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
		allow_any_instance_of(ImageExplorer).to receive(:delete_image) { true }
		login_as(admin)
	end

	it 'GET /businesses not logged in as admin' do
		logout
		get admin_businesses_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /businesses" do
			get admin_businesses_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /businesses/:id' do
			get admin_business_path(business_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'GET /businesses/new' do
			get new_admin_business_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /businesses/:id/edit' do
			get edit_admin_business_path(business_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'POST /businesses' do
			post admin_businesses_path, {admin_business: business_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::Business.last)
		end

		it 'PUT /businesses/:id' do
			business_fixture.name = 'Modified Name'
			put admin_business_path(business_fixture.id), {admin_business: business_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_business_path(business_fixture.id))
		end

		it 'DELETE /businesses/:id' do
			delete admin_business_path(business_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_businesses_path)
		end

		it 'POST /businesses/upload_logo' do
			post upload_logo_admin_business_path(business_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(business_fixture)
		end

		it 'DELETE /businesses/delete_logo' do
			delete delete_logo_admin_business_path(business_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(business_fixture)
		end
	end
end
