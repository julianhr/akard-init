require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::Coupons", type: :request do
	include_context 'model_attributes'

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :coupons
	fixtures :businesses
	fixtures :business_branches

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:coupon_fixture) { coupons(:sushi) }

	before(:example) do
		allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
		allow_any_instance_of(ImageExplorer).to receive(:get_image_file_names) { [] }
		allow_any_instance_of(ImageExplorer).to receive(:upload_image) { true }
		allow_any_instance_of(ImageExplorer).to receive(:delete_image) { true }
		login_as(admin)
	end

	it 'GET /coupons not logged in as admin' do
		logout
		get admin_coupons_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /coupons" do
			get admin_coupons_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/:id' do
			get admin_coupon_path(coupon_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/new' do
			get new_admin_coupon_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/:id/edit' do
			get edit_admin_coupon_path(coupon_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'POST /coupons' do
			post admin_coupons_path, {admin_coupon: coupon_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::Coupon.last)
		end

		it 'PUT /coupons/:id' do
			coupon_fixture.title = 'Modified Title'
			put admin_coupon_path(coupon_fixture.id), {admin_coupon: coupon_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_coupon_path(coupon_fixture.id))
		end

		it 'GET /coupons/:id/preview' do
			get preview_admin_coupon_path(coupon_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'DELETE /coupons/:id' do
			delete admin_coupon_path(coupon_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_coupons_path)
		end

		it 'POST /coupons/upload_image' do
			post upload_image_admin_coupon_path(coupon_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(coupon_fixture)
		end

		it 'DELETE /coupons/delete_image' do
			delete delete_image_admin_coupon_path(coupon_fixture.id), file: 'image_1.jpg'
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(coupon_fixture)
		end
	end
end
