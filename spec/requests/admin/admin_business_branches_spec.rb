require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::BusinessBranches", type: :request do
	include_context 'model_attributes'

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :business_branches
	fixtures :businesses
	fixtures :states

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:bb_fixture) { business_branches(:americas) }

	before(:example) { login_as(admin) }

	it 'GET /business_branches not logged in as admin' do
		logout
		get admin_business_branches_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /business_branches" do
			get admin_business_branches_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /business_branches/:id' do
			get admin_business_branches_path(bb_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'GET /business_branches/new' do
			get new_admin_business_branch_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /business_branches/:id/edit' do
			get edit_admin_business_branch_path(bb_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'POST /business_branches' do
			post admin_business_branches_path, {admin_business_branch: business_branch_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::BusinessBranch.last)
		end

		it 'PUT /business_branches/:id' do
			bb_fixture.street_name = 'Modifies Street Name'
			put admin_business_branch_path(bb_fixture.id), {admin_business_branch: bb_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_business_branch_path(bb_fixture.id))
		end

		it 'DELETE /business_branches/:id' do
			delete admin_business_branch_path(bb_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_business_branches_path)
		end
	end
end
