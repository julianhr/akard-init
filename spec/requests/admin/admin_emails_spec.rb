require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::Emails", type: :request do
	include_context "model_attributes"

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :emails

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:email_fixture) { emails(:new_coupon) }

	before(:example) { login_as(admin) }

	it 'GET /emails not logged in as admin' do
		logout
		get admin_emails_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to("#{root_path}agents/acceder")
	end

	context 'logged in' do 
		it 'GET /emails' do
			get admin_emails_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /emails/:id' do
			get admin_email_path(email_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/new' do
			get new_admin_email_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /emails/:id/edit' do
			get edit_admin_email_path(email_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'POST /emails' do
			post admin_emails_path, {admin_email: email_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::Email.last)
		end

		it 'PUT /emails/:id' do
			email_fixture.subject = 'new subject'
			put admin_email_path(email_fixture.id), {admin_email: email_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_email_path(email_fixture.id))
		end

		it 'DELETE /emails/:id' do
			delete admin_email_path(email_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_emails_path)
		end
	end

end
