require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::States", type: :request do
	include_context 'model_attributes'
	
	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :states

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:state_fixture) { states(:jalisco) }

	before(:example) { login_as(admin) }

	it 'GET /states not logged in as admin' do
		logout
		get admin_states_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /states" do
			get admin_states_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /states/:id' do
			get admin_state_path(state_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'GET /states/:id/edit' do
			get edit_admin_state_path(state_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'PUT /states/:id' do
			state_fixture.name = 'Modified State'
			put admin_state_path(state_fixture.id), {admin_state: state_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_state_path(state_fixture.id))
		end
	end

end
