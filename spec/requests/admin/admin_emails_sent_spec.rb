require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::EmailsSent", type: :request do
	include_context 'model_attributes'

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :emails_sent
	fixtures :emails
	fixtures :users

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:email_sent_fixtures) { emails_sent(:registration_successful) }

	before(:example) { login_as(admin) }

	it 'GET /emails_sent not logged in as admin' do
		logout
		get admin_emails_sent_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it 'GET /emails_sent' do
			get admin_emails_sent_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /emails_sent/:id' do
			get admin_email_sent_path(email_sent_fixtures)
			expect(response).to have_http_status(:success)
		end

		it 'GET /emails_sent/new' do
			get new_admin_email_sent_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /emails_sent/:id/edit' do
			get edit_admin_email_sent_path(email_sent_fixtures)
			expect(response).to have_http_status(:success)
		end

		it 'POST /emails_sent' do
			post admin_emails_sent_path, {admin_email_sent: email_sent_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::EmailSent.last)
		end

		it 'PUT /emails_sent/:id' do
			email_sent = email_sent_fixtures
			email_sent.metadata = '{testing: "testing string"}'
			put admin_email_sent_path(email_sent.id), {admin_email_sent: email_sent.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_email_sent_path(email_sent.id))
		end

		it 'DELETE /emails_sent/:id' do
			delete admin_email_sent_path(email_sent_fixtures.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_emails_sent_path)
		end
	end
end
