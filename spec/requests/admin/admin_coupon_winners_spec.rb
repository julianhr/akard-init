require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::CouponWinners", type: :request do
	include_context 'model_attributes'
	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :coupon_winners
	fixtures :coupons
	fixtures :users
	fixtures :emails_sent

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:coupon_winner_fixture) { coupon_winners(:pedro) }

	before(:example) { login_as(admin) }

	it 'GET /coupon_winners not logged in as admin' do
		logout
		get admin_coupon_winners_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /coupon_winners" do
			get admin_coupon_winners_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupon_winners/:id' do
			get admin_coupon_winner_path(coupon_winner_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupon_winners/new' do
			get new_admin_coupon_winner_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupon_winners/:id/edit' do
			get edit_admin_coupon_winner_path(coupon_winner_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'POST /coupon_winners' do
			post admin_coupon_winners_path, {admin_coupon_winner: coupon_winner_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::CouponWinner.last)
		end

		it 'PUT /coupon_winners/:id' do
			cw = coupon_winner_fixture
			cw.token = 'NEWTOKEN'
			put admin_coupon_winner_path(cw.id), {admin_coupon_winner: cw.attributes}

			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_coupon_winner_path(cw.id))
		end

		it 'DELETE /coupon_winners/:id' do
			cw = coupon_winner_fixture
			delete admin_coupon_winner_path(cw.id)

			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_coupon_winners_path)
		end
	end
end
