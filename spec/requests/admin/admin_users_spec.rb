require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::Users", type: :request do
	include_context "model_attributes"

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :admin_users

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:user_fixture) { admin_users(:pedro) }

	before(:example) { login_as(admin) }

	it 'GET /agents not logged in as admin' do
		logout
		get admin_users_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it "GET /coupons" do
			get admin_users_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/:id' do
			get admin_user_path(user_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/new' do
			get new_admin_user_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/:id/edit' do
			get edit_admin_user_path(user_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'POST /coupons' do
			post admin_users_path, {admin_user: user_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::User.last)
		end

		it 'PUT /coupons/:id' do
			user_fixture.email = 'my_new_email@email.com'
			put admin_user_path(user_fixture.id), {admin_user: user_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_user_path(user_fixture.id))
		end

		it 'DELETE /coupons/:id' do
			delete admin_user_path(user_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_users_path)
		end
	end

end
