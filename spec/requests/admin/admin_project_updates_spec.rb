require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::ProjectUpdates", type: :request do
	include_context "model_attributes"

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :project_updates
	fixtures :users

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:pu_fixture) { project_updates(:can_email) }

	before(:example) { login_as(admin) }

	it 'GET /project_updates not logged in as admin' do
		logout
		get admin_project_updates_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to("#{root_path}agents/acceder")
	end

	context 'logged in' do 
		it 'GET /project_updates' do
			get admin_project_updates_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /project_updates/:id' do
			get admin_project_update_path(pu_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /coupons/new' do
			get new_admin_project_update_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /project_updates/:id/edit' do
			get edit_admin_project_update_path(pu_fixture.id)
			expect(response).to have_http_status(:success)
		end

		it 'POST /project_updates' do
			post admin_project_updates_path, {project_update: project_update_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::ProjectUpdate.last)
		end

		it 'PUT /project_updates/:id' do
			pu_fixture.metadata = {new: 'new entry'}
			put admin_project_update_path(pu_fixture.id), {project_update: pu_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_project_update_path(pu_fixture.id))
		end

		it 'DELETE /project_updates/:id' do
			delete admin_project_update_path(pu_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_project_updates_path)
		end
	end
end
