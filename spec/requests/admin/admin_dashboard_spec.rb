require 'rails_helper'

RSpec.describe "Admin::Dashboard", type: :request do
	include Warden::Test::Helpers
	Warden.test_mode!

	let(:admin) { FactoryGirl.build(:admin_julian) }

	before(:example) { login_as(admin) }

	it 'GET /users not logged in as admin' do
		logout
		get admin_dashboard_index_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it 'GET /admin' do
			get admin_dashboard_index_path
			expect(response).to have_http_status(:success)
		end
	end

end
