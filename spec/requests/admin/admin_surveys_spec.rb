require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Surveys", type: :request do
	include_context "model_attributes"

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :surveys

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:survey_fixture) { surveys(:signup) }

	before(:example) { login_as(admin) }

	it 'GET /surveys not logged in as admin' do
		logout
		get admin_surveys_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it 'GET /surveys' do
			get admin_surveys_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys/:id' do
			get admin_survey_path(surveys(:signup))
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys/new' do
			get new_admin_survey_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys/:id/edit' do
			get edit_admin_survey_path(surveys(:signup))
			expect(response).to have_http_status(:success)
		end

		it 'POST /surveys' do
			post admin_surveys_path, {admin_survey: survey_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::Survey.last)
		end

		it 'PUT /surveys/:id' do
			survey_fixture.audience = 'businesses'
			put admin_survey_path(survey_fixture.id), {admin_survey: survey_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_survey_path(survey_fixture.id))
		end

		it 'DELETE /surveys/:id' do
			delete admin_survey_path(survey_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_surveys_path)
		end
	end

end
