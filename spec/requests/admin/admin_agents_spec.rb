require 'rails_helper'

RSpec.describe "Admin::Agents", type: :request do
	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :admin_agents

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:agent_fixtures) { admin_agents(:julian) }

	before(:example) { login_as(admin) }

	it 'GET /agents not logged in as admin' do
		logout
		get admin_agents_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it 'GET /agents' do
			get admin_agents_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /agents/:id' do
			get admin_agent_path(agent_fixtures.id)
			expect(response).to have_http_status(:success)
		end

		it 'GET /agents/:id/edit' do
			get edit_admin_agent_path(agent_fixtures.id)
			expect(response).to have_http_status(:success)
		end

		it 'PUT /agents/:id' do
			agent = agent_fixtures
			agent.email = 'email_changed@email.com'
			put admin_agent_path(agent.id), {admin_agent: agent.attributes}

			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_agent_path(agent.id))
		end

		it 'DELETE /agents/:id' do
			agent = agent_fixtures
			delete admin_agent_path(agent.id)

			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_agents_path)
		end
	end

end
