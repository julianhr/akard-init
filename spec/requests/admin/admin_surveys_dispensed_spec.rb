require 'rails_helper'
require 'support/model_attributes'

RSpec.describe "Admin::SurveysDispensed", type: :request do
	include_context 'model_attributes'

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :surveys_dispensed
	fixtures :surveys
	fixtures :users

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:sd_fixture) { surveys_dispensed(:signup) }

	before(:example) { login_as(admin) }

	it 'GET /surveys_dispensed not logged in as admin' do
		logout
		get admin_surveys_dispensed_index_path
		expect(response).to have_http_status(:found)
		expect(response).to redirect_to('/agents/acceder')
	end

	context 'logged in as admin' do 
		it 'GET /surveys_dispensed' do
			get admin_surveys_dispensed_index_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys_dispensed/:id' do
			get admin_surveys_dispensed_path(sd_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys_dispensed/new' do
			get new_admin_surveys_dispensed_path
			expect(response).to have_http_status(:success)
		end

		it 'GET /surveys_dispensed/:id/edit' do
			get edit_admin_surveys_dispensed_path(sd_fixture)
			expect(response).to have_http_status(:success)
		end

		it 'POST /surveys_dispensed' do
			post admin_surveys_dispensed_index_path, {admin_surveys_dispensed: survey_dispensed_valid_attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(Admin::SurveysDispensed.last)
		end

		it 'PUT /surveys_dispensed/:id' do
			sd_fixture.where = 'new_landing_page'
			put admin_surveys_dispensed_path(sd_fixture.id), {admin_surveys_dispensed: sd_fixture.attributes}
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_surveys_dispensed_path(sd_fixture.id))
		end

		it 'DELETE /surveys_dispensed/:id' do
			delete admin_surveys_dispensed_path(sd_fixture.id)
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(admin_surveys_dispensed_index_path)
		end
	end
end
