require 'rails_helper'

RSpec.describe "Promotion", type: :request do

	fixtures :coupons
	fixtures :coupon_winners
	fixtures :users

	let(:promotion_fixture) { coupons(:sushi) }

	before(:example) do
		allow_any_instance_of(ImageExplorer).to receive(:get_image_paths) { [] }
	end

	it 'GET /index' do
		get promotions_path
		expect(response).to have_http_status(:success)
	end

	it 'GET /show' do
		get promotion_path(promotion_fixture.id)
		expect(response).to have_http_status(:success)
	end

	it 'POST /claim' do
		mail_message_double = instance_double("Mail::Message")
		allow(mail_message_double).to receive(:errors) { [] }
		allow_any_instance_of(ActionMailer::MessageDelivery)
			.to receive(:deliver_later) { mail_message_double }
				
		valid_attributes = {ganador: {correo: '123@email.com'}}
		post promotion_path(promotion_fixture.id), valid_attributes
		promotion_winner = Admin::CouponWinner.last
		opt = {token: promotion_winner.token, email: valid_attributes[:ganador][:correo]}

		expect(response).to have_http_status(:found)
		expect(response).to redirect_to(promotion_winner_path(promotion_fixture.id, opt))
	end

	it 'GET /winner' do
		promotion_winner = coupon_winners(:pedro)
		opt = {token: promotion_winner.token, email: promotion_winner.user.email}
		get promotion_winner_path(promotion_fixture.id), opt
		expect(response).to have_http_status(:success)
	end

end
