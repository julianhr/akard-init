require 'rails_helper'

RSpec.describe "Registration", type: :request do

	include Warden::Test::Helpers
	Warden.test_mode!

	fixtures :users
	fixtures :surveys_dispensed
	fixtures :agents

	let(:admin) { FactoryGirl.build(:admin_julian) }
	let(:user_fixtures) { users(:pedro) }
	let(:sd_fixtures) { surveys_dispensed(:signup) }

	before(:example) do
		login_as(admin)
	end

	it 'GET /new' do
		get registro_nuevo_path
		expect(response).to have_http_status(:success)
	end

	it 'POST /create' do
		mail_message_double = instance_double("Mail::Message")
		allow(mail_message_double).to receive(:errors) { [] }
		allow_any_instance_of(ActionMailer::MessageDelivery)
			.to receive(:deliver_later) { mail_message_double }

		valid_attributes = {email: '123@email.com', email_status: 'can_email'}
		post registro_crear_path, {user: valid_attributes}
		user = User.last
		params = {id: user.id, email: user.email}

		expect(response).to have_http_status(:found)
		expect(response).to redirect_to( registro_exito_path(params) )
	end

	it 'GET /success' do
		get registro_exito_path, {id: user_fixtures.id, email: user_fixtures.email}
		expect(response).to have_http_status(:success)
	end

	it 'POST /survey' do
		valid_attributes = {
			user_id: users(:pedro).id,
			sd_id: surveys_dispensed(:signup).id,
			survey: {category: "laptops, electronics"}
		}
		post registro_encuesta_path, valid_attributes, {'HTTP_REFERER' => root_url}

		expect(response).to have_http_status(:found)
		expect(response).to redirect_to(promotions_url)
	end

end
