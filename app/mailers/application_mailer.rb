class ApplicationMailer < ActionMailer::Base
	default from: 'akard@akard.club'
	layout 'mailer'
end
