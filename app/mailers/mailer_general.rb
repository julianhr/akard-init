class MailerGeneral < ApplicationMailer

	SENDERS = {
		internal: 'internal@akard.club'
	}

	RECIPIENTS = {
		contact: 'julian1@cimarron.me'
	}

	def internal(msg, recipient) 
		begin
			raise TypeError, "msg is not of Message type." unless msg.class == Message
			raise TypeError, "Recipient is of incorrect type." unless [String, Symbol].include? recipient.class
			raise ArgumentError, "Recipient is not allowed." if RECIPIENTS[recipient].nil?
		rescue Exception => e
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
			return
		end

		mail(
			to: RECIPIENTS[recipient],
			from: SENDERS[:internal],
			reply_to: msg.email,
			subject: msg.subject,
			body: msg.body
		)
	end

	def user_registration(user_id, email_id, email_sent_id) 
		@user = User.where(id: user_id).first
		email = Admin::Email.where(id: email_id).first
		email_sent = Admin::EmailSent.where(id: email_sent_id).first

		validate_user_email_emailsent(@user, email, email_sent) or raise
		preprocess_emailsent(email_sent) or raise
		mail(to: @user.email, subject: email.subject)
		postprocess_emailsent(email_sent)
	end

	def user_coupon_winner(user_id, email_id, email_sent_id, promotion_id) 
		@winner = {user: User.where(id: user_id).first}
		@winner[:promotion] = Admin::Coupon.where(id: promotion_id).first
		@winner[:promo_winner] = Admin::CouponWinner.where(
			user_id: @winner[:user].id, coupon_id: @winner[:promotion].id
		).first
		url_opt = {token: @winner[:promo_winner].token, email: @winner[:user].email}
		@winner[:promo_url] = promotion_winner_url(@winner[:promotion].id, url_opt)
		email = Admin::Email.where(id: email_id).first
		email_sent = Admin::EmailSent.where(id: email_sent_id).first

		validate_user_email_emailsent(@winner[:user], email, email_sent) or raise
		@winner[:promotion] or raise
		preprocess_emailsent(email_sent) or raise
		mail(to: @winner[:user].email, subject: email.subject)
		postprocess_emailsent(email_sent)
	end

	private

		def validate_user_email_emailsent(user, email, email_sent) 
			if user.nil? or email.nil? or email_sent.nil?
				error_message = error_msg_user_email_emailsent(user, email, email_sent)
				ExceptionHandler.log(RuntimeError.new(error_message), :error, __FILE__, __LINE__)
				return false
			end

			if user.id != email_sent.user_id or email.id != email_sent.email_id
				error_message = error_msg_user_email_emailsent(user, email, email_sent)
				ExceptionHandler.log(RuntimeError.new(error_message), :error, __FILE__, __LINE__)
				return false
			end

			return true
		end

		def preprocess_emailsent(email_sent) 
			email_sent.attempts += 1
			email_sent.sent = false
			save_with_rescue_logger(email_sent) or return false
		end

		def postprocess_emailsent(email_sent) 
			email_sent.sent = true
			email_sent.status = 'sent'
			save_with_rescue_logger(email_sent) or return false
		end

		def save_with_rescue_logger(record) 
			return unless record.respond_to? :save!

			record.save!
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			return false
		end

		def error_msg_user_email_emailsent(user, email, email_sent) 
			error_message = "User: " + user.inspect + "\n"
			error_message += "Email: " + email.inspect + "\n"
			error_message += "EmailSent: " + email_sent.inspect

			return error_message
		end

end
