json.array!(@emails_sent) do |email_sent|
  json.extract! email_sent, :id, :email_id, :user_id, :sent, :status, :attempts, :response
  json.url email_sent_url(email_sent, format: :json)
end
