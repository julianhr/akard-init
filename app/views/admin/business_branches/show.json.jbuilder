json.extract! @business_branch, :id, :business_id, :type, :street_name, :street_no, :inner_no, :street_context, :borough, :city, :zip, :state_id, :latitude, :longitude, :created_at, :updated_at
