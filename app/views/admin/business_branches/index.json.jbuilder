json.array!(@business_branches) do |business_branch|
  json.extract! business_branch, :id, :business_id, :type, :street_name, :street_no, :inner_no, :street_context, :borough, :city, :zip, :state_id, :latitude, :longitude
  json.url business_branch_url(business_branch, format: :json)
end
