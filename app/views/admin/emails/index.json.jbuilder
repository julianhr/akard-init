json.array!(@emails) do |email|
  json.extract! email, :id, :model, :purpose, :subject, :template_name
  json.url email_url(email, format: :json)
end
