json.array!(@project_updates) do |project_update|
  json.extract! project_update, :id, :user_id, :email_status, :metadata
  json.url admin_project_update_url(project_update, format: :json)
end
