json.array!(@users) do |user|
  json.extract! user, :id, :email, :email_status, :no_email_desc
  json.url user_url(user, format: :json)
end
