json.array!(@surveys) do |survey|
  json.extract! survey, :id, :purpose, :audience, :template_name
  json.url survey_url(survey, format: :json)
end
