json.array!(@coupon_winners) do |coupon_winner|
  json.extract! coupon_winner, :id, :coupon_id, :user_id, :token, :email_sent_id, :metadata, :status, :redeemed_at
  json.url admin_coupon_winner_url(coupon_winner, format: :json)
end
