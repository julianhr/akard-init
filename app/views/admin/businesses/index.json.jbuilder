json.array!(@businesses) do |business|
  json.extract! business, :id, :name, :line_of_business, :url, :description, :address_id
  json.url business_url(business, format: :json)
end
