json.array!(@surveys_dispensed) do |surveys_dispensed|
  json.extract! surveys_dispensed, :id, :survey_id, :user_id, :where, :response
  json.url surveys_dispensed_url(surveys_dispensed, format: :json)
end
