require 'action_controller'

class ImageExplorer

	attr_reader :built

	DIR = {
		businesses: 'businesses',
		promotions: 'promotions'
	}

	FILE_TYPES = %w(jpg jpeg png)

	def initialize(dir, id) 
		@dir = dir
		@id = id
		@dir_path = nil
		@built = false
	end

	def build
		@dir = sanitize_dir(@dir)
		@id = sanitize_id(@id)
		@dir_path = build_directory_path(@dir, @id)
	rescue Exception => e
		ExceptionHandler.log(e, :error, __FILE__, __LINE__)
		raise e
	else
		@built = true
	end

	def get_image_paths
		check_build()
		paths = nil

		begin
			paths = Dir[@dir_path + "*.{#{FILE_TYPES.join(',')}}"]
		rescue Exception => e
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
			raise_generic_ioerror()
		end

		return paths.map {|path| path.split('/')[2..path.length-1].join('/')}.sort
	end

	def get_image_file_names 
		check_build()

		get_image_paths().map {|path| path.split('/').last}.sort
	rescue Exception => e
		raise e
	end

	def upload_image(img_file_name, image_io, file_class = File) 
		check_build()

		img_file_name = sanitize_img_file_name(img_file_name)
		file_path = @dir_path + img_file_name

		begin
			file_class.open(file_path, 'wb') {|file| file.write(image_io.read)}
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			raise IOError, "Hubo un error interno al intentar subir la imagen. El problema ya se reportó."
		end
	end

	def delete_image(img_file_name, file_class = File) 
		check_build()

		img_file_name = sanitize_img_file_name(img_file_name.to_s)
		file_path = @dir_path + img_file_name

		unless file_class.exist? file_path
			e = ArgumentError.new "La imagen '#{img_file_name}' no existe. Nada que borrar."
			ExceptionHandler.log(e, :error, __LINE__, __LINE__)
			raise e
		end

		begin
			file_class.delete(file_path)
		rescue Exception => e
			ExceptionHandler.log(e, :error, __LINE__, __LINE__)
			raise IOError.new "Hubo un error interno al intentar borrar la imagen. El problema ya se reportó."
		end
	end

	private

		def check_build
			build() unless @built
		end

		def sanitize_dir(dir)
			raise TypeError, "The dir '#{dir}' is of incorrect type." unless dir.respond_to? :to_sym

			if DIR.keys.include? dir.to_sym
				return dir
			else
				raise ArgumentError, "The directory '#{dir}' is an invalid option."
			end
		end

		def sanitize_id(id)
			raise TypeError, "The id '#{id}' of incorrect type." unless id.respond_to? :to_i

			if id.to_i > 0
				return id
			else
				raise ArgumentError, "The id '#{id}' is not > 0."
			end
		end

		def build_directory_path(dir, id) 
			dir_path = Pathname.new('public/images') + DIR[dir.to_sym] + id.to_s

			return dir_path if Dir.exist? dir_path

			begin
				Dir.mkdir dir_path
			rescue Exception => e
				raise e
			else
				return dir_path
			end
		end

		def sanitize_img_file_name(name) 
			name = name.strip
			file_types = FILE_TYPES.join('|')
			pattern = '\A[\w_-]+\.(file_types)\z'
			pattern.gsub!(/file_types/, file_types)
			pattern = Regexp.new pattern

			if !!(pattern =~ name)
				return name
			else
				raise ArgumentError, "The file name for the image is invalid."
			end
		end

		def raise_generic_ioerror 
			raise IOError, "Hubo un error interno. El problema ya se reportó."
		end

end
