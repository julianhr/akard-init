class EmailSqsUpdater

	attr_reader :body_hash

	def initialize(body, signature_version, master_email) 
		@body_json = body
		@signature_version = signature_version
		@master_email = master_email
		@body_hash = {}
		@built = false
	end

	def build()
		#body_json
		raise TypeError, 'body_json is not a String' if @body_json.class != String
		@body_json.strip!
		raise ArgumentError, 'body_json is empty' if @body_json.empty?

		# signature_version
		case @signature_version
		when String
			@signature_version.strip!
		when Fixnum
			@signature_version = @signature_version.to_s
		else
			raise TypeError, 'The signature_version is of incorrect type.'
		end

		# master_email
		raise TypeError, 'master_email is of incorrect type' if @master_email.class != Admin::Email

		#parse body
		@body_hash = parse_body()

		@built = true
	end

	def update 
		check_build()
		validate_signature_version()

		sqs_message = @body_hash['Message'] || {notificationType: ''}

		case sqs_message['notificationType']			
		when 'Delivery'		then update_delivery( sqs_message['delivery'] )
		when 'Bounce'			then update_bounce( sqs_message['bounce'] )
		when 'Complaint'	then update_complaint( sqs_message['complaint'] )
		else
			return false
		end

		return true
	rescue Exception => e
		ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
		return false		
	end

	private

		def check_build
			build() unless @built
		end

		def parse_body 
			hash = nil

			begin
				hash = JSON.parse @body_json
				hash['Message'] = JSON.parse(hash['Message'])
				return hash
			rescue
				raise
			end
		end

		def validate_signature_version 
			if @body_hash['SignatureVersion'] == @signature_version
				@signature_version
			else
				raise ArgumentError, "The 'signature_version' provided is not supported."
			end
		end

		def update_delivery(sqs_delivery) 
			update = {status: 'delivery'}
			emails = sqs_delivery['recipients']

			emails.each { |email| update_email_sent(email, update) }
		end

		def update_bounce(sqs_bounce) 
			bounce_type = bounce_type(sqs_bounce['bounceType'], sqs_bounce['bounceSubType'])
			update = {status: bounce_type}
			emails = []

			sqs_bounce['bouncedRecipients'].each do |recipient|
				emails << recipient['emailAddress']
			end
			emails.each { |email| update_email_sent(email, update) }
		end

		def bounce_type(type, subtype) 
			if type == 'Permanent' && subtype == 'General'
				return 'permanent_bounce'
			elsif type == 'Transient' && subtype == 'General'
				return 'temporary_bounce'
			elsif type == 'Permanent' && subtype == 'Suppressed'
				return 'suppressed'
			else
				return 'unknown_bounce'
			end
		end

		def update_complaint(sqs_complaint) 	
			update = {status: 'complained'}
			emails = []

			sqs_complaint['complainedRecipients'].each do |recipient|
				emails << recipient['emailAddress']
			end
			emails.each { |email| update_email_sent(email, update) }
		end

		def update_email_sent(email, update) 			
			if email_sent = get_email_sent(email)
				email_sent.status = update[:status]
				key = "#{Time.now.to_i}_aws_sqs"
				email_sent.response = {} if email_sent.response.nil?
				email_sent.response[key] = @body_hash
			else
				raise ArgumentError, "No Admin::EmailSent was found."
			end

			begin 
				email_sent.save
			rescue
				raise
			end
		end

		def get_email_sent(email)
			if user = User.where(email: email).first
				Admin::EmailSent.where(user_id: user.id, email_id: @master_email.id).first
			end
		end

	end
