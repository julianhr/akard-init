class SurveySignupProcessor

	OPT = {
		response_max_characters: 4_000
	}

	QUESTIONS = {
		category: 'any',
		subscribed: 'any',
		apps_use: [
			'En todo momento',
			'Seguido',
			'De vez en cuando',
			'Jamás'
		],
		age_range: [
			'16 - 25', 
			'26 - 35', 
			'36 - 45', 
			'Más de 46'
		],
		sex: [
			'Mujer', 
			'Hombre'
		],
		comments: 'any'
	}

	def initialize(survey, user_id, sd_id)
		@survey = survey
		@survey_dispensed = Admin::SurveysDispensed.where(id: sd_id).first
		@user = User.where(id: user_id).first
		@can_save = false
		@built = false
	end

	def build 
		if @survey.class != ActionController::Parameters
			raise TypeError, "The Survey passed is of incorrect type" 
		end

		if @survey_dispensed.nil?
			raise IndexError, "The survey_dispensed passed was not found" 
		end

		if @user.nil?
			raise IndexError, "The user passed was not found" 
		end

		@built = true
	end

	def verify_responses()
		check_build()

		max_length = OPT[:response_max_characters]
		verified_survey = {}

		QUESTIONS.each do |question, option|
			answer = @survey[question]
			answer.strip! if answer.class == String

			case option
			when 'any'
				next if answer.nil? or answer.empty?
				answer = answer[0...max_length] if answer.length > max_length
				verified_survey[question] = CGI.escape_html(answer)
			when Array
				next unless option.include?(answer)
				verified_survey[question] = answer
			end
		end

		@can_save = true
		@survey = verified_survey
	end

	def save
		if @can_save and @survey.any?
			@survey_dispensed.response = @survey
			
			begin
				@survey_dispensed.save!
			rescue
				raise
			end
		end
	end

	private

		def check_build
			build() unless @built
		end
end
