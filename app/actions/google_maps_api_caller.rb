class GoogleMapsApiCaller

	attr_reader :latitude, :longitude

	def initialize(latitude, longitude)
		@latitude 	= latitude
		@longitude 	= longitude
		@built = false
	end

	def build
		@latitude = sanitize_latitude(@latitude)
		@longitude = sanitize_longitude(@longitude)
		@built = true
	end

	def static_map_uri(size, zoom_level)
		check_build()

		size = sanitize_size(size)
		zoom_level = sanitize_zoom_level(zoom_level)

		URI::HTTPS.build(
			host: 'maps.googleapis.com',
			path: '/maps/api/staticmap',
			query: static_map_uri_query(size, zoom_level.to_i).to_param
		).to_s
	end

	def uri_to_maps_in_google(zoom_level) 
		check_build()

		zoom_level = sanitize_zoom_level(zoom_level)

		URI::HTTPS.build(
			host: 'www.google.com',
			path: "/maps/place/#{location()}/@#{location()},#{zoom_level.to_i}z",
		).to_s
	end
	
	private

		def sanitize_latitude(latitude) 
			begin 
				check_geo_coordinate(latitude)
			rescue
				raise TypeError, "The latitude provided is of incorrect type"
			end
		end

		def sanitize_longitude(longitude) 
			begin 
				check_geo_coordinate(longitude)
			rescue
				raise TypeError, "The longitude provided is of incorrect type"
			end
		end

		def check_geo_coordinate(coordiante)
			coordiante.strip! if coordiante.class == String

			if coordiante.respond_to? :to_f
				return coordiante
			else
				raise ArgumentError, "The argument does not respond to '#to_f'"
			end
		end

		def check_build
			build() unless @built
		end

		def sanitize_size(size)
			raise TypeError, "The size is not a String" unless size.class == String
			
			size.strip!

			if /\A\d+x\d+\z/ =~ size
				size
			else
				raise ArgumentError, "The size format is incorrect. It must be of the form '300x300'"
			end
		end

		def sanitize_zoom_level(zoom_level)
			case zoom_level
			when String
				zoom_level.strip!
				return zoom_level.to_i
			when Fixnum
				return zoom_level
			else
				raise TypeError, "The zoom level is of incorrect type."
			end
		end

		def location
			location ||= "#{@latitude},#{@longitude}"
		end

		def static_map_uri_query(size, zoom_level)
			{
				center: location(),
				zoom: zoom_level,
				markers: location(),
				size: size,
				language: 'es',
				key: ENV['GOOGLE_API_KEY']
			}
		end
end
