module PromotionHelper

	def business_address(promotion) 
		branch = promotion.business_branch
		br = '<br/>'
		address = "#{branch.street_name} #{branch.street_no}"
		address += " int. #{branch.inner_no}" if branch.inner_no?
		address += br
		address += branch.street_context + br if branch.street_context?
		address += "#{branch.borough}, #{branch.city}" + br
		address += branch.state.name + ", C.P. #{branch.zip}"
		unless branch.contact.nil?
			branch.contact.each_line do |line|
				line.chomp!
				next unless line.present?
				address += br + line
			end
		end

		return haml_tag :address, address.html_safe
	end

	def promotion_time_left_noscript(end_date) 
		time_left = {initial: ((end_date - Time.now).round)}
		time_in_sec = {sec: 1}
		calc_time = Proc.new do |time_left_key, time_to_sec_key|
			time_left[time_left_key] = time_left[:sec] / time_in_sec[time_to_sec_key]
			time_left[:sec] -= time_in_sec[time_to_sec_key] * time_left[time_left_key]
		end

		time_left[:initial] = 0 if time_left[:initial] < 0
		time_left[:sec] = time_left[:initial]

		time_in_sec[:min] = time_in_sec[:sec] * 60
		time_in_sec[:hr] = time_in_sec[:min] * 60
		time_in_sec[:day] = time_in_sec[:hr] * 24

		calc_time.call(:days, :day)
		calc_time.call(:hr, :hr)
		calc_time.call(:min, :min)

		if time_left[:initial] == 0
			'Tiempo agotado'
		elsif time_left[:initial] < time_in_sec[:hr]
			sprintf '%d min %d seg', time_left[:min], time_left[:sec]
		elsif time_left[:initial] < time_in_sec[:day]
			sprintf '%d hr %d min %d seg', time_left[:hr], time_left[:min], time_left[:sec]
		elsif time_left[:days] == 1
			sprintf '1 día %02d:%02d:%02d', time_left[:hr], time_left[:min], time_left[:sec]
		elsif time_left[:days] > 1
			sprintf '%d días %02d:%02d:%02d', time_left[:days], time_left[:hr], time_left[:min], time_left[:sec]
		end
	end

	def linked_business_map(size, zoom_level) 
		branch = @promotion.business_branch
		caller = GoogleMapsApiCaller.new(branch.latitude, branch.longitude)
		map_api_uri = caller.static_map_uri(size, zoom_level)
		uri_to_maps_in_google = caller.uri_to_maps_in_google(zoom_level)

		link_to(uri_to_maps_in_google, target: '_blank') do
			image_tag map_api_uri, alt: 'Mapa del Negocio'
		end
	end

	def print_signup_survey_link_if_unanswered(user) 
		unless user.class == User
			raise TypeError, "The user passed is not of type User."
		end
		
		survey = Admin::SurveysDispensed.where(
			user_id: user.id, 
			where: 'registro_exito'
		).first

		unless survey.present? and survey.response.present?
			haml_tag :p do
				haml_concat "Ayúdanos a conocer tus preferencias"
				haml_concat link_to(
					'en este enlace.', 
					registro_exito_path(email: user.email, id: user.id), 
					target: '_blank'
				)
			end
		end
	end

end
