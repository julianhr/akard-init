module Admin::BusinessesHelper

	def concatenate_address_from_branch(branch) 
		if branch.class != Admin::BusinessBranch
			raise TypeError, "Not of type Admin::BusinessBranch" 
		end

		address = branch.street_name + ' ' + branch.street_no
		address += ' int. ' + branch.inner_no if branch.inner_no?
		return address
	end

	def print_logos_from_paths(image_paths, tag, business)
		raise TypeError, "image_paths is not a collection" unless image_paths.respond_to? :each
		raise TypeError, "tag not of type String or Symbol" unless tag.respond_to? :to_sym
		raise TypeError, "business is not of type Admin::Business" if business.class != Admin::Business

		image_paths.each do |path|
			haml_tag(tag.to_sym) do
				haml_concat(link_to image_tag(path, size: '200x200'), root_url + 'images/' + path)
				haml_tag :span, 
					"Tamaño: #{number_to_human_size(File.size "public/images/" + path)}"
				img_file_name = path.split('/').last
				haml_concat(
					link_to 'Eliminar', 
					delete_logo_admin_business_path(business.id, file: img_file_name), 
					method: :delete
				)
			end
		end
	end

end
