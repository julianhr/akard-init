module Admin::CouponsHelper

	def print_images_from_paths(image_paths, tag, coupon)
		raise TypeError, "image_paths is not a collection" unless image_paths.respond_to? :each_with_index
		raise TypeError, "tag not of type String or Symbol" unless tag.respond_to? :to_sym
		raise TypeError, "coupon is not of type Admin::Coupon" if coupon.class != Admin::Coupon

		image_paths.each_with_index do |path, index|
			haml_tag(tag.to_sym) do
				haml_tag :span, "#{index + 1})"
				haml_concat(link_to image_tag(path, size: '200x200'), root_url + 'images/' + path)
				haml_tag :span, 
					"Tamaño: #{number_to_human_size(File.size "public/images/" + path)}"
				img_file_name = path.split('/').last
				haml_concat(
					link_to 'Eliminar', 
					delete_image_admin_coupon_path(coupon.id, file: img_file_name), 
					method: :delete
				)
			end
		end
	end

	def form_select_for_business(form) 
		return if @businesses.nil?

		businesses_list = @businesses.map {|b| [b.name, b.id]}
		@options = {
			selected: params[:coupon_business_id],
			include_blank: true
		}

		if @coupon.business_id? and params[:coupon_business_id].nil?
			@options[:selected] = @coupon.business_id
		end

		return form.select :business_id, businesses_list, @options
	end

end
