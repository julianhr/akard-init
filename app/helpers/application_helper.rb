module ApplicationHelper

	ADMIN_PATH_REGEX = /\A\/admin(\/.*)*\z/

	def get_title(path)
		opt_h = {
			'user' => 'AKard',
			'admin' => 'AKard - Admin'
		}

		case path
		when ADMIN_PATH_REGEX
			opt_h['admin']
		else
			opt_h['user']
		end
	end

	# Returns the corresponding robots meta tag in Haml depending on the URL path.
	def set_robots_meta_tag(path)
		case path
		when ADMIN_PATH_REGEX
			haml_tag :meta, {name: 'robots', content: 'noindex, nofollow, noarchive'}
		else
			nil
		end
	end

	def global_notifications()
		return unless notice or alert

		outer_div_opt = {
			class: "alert-box info", 
			data: {alert: true},
			aria: {live: 'assertive'},
			role: 'alertdialog',
			tabindex: 0
		}

		button_opt = {
			tabindex: 0, 
			class: 'close', 
			aria: {label: 'Close Alert'}
		}
		
		haml_tag :div, outer_div_opt do
			haml_concat notice if notice
			haml_concat alert if alert
			haml_tag :button, "&times;".html_safe, button_opt
		end
	end

	def wrap_lines_with_tag(text, tag, klass = nil)
		return if text.nil?
		raise TypeError, "the text is not of type String" unless text.class == String
		raise TypeError, "the tag is of incorrect type" unless tag.respond_to? :to_sym

		text.each_line do |line|
			line.chomp!
			next unless line.present?
			haml_tag tag.to_sym, line, class: klass
		end
	end

	def format_datetime(datetime) 
		unless datetime.respond_to? :to_datetime
			raise TypeError, "The datetime does not respond to #to_datetime" 
		end

		return l(datetime, format: "%A, %d/%B/%Y, %l:%M %p")
	end

	def form_field(form, field_type, attr_name, errors, options = {}) 
		raise TypeError "form is not of type FormBuilder" unless form.class == ActionView::Helpers::FormBuilder
		raise TypeError "field_type does not respond to #to_sym" unless field_type.respond_to? :to_sym
		raise TypeError "attr_name does not respond to #to_sym" unless attr_name.respond_to? :to_sym
		raise TypeError "errors is not of type ActiveModel::Errors" unless errors.class == ActiveModel::Errors
		raise TypeError, "options is not of type Hash" unless options.class == Hash


		field_type = field_type.to_sym
		attr_name = attr_name.to_sym
		css_class = nil
		error = ''

		if errors[attr_name].present?
			css_class = 'field-error' 
			error = errors[attr_name].first
		end

		haml_tag :div, class: css_class do
			haml_concat(form.label attr_name, options[:label_text])
			haml_concat(form.send field_type, attr_name, (options[:field] or {}))
			haml_tag :span, error if error.present?
		end
	end

	def generic_logo_section(tag, title) 
		haml_tag :div, class: 'column medium-2' do
			haml_concat(
				link_to(promotions_path) do
					image_tag 'general/logo-80.png', alt: 'Logo'
				end
			)
		end
		haml_tag :div, class: 'column medium-8 end' do
			haml_tag tag.to_sym, title, class: 'text-center title-generic'
		end
	end

end
