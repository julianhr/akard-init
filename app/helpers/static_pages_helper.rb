module StaticPagesHelper

	def static_logo_section(tag, title) 
		haml_tag :div, class: 'column medium-2' do
			haml_concat(
				link_to(promotions_path) do
					image_tag 'general/logo-80.png', alt: 'Logo', class: 'logo-akard'
				end
			)
		end
		haml_tag :div, class: 'column medium-8 end' do
			haml_tag tag.to_sym, title, class: 'text-center title-static'
		end
	end

end
