class Message
	include ActiveModel::Model

	attr_accessor :name, :email, :subject, :body

	validates :name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :email,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		format: {
			with: Util::REGEX[:email],
			message: ErrorMsg::MSG[:email_invalid]
		}

	validates :subject,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :body,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		length: {
			maximum: 4_000,
			message: ErrorMsg::MSG[:text_too_long]
		}

end
