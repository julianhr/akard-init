class Agent < ActiveRecord::Base
	
	# Include default devise modules. Others available are:
	# :registerable, :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

	# Validations
	validates :first_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:text_too_long]
		}

	validates :last_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:text_too_long]
		}

	validates :email,
		presence: {
			message: ErrorMsg::MSG[:email_missing]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:email_too_long]
		},
		format: {
			with: Util::REGEX[:email],
			message: ErrorMsg::MSG[:email_invalid]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:email_duplicity],
			case_sensitive: false
		}

end
