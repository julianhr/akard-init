class User < ActiveRecord::Base
	has_many :emails_sent, dependent: :destroy, class_name: 'Admin::EmailSent'
	has_many :project_updates, dependent: :destroy, class_name: 'Admin::ProjectUpdate'
	has_many :coupon_winners, dependent: :destroy, class_name: 'Admin::CouponWinner'
	has_many :surveys_dispensed, dependent: :destroy, class_name: 'Admin::SurveysDispensed'

	validates :email,
		presence: {
			message: ErrorMsg::MSG[:email_missing_cry]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:email_too_long]
		},
		format: {
			with: Util::REGEX[:email],
			message: ErrorMsg::MSG[:email_invalid]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:email_duplicity],
			case_sensitive: false
		}
end
