class Admin::Business < ActiveRecord::Base
	self.table_name = "businesses"
	
	has_many :business_branches, dependent: :destroy
	has_many :coupons, dependent: :destroy

	validates :name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :line_of_business,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :url,
		unless: 'url.empty?',
		format: {
			with: Util::REGEX[:url],
			message: ErrorMsg::MSG[:url_invalid]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:url_duplicity],
			case_sensitive: false
		}

end
