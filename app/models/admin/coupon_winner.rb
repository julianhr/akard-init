class Admin::CouponWinner < ActiveRecord::Base
	self.table_name = "coupon_winners"

	belongs_to :coupon
	belongs_to :user
	belongs_to :email_sent

	STATUS_OPT = [
		'visible'
	]

	validates :coupon_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :coupon_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :user_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :user_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :token,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :email_sent_id,
		allow_nil: true,
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :email_sent_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :status,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {			
			in: STATUS_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	def coupon_ids
		Admin::Coupon.all.pluck(:id)
	end

	def user_ids 
		User.all.pluck(:id)
	end

	def email_sent_ids 
		Admin::EmailSent.all.pluck(:id)
	end

end
