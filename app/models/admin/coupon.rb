class Admin::Coupon < ActiveRecord::Base
	self.table_name = "coupons"
	
	belongs_to :business_branch
	belongs_to :business
	has_many :coupon_winners, dependent: :destroy

	TYPE_OF_COUPON_OPT = [
		'sucursal_especifica',
		'todas_las_sucursales'
	]

	validates :business_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :business_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :business_branch_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {
			in: :business_branch_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :type_of_coupon,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {			
			in: TYPE_OF_COUPON_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :publish_active,
		allow_nil: true,
		inclusion: { in: [true, false] }

	validates :publish_start_datetime,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :publish_end_datetime,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :valid_start_datetime,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :valid_end_datetime,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :title,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:text_too_long]
		}

	validates :description,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :price_regular,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			greater_than: 0,
			message: ErrorMsg::MSG[:number_currency_gr_zero]
		}

	validates :price_promo,
		allow_nil: true,
		numericality: {
			greater_than_or_equal_to: 0,
			message: ErrorMsg::MSG[:number_currency]
		}

	validates :quantity,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			greater_than_or_equal_to: 0,
			message: ErrorMsg::MSG[:number_digits_positive]
		}

	validates :available,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			greater_than_or_equal_to: 0,
			message: ErrorMsg::MSG[:number_digits_positive],
		}

	validates :conditions,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validate(
		:publish_end_datetime_conditions,
		:valid_end_datetime_conditions,
		:exchanged_datetime_conditions,
		:available_conditions,
		:price_promo_conditions
	) 

	def business_ids
		Admin::Business.all.pluck(:id)
	end

	def business_branch_ids 
		Admin::BusinessBranch.where(business_id: business_id).pluck(:id)
	end

	def publish_end_datetime_conditions
		return if publish_end_datetime.nil?
		return if publish_start_datetime.nil?

		if publish_end_datetime <= publish_start_datetime
			errors.add(:publish_end_datetime, ErrorMsg::MSG[:date_end_lt_or_eq_to_start])
		end
	end

	def valid_end_datetime_conditions
		return if valid_end_datetime.nil? or valid_start_datetime.nil? or publish_end_datetime.nil?

		if valid_end_datetime <= valid_start_datetime
			errors.add(:valid_end_datetime, ErrorMsg::MSG[:date_end_lt_or_eq_to_start])
		end

		if valid_end_datetime < publish_end_datetime
			errors.add(:valid_end_datetime, ErrorMsg::MSG[:date_valid_lt_publish])
		end
	end

	def exchanged_datetime_conditions
		return if exchanged_datetime.nil? or valid_start_datetime.nil?

		if exchanged_datetime <= valid_start_datetime
			errors.add(:exchanged_datetime, ErrorMsg::MSG[:date_exchanged_lt_valid_start])
		end
	end

	def available_conditions
		return if available.nil? or quantity.nil?

		if available > quantity
			errors.add(:available, ErrorMsg::MSG[:coupon_availability_out_of_range])
		end
	end

	def price_promo_conditions
		return if price_promo.nil? or price_regular.nil?

		if price_promo >= price_regular
			errors.add(:price_promo, ErrorMsg::MSG[:coupon_price_promo_gt_regular])
		end
	end

end
