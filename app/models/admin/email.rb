class Admin::Email < ActiveRecord::Base
	self.table_name = "emails"

	has_many :emails_sent, dependent: :destroy

	MODEL_OPT = [
		'users',
		'coupons'
	]

	validates :model,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {
			in: MODEL_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :purpose,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:not_unique],
			case_sensitive: false
		}

	validates :subject,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :template_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

end
