class Admin::Agent < ActiveRecord::Base
	self.table_name = "agents"

	validates :first_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :last_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :email,
		presence: {
			message: ErrorMsg::MSG[:email_missing]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:email_too_long]
		},
		format: {
			with: Util::REGEX[:email],
			message: ErrorMsg::MSG[:email_invalid]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:email_duplicity],
			case_sensitive: false
		}

end
