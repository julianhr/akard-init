class Admin::State < ActiveRecord::Base
	self.table_name = "states"

	has_many :business_branches, dependent: :nullify

	validates :name, 
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

end
