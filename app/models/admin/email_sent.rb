class Admin::EmailSent < ActiveRecord::Base
	self.table_name = "emails_sent"
	
	belongs_to :email
	belongs_to :user
	has_many :coupon_winners, dependent: :destroy

	STATUS_OPT = {
		staged_for_send: nil,
		sent: nil,
		errors: nil,
		delivery: nil,
		permanent_bounce: 'review',
		temporary_bounce: 'retry',
		unknown_bounce: 'review',
		suppressed: 'review',
		complained: 'review'
	}


	validates :status,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {
			in: STATUS_OPT.keys.map{|x| x.to_s},
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :attempts,
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_only]
		}

end
