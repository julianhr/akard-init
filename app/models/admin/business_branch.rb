class Admin::BusinessBranch < ActiveRecord::Base
	self.table_name = "business_branches"
	
	belongs_to :business
	belongs_to :state
	has_many :coupons, dependent: :destroy

	TYPE_OF_BRANCH_OPT = [
		'sucursal',
		'matriz',
		'matriz y sucursal'
	]

	validates :business_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :business_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :type_of_branch,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {
			in: TYPE_OF_BRANCH_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :street_name,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :street_no,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :borough,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :city,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :zip,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_only]
		},
		length: {
			is: 5,
			message: ErrorMsg::MSG[:number_zip]
		}

	validates :state_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :state_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :latitude,
		allow_nil: true,
		numericality: {
			message: ErrorMsg::MSG[:number_only]
		}

	validates :longitude,
		allow_nil: true,
		numericality: {
			message: ErrorMsg::MSG[:number_only]
		}

	def business_ids
		Admin::Business.all.pluck(:id)
	end

	def state_ids
		Admin::State.all.pluck(:id)
	end

end
