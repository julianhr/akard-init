class Admin::SurveysDispensed < ActiveRecord::Base
	self.table_name = "surveys_dispensed"
	
	belongs_to :survey
	belongs_to :user

	validates :user_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :user_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :survey_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_positive]
		},
		inclusion: {			
			in: :survey_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :where,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	def user_ids
		User.all.pluck(:id)
	end

	def survey_ids
		Admin::Survey.all.pluck(:id)
	end

end
