class Admin::Survey < ActiveRecord::Base
	self.table_name = "surveys"
	
	has_many :surveys_dispensed, dependent: :destroy

	AUDIENCE_OPT = %w(users businesses)

	validates :purpose, 
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

	validates :audience, 
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {
			in: AUDIENCE_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :template_name, 
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		}

end
