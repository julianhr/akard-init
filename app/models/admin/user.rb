class Admin::User < ActiveRecord::Base
	self.table_name = "users"

	has_many :emails_sent, dependent: :destroy
	has_many :project_updates, dependent: :destroy
	has_many :coupon_winners, dependent: :destroy
	has_many :surveys_dispensed, dependent: :destroy

	validates :email,
		presence: {
			message: ErrorMsg::MSG[:email_missing]
		},
		length: {
			maximum: 254,
			message: ErrorMsg::MSG[:email_too_long]
		},
		format: {
			with: Util::REGEX[:email],
			message: ErrorMsg::MSG[:email_invalid]
		},
		uniqueness: {
			message: ErrorMsg::MSG[:email_duplicity],
			case_sensitive: false
		}

end
