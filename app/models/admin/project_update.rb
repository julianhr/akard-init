class Admin::ProjectUpdate < ActiveRecord::Base
	self.table_name = "project_updates"

	belongs_to :user

	EMAIL_STATUS_OPT = [
		'habilitado',
		'revisar',
		'no_correos'
	]

	validates :user_id,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		numericality: {
			only_integer: true,
			message: ErrorMsg::MSG[:number_digits_only]
		},
		inclusion: {			
			in: :user_ids,
			message: ErrorMsg::MSG[:invalid_option]
		}

	validates :email_status,
		presence: {
			message: ErrorMsg::MSG[:empty_field]
		},
		inclusion: {
			in: EMAIL_STATUS_OPT,
			message: ErrorMsg::MSG[:invalid_option]
		}

	def user_ids
		Admin::User.all.pluck(:id)
	end
end
