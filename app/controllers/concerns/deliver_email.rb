module DeliverEmail
	extend ActiveSupport::Concern

	def deliver_emailsent(message_delivery, delivery, email_sent) 
		delivery_job = message_delivery.send delivery

		if delivery_job.respond_to?(:errors) and delivery_job.errors.present?
			email_sent.response[:errors] = delivery_job.errors
			email_sent.status = 'errors'
		else
			email_sent.response[:sent_time] = Time.now.to_i
			email_sent.status = 'sent'
		end

		email_sent.save!
		return true
	rescue Exception => e
		ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
		return false
	end
end
