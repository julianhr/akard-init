class PromotionsController < ApplicationController
	include DeliverEmail

	before_action :set_promotion, only: [:show, :claim, :winner]
	before_action :set_promotion_img_explorer, only: [:show, :winner, :claim]
	before_action :set_business_img_explorer, only: [:show, :winner, :claim]

	# GET /promotions
	def index 
		@promotions = Admin::Coupon.where(
			"publish_active = ? AND publish_start_datetime <= ? AND publish_end_datetime > ?",
			true,
			DateTime.now,
			DateTime.now
		)
		@referrer = flash[:referrer]
	end

	# GET /promotions/:id
	def show
		begin
			@promotion_img_paths = @promotion_img_explorer.get_image_paths()
			@business_img_path = @business_img_explorer.get_image_paths().last || ''
		rescue Exception => e
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
			notice = "Hubo un error al buscar la promoción. El error ya se reportó."
			redirect_to promotions_url, notice: notice
		end
	end

	# POST /promotions/:id
	def claim
		# variables
		@user = nil
		@promotion_winner = nil
		@survey = nil
		@promotion_img_paths = nil
		@business_img_path = nil
		@email = nil
		@email_sent = nil
		params_email = nil
		repeat_winner = nil

		# check if any promotions left
		if @promotion.available <= 0
			notice = ErrorMsg::MSG[:promo_depleted]
			redirect_to :back, notice: notice and return
		end

		# if winner email in params, assign @user if found in User
		if params[:ganador][:correo]
			params_email = params[:ganador][:correo].strip
			@user = User.where(email: params_email).first
		end

		# if winner email not found in User, then create new User
		if @user.nil?
			@user = User.new(email: params_email)
			begin
				@user.save!
			rescue ActiveRecord::RecordInvalid
				@promotion_img_paths = @promotion_img_explorer.get_image_paths()
				@business_img_path = @business_img_explorer.get_image_paths().last || ''
				render :show and return
			rescue Exception => e
				ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
				notice = "Hubo un error registrando el correo. Favor de volver a intentar."
				redirect_to :back, notice: notice and return
			end

			# save signup survey for new user
			@survey = new_signup_survey(@user.id)
			begin
				@survey.save!
			rescue Exception => e
				ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			end
		end

		# if repeat winner, then reject
		repeat_winner = !!Admin::CouponWinner.where(coupon_id: @promotion.id, user_id: @user.id).first
		if repeat_winner
			@promotion_img_paths = @promotion_img_explorer.get_image_paths()
			@business_img_path = @business_img_explorer.get_image_paths().last || ''
			flash[:notice] = "Este usuario ya se ganó esta promoción."
			render :show and return
		end

		# save the winner and decrease availability by 1 in one transaction
		@promotion_winner = Admin::CouponWinner.new(
			coupon_id: @promotion.id,
			user_id: @user.id,
			token: SecureRandom.urlsafe_base64(32),
			status: 'visible'
		)
		@promotion.available -= 1
		begin
			Admin::Coupon.transaction do
				@promotion_winner.save!
				@promotion.save!
			end
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			alert = "Hubo un error al procesar la información. Favor de volver a intentar."
			redirect_to :back, alert: alert and return
		end

		# send email to winner
		@email = Admin::Email.where(purpose: 'usuario_gano_cupon').first
		@email_sent = new_email_sent(@user.id, @email.id) unless @email.nil?
		unless @email.nil? or @email_sent.nil?			
			begin
				@email_sent.save!
				message_delivery = MailerGeneral.user_coupon_winner(
					@user.id, 
					@email.id, 
					@email_sent.id,
					@promotion.id
				)
				deliver_emailsent(message_delivery, :deliver_later, @email_sent)
			rescue Exception => e
				ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			end
		end

		# redirect to winner path
		opt = {token: @promotion_winner.token, email: @user.email}
		redirect_to promotion_winner_url(@promotion.id, opt)
	end

	# GET /promotions/:id/winner
	def winner 
		# variables
		@promotion
		@promotion_img_explorer
		@business_img_explorer
		@qr = RQRCode::QRCode.new("http://akard.club/promociones/#{@promotion.id}", :size => 3, :level => :m)
		@promotion_img_path = @promotion_img_explorer.get_image_paths().first
		@logo_img_path = @business_img_explorer.get_image_paths().first
		@user = nil
		@promotion_winner = nil
		params_coupon_id = sanitize_id(params[:id])
		params_email = sanitize_email(params[:email])
		params_token = sanitize_token(params[:token])
		notice = ''
		
		# check params variables
		if params_coupon_id.nil? or params_email.nil? or params_token.nil?
			notice = "La información proporcionada contiene errores."
			redirect_to promotions_url, notice: notice and return 
		end

		# lookup user
		unless @user = User.where(email: params_email).first
			notice = "El usuario proporcionado no se puedo encontrar."
			redirect_to promotions_url, notice: notice and return
		end

		# lookup coupon_winner
		@promotion_winner = Admin::CouponWinner.where(
			coupon_id: params_coupon_id, 
			user_id: @user.id
		).first
		if @promotion_winner.nil? or @promotion_winner.token != params_token
			notice = "La información proporcionada no corresponde."
			redirect_to promotions_url, notice: notice and return
		end
	end

	private

		def set_promotion
			@promotion = Admin::Coupon.where(id: sanitize_id(params[:id])).first
			notice = "Lo sentimos pero el cupón buscado no existe."
			redirect_to promotions_url, notice: notice if @promotion.nil?
		end

		def set_promotion_img_explorer
			@promotion_img_explorer = ImageExplorer.new('promotions', @promotion.id)
		end

		def set_business_img_explorer
			@business_img_explorer = ImageExplorer.new('businesses', @promotion.business_id)
		end

		def sanitize_id(id) 
			id.strip! if id.class == String
			return nil unless id.respond_to? :to_i

			return id
		end

		def sanitize_token(token) 
			if token.class == String
				token.strip!
			else
				return nil
			end

			if token.empty?
				return nil
			else
				return token
			end
		end

		def sanitize_email(email)
			if email.class == String
				email.strip!
			else
				return nil
			end

			return nil if email.length > 254
			return nil unless Util::REGEX[:email] =~ email

			return email
		end

		def registration_survey 
			Admin::Survey.where(purpose: 'registro_exito', audience: 'users').first
		end

		def new_signup_survey(user_id) 
			Admin::SurveysDispensed.new(
				survey_id: registration_survey().id,			
				user_id: user_id,
				where: 'registro_exito'
			)
		end

		def new_email_sent(user_id, email_id)
			Admin::EmailSent.new(
				user_id: user_id, 
				email_id: email_id, 
				status: 'staged_for_send',
				attempts: 0, 
				response: {}
			)
		end

end
