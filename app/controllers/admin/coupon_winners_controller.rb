class Admin::CouponWinnersController < ApplicationController
	
	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_admin_coupon_winner, only: [:show, :edit, :update, :destroy]

	# GET /admin/coupon_winners
	# GET /admin/coupon_winners.json
	def index
		@coupon_winners = Admin::CouponWinner.all
	end

	# GET /admin/coupon_winners/1
	# GET /admin/coupon_winners/1.json
	def show
	end

	# GET /admin/coupon_winners/new
	def new
		@coupon_winner = Admin::CouponWinner.new
	end

	# GET /admin/coupon_winners/1/edit
	def edit
	end

	# POST /admin/coupon_winners
	# POST /admin/coupon_winners.json
	def create
		@coupon_winner = Admin::CouponWinner.new(admin_coupon_winner_params)

		respond_to do |format|
			if @coupon_winner.save
				format.html { redirect_to @coupon_winner, notice: 'Coupon winner was successfully created.' }
				format.json { render :show, status: :created, location: @coupon_winner }
			else
				format.html { render :new }
				format.json { render json: @coupon_winner.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /admin/coupon_winners/1
	# PATCH/PUT /admin/coupon_winners/1.json
	def update
		respond_to do |format|
			if @coupon_winner.update(admin_coupon_winner_params)
				format.html { redirect_to @coupon_winner, notice: 'Coupon winner was successfully updated.' }
				format.json { render :show, status: :ok, location: @coupon_winner }
			else
				format.html { render :edit }
				format.json { render json: @coupon_winner.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /admin/coupon_winners/1
	# DELETE /admin/coupon_winners/1.json
	def destroy
		@coupon_winner.destroy
		respond_to do |format|
			format.html { redirect_to admin_coupon_winners_url, notice: 'Coupon winner was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_admin_coupon_winner
			@coupon_winner = Admin::CouponWinner.where(id: params[:id]).first
			notice = "El ganador del cupón buscado no se encontró."
			redirect_to(admin_coupon_winners_url, notice: notice) if @coupon_winner.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def admin_coupon_winner_params
			params.require(:admin_coupon_winner).permit(:coupon_id, :user_id, :token, :email_sent_id, :status, :redeemed_at, :metadata)
		end
end
