class Admin::BusinessesController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_business, only: [:show, :edit, :update, :destroy, :delete_logo, :upload_logo]

	# GET /businesses
	# GET /businesses.json
	def index
		@businesses = Admin::Business.all
	end

	# GET /businesses/1
	# GET /businesses/1.json
	def show	
		@branches = Admin::BusinessBranch.where(business_id: @business.id)
		@referrer_opt = {referring_business_id: @business.id}
		@image_paths = ImageExplorer.new('businesses', @business.id).get_image_paths
	rescue Exception => e
		redirect_to admin_path, notice: e.message
	end

	# GET /businesses/new
	def new
		@business = Admin::Business.new
		@referrer_opt = {}

		@referrer_opt[:business_id] = params['business_id'].strip if params['business_id']
	end

	# GET /businesses/1/edit
	def edit
	end

	# POST /businesses
	# POST /businesses.json
	def create
		@business = Admin::Business.new(business_params)

		respond_to do |format|
			if @business.save
				format.html { redirect_to @business, notice: 'El negocio se creó con éxito.' }
				format.json { render :show, status: :created, location: @business }
			else
				format.html { render :new }
				format.json { render json: @business.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /businesses/1
	# PATCH/PUT /businesses/1.json
	def update
		respond_to do |format|
			if @business.update(business_params)
				format.html { redirect_to @business, notice: 'El negocio se actualizó con éxito.' }
				format.json { render :show, status: :ok, location: @business }
			else
				format.html { render :edit }
				format.json { render json: @business.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /businesses/1
	# DELETE /businesses/1.json
	def destroy
		@business.destroy
		respond_to do |format|
			format.html { redirect_to admin_businesses_url, notice: 'El negocio se eliminó con éxito.' }
			format.json { head :no_content }
		end
	end

	# POST businesses/1/upload_logo
	def upload_logo 
		# variables
		@image_io = params[:logo]
		@img_explorer = ImageExplorer.new('businesses', @business.id)
		file_name = nil
		notice = ''

		# check for correct @image_io type
		if @image_io.respond_to? :original_filename
			file_name = "logo_#{@business.id}_#{Time.now.strftime("%Y-%m-%d")}.#{@image_io.original_filename.split('.').last.downcase}"
		else
			notice = 'Hubo un error al intentar procesar el logo.'
			redirect_to(@business, notice: notice) and return
		end

		# attempt to upload the image
		begin
			@img_explorer.upload_image(file_name, @image_io)
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			redirect_to(@business, notice: e.message)
		else
			redirect_to @business, notice: 'El logo se agregó con éxito.'
		end
	end

	# DELETE businsses/1/delete_logo
	def delete_logo 
		@img_explorer = ImageExplorer.new('businesses', @business.id)

		begin
			@img_explorer.delete_image(params[:file])
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			redirect_to(@business, notice: e.message)
		else
			redirect_to @business, notice: 'El logo se eliminó con éxito'
		end
	end	

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_business
			@business = Admin::Business.where(id: params[:id]).first
			notice = "El negocio buscado no existe."
			redirect_to(admin_businesses_url, notice: notice) if @business.nil?
		end

		def set_img_explorer
			@img_explorer = ImageExplorer.new('businesses', @business.id)
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def business_params
			params.require(:admin_business).permit(:name, :line_of_business, :url, :description, :address_id)
		end
end
