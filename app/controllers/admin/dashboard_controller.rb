class Admin::DashboardController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!

	def index
	end

end
