class Admin::SurveysController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_survey, only: [:show, :edit, :update, :destroy]

	# GET /surveys
	# GET /surveys.json
	def index
		@surveys = Admin::Survey.order(:id)
	end

	# GET /surveys/1
	# GET /surveys/1.json
	def show
	end

	# GET /surveys/new
	def new
		@survey = Admin::Survey.new
	end

	# GET /surveys/1/edit
	def edit
	end

	# POST /surveys
	# POST /surveys.json
	def create
		@survey = Admin::Survey.new(survey_params)

		respond_to do |format|
			if @survey.save
				format.html { redirect_to @survey, notice: 'La encuesta se creó con éxito.' }
				format.json { render :show, status: :created, location: @survey }
			else
				format.html { render :new }
				format.json { render json: @survey.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /surveys/1
	# PATCH/PUT /surveys/1.json
	def update
		respond_to do |format|
			if @survey.update(survey_params)
				format.html { redirect_to @survey, notice: 'La encuesta se actualizó con éxito.' }
				format.json { render :show, status: :ok, location: @survey }
			else
				format.html { render :edit }
				format.json { render json: @survey.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /surveys/1
	# DELETE /surveys/1.json
	def destroy
		@survey.destroy
		respond_to do |format|
			format.html { redirect_to admin_surveys_url, notice: 'La encuesta se eliminón con éxito.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_survey
			@survey = Admin::Survey.where(id: params[:id]).first
			notice = "La encuesta buscada no se encontró."
			redirect_to(admin_surveys_url, notice: notice) if @survey.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def survey_params
			params.require(:admin_survey).permit(:purpose, :audience, :template_name)
		end
end
