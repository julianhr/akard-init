class Admin::AgentsController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_agent, only: [:show, :edit, :update, :destroy]

	# GET /agents
	# GET /agents.json
	def index
		@agents = Admin::Agent.all
	end

	# GET /agents/1
	# GET /agents/1.json
	def show
	end

	# GET /agents/1/edit
	def edit
	end

	# PATCH/PUT /agents/1
	# PATCH/PUT /agents/1.json
	def update
		respond_to do |format|
			if @agent.update(agent_params)
				format.html { redirect_to @agent, notice: 'Agent was successfully updated.' }
				format.json { render :show, status: :ok, location: @agent }
			else
				format.html { render :edit }
				format.json { render json: @agent.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /agents/1
	# DELETE /agents/1.json
	def destroy
		@agent.destroy
		respond_to do |format|
			format.html { redirect_to admin_agents_url, notice: 'Agent was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
	
	# Use callbacks to share common setup or constraints between actions.
	def set_agent
		@agent = Admin::Agent.where(id: params[:id]).first
		notice = "El agente buscado no existe."
		redirect_to(admin_agents_url, notice: notice) if @agent.nil?
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def agent_params
		params.require(:admin_agent).permit(:first_name, :last_name, :email, :approved)
	end
end
