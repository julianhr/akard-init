class Admin::BusinessBranchesController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_business_branch, only: [:show, :edit, :update, :destroy]
	before_action :set_business, only: [:edit, :update]
	before_action :set_states, only: [:new, :edit, :update, :create]

	# GET /business_branches
	# GET /business_branches.json
	def index
		@business_branches = Admin::BusinessBranch.all
	end

	# GET /business_branches/1
	# GET /business_branches/1.json
	def show
	end

	# GET /business_branches/new
	def new
		@business_branch = Admin::BusinessBranch.new
		@referring_business = nil

		if params['referring_business_id']
			@referring_business = Admin::Business.where(id: params['referring_business_id']).first 
			flash[:referring_business_id] = @referring_business.id if @referring_business
		end
	end

	# GET /business_branches/1/edit
	def edit
		@referring_business = nil
		
		if params['referring_business_id']
			@referring_business = Admin::Business.where(id: params['referring_business_id']).first 
			flash[:referring_business_id] = @referring_business.id if @referring_business
		end
	end

	# POST /business_branches
	# POST /business_branches.json
	def create
		@business_branch = Admin::BusinessBranch.new(business_branch_params)
		@referring_business = get_business_from_flash()
		render_opt = {}

		if @referring_business
			unless @referring_business == @business_branch.business
				flash[:alert] = "El negocio referente y el indicado por la sucursal no son el mismo."
				redirect_to admin_businesses_url and return
			end

			flash[:referring_business_id] = @referring_business.id
			render_opt = {referring_business_id: @referring_business.id}
		end

		begin
			@business_branch.save!
			flash[:notice] = "La sucursal se creó con éxito."
		rescue ActiveRecord::RecordInvalid => e
			render(:new, render_opt) and return
		rescue => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			flash[:alert] = "Hubo un error al intentar crear la sucursal y no se pudo concretar la acción."
			render(:new, render_opt) and return
		end

		if @referring_business
			redirect_to @referring_business
		else
			redirect_to @business_branch
		end
	end

	# PATCH/PUT /business_branches/1
	# PATCH/PUT /business_branches/1.json
	def update
		@referring_business = get_business_from_flash()
		render_opt = {}

		if @referring_business
			unless @referring_business == @business_branch.business
				flash[:alert] = "El negocio referente y el indicado por la sucursal no son el mismo."
				redirect_to admin_businesses_url and return
			end

			flash[:referring_business_id] = @referring_business.id
			render_opt = {referring_business_id: @referring_business.id}
		end

		begin
			@business_branch.update!(business_branch_params)
			flash[:notice] = "La sucursal se actualzó con éxito."
		rescue ActiveRecord::RecordInvalid => e
			render(:edit, render_opt) and return
		rescue => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			flash[:alert] = "Hubo un error al intentar actualizar la sucursal y no se pudo concretar la acción."
			render(:edit, render_opt) and return
		end

		if @referring_business
			redirect_to @referring_business
		else
			redirect_to @business_branch
		end
	end

	# DELETE /business_branches/1
	# DELETE /business_branches/1.json
	def destroy
		@referring_business = nil
		render_opt = {}

		if business_id = params['referring_business_id']
			@referring_business = Admin::Business.where(id: business_id.strip).first
		end

		if @referring_business
			unless @referring_business == @business_branch.business
				flash[:alert] = "El negocio referente y el indicado por la sucursal no son el mismo."
				redirect_to admin_businesses_url and return
			end
			render_opt = {referring_business_id: @referring_business.id}
		end

		begin
			@business_branch.destroy!
			flash[:notice] = "La sucursal se eliminó con éxito."
		rescue => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			flash[:alert] = "Hubo un error al intentar eliminar la sucursal y no se pudo concretar la acción."
		end

		if @referring_business
			redirect_to @referring_business
		else
			redirect_to @business_branch
		end
	end

	private
	
		# Use callbacks to share common setup or constraints between actions.
		def set_business_branch
			@business_branch = Admin::BusinessBranch.where(id: params[:id]).first

			if @business_branch.nil?
				notice = "La sucursal buscada no existe."

				if business_id = params['referring_business_id'] or business_id = flash['referring_business_id']
					redirect_to(admin_business_url(business_id), notice: notice)
				else
					redirect_to(admin_business_branches_url, notice: notice)
				end
			end
		end

		def set_business 
			@business = Admin::Business.where(id: @business_branch.business.id).first
		end

		def set_states 
			@states_opt = Admin::State.all.map { |state| [state.name, state.id] }
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def business_branch_params
			params.require(:admin_business_branch).permit(
				:business_id,
				:type_of_branch,
				:street_name,
				:street_no,
				:inner_no,
				:street_context,
				:borough,
				:city,
				:zip,
				:state_id,
				:contact,
				:latitude,
				:longitude
			)
		end

		def get_business_from_flash
			if flash[:referring_business_id]
				referring_business_id = flash[:referring_business_id]
				Admin::Business.where(id: referring_business_id).first
			end
		end
end
