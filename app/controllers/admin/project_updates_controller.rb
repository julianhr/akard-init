class Admin::ProjectUpdatesController < ApplicationController
	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_project_update, only: [:show, :edit, :update, :destroy]

	# GET /admin/project_updates
	# GET /admin/project_updates.json
	def index
		@project_updates = Admin::ProjectUpdate.all
	end

	# GET /admin/project_updates/1
	# GET /admin/project_updates/1.json
	def show
	end

	# GET /admin/project_updates/new
	def new
		@project_update = Admin::ProjectUpdate.new
	end

	# GET /admin/project_updates/1/edit
	def edit
	end

	# POST /admin/project_updates
	# POST /admin/project_updates.json
	def create
		@project_update = Admin::ProjectUpdate.new(project_update_params)

		respond_to do |format|
			if @project_update.save
				format.html { redirect_to @project_update, notice: 'Project update was successfully created.' }
				format.json { render :show, status: :created, location: @project_update }
			else
				format.html { render :new }
				format.json { render json: @project_update.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /admin/project_updates/1
	# PATCH/PUT /admin/project_updates/1.json
	def update
		respond_to do |format|
			if @project_update.update(project_update_params)
				format.html { redirect_to @project_update, notice: 'Project update was successfully updated.' }
				format.json { render :show, status: :ok, location: @project_update }
			else
				format.html { render :edit }
				format.json { render json: @project_update.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /admin/project_updates/1
	# DELETE /admin/project_updates/1.json
	def destroy
		@project_update.destroy
		respond_to do |format|
			format.html { redirect_to admin_project_updates_url, notice: 'Project update was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_project_update
			@project_update = Admin::ProjectUpdate.where(id: params[:id]).first
			notice = "El usuario en actualizaciones del proyecto buscado no se encontró."
			redirect_to(admin_project_updates_url, notice: notice) if @project_update.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def project_update_params
			params.require(:project_update).permit(:user_id, :email_status, :metadata)
		end
end
