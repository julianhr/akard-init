class Admin::CouponsController < ApplicationController
	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_coupon, only: [
		:show, 
		:preview, 
		:edit, 
		:update, 
		:destroy, 
		:upload_image, 
		:delete_image
	]
	before_action :set_business_temp_variables, only: [:show, :new, :edit, :create, :update]


	# GET /coupons
	# GET /coupons.json
	def index
		@coupons = Admin::Coupon.all
	end

	# GET /coupons/1
	# GET /coupons/1.json
	def show
		@image_paths = ImageExplorer.new('promotions', @coupon.id).get_image_paths
	rescue Exception => e
		redirect_to admin_path, notice: e.message
	end

	# GET /coupons/1/preview
	def preview 
		@business_img_path = ImageExplorer.new('businesses', @coupon.business_id)
			.get_image_paths().last
		@coupon_img_paths = ImageExplorer.new('promotions', @coupon.id)
			.get_image_paths()
		@errors = flash[:cw_errors] || {}
	end

	# GET /coupons/new
	def new
		@coupon = Admin::Coupon.new
		@businesses = Admin::Business.order(:name)
	end

	# GET /coupons/1/edit
	def edit
		@businesses = Admin::Business.order(:name)
	end

	# POST /coupons
	# POST /coupons.json
	def create
		@coupon = Admin::Coupon.new coupon_params()

		respond_to do |format|
			if @coupon.save
				format.html { redirect_to @coupon, notice: 'El cupón se creó con éxito.' }
				format.json { render :show, status: :created, location: @coupon }
			else
				format.html { render :new }
				format.json { render json: @coupon.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /coupons/1
	# PATCH/PUT /coupons/1.json
	def update
		respond_to do |format|
			if @coupon.update(coupon_params)
				format.html { redirect_to @coupon, notice: 'El cupón se actualizó con éxito.' }
				format.json { render :show, status: :ok, location: @coupon }
			else
				format.html { render :edit }
				format.json { render json: @coupon.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /coupons/1
	# DELETE /coupons/1.json
	def destroy
		@coupon.destroy
		respond_to do |format|
			format.html { redirect_to admin_coupons_url, notice: 'El cupón se eliminó con éxito.' }
			format.json { head :no_content }
		end
	end

	# PUT /coupons/1/add_logo
	def upload_image
		@image_io = params[:img]
		@img_explorer = ImageExplorer.new('promotions', @coupon.id)
		extension = nil
		last_file_name = nil
		next_file_name = nil
		notice = ''

		# check for correct @image_io type
		unless @image_io.respond_to? :original_filename
			e = RuntimeError.new("@image_io '#{@image_io}' doesn't respond to :original_filename")
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
			notice = "Hubo un error al procesar la imagen."
			redirect_to(@coupon, notice: notice) and return
		end

		# process @image_io and attempt to upload the image
		begin
			extension = @image_io.original_filename.split('.').last.downcase
			last_file_name = @img_explorer.get_image_file_names().last
			next_file_name = gallery_next_img_file_name(last_file_name, extension)
			@img_explorer.upload_image(next_file_name, @image_io)
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			redirect_to(@coupon, notice: e.message) and return
		end

		# happy path redirection
		redirect_to @coupon, notice: 'La imagen se agregó con éxito.'
	end	

	# DELETE /coupons/1/delete_logo
	def delete_image
		@img_explorer = ImageExplorer.new('promotions', @coupon.id)

		begin
			@img_explorer.delete_image(params[:file])
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			redirect_to(@coupon, notice: e.message) and return
		end

		redirect_to @coupon, notice: 'La imagen se eliminó con éxito'
	end

	private

		# Never trust parameters from the scary internet, only allow the white list through.
		def coupon_params
			params.require(:admin_coupon).permit(
				:business_id,
				:business_branch_id,
				:type_of_coupon,
				:publish_active,
				:publish_start_datetime,
				:publish_end_datetime,
				:valid_start_datetime,
				:valid_end_datetime,
				:exchanged_datetime,
				:status,
				:title,
				:description,
				:price_regular,
				:price_promo,
				:quantity,
				:available,
				:conditions
			)
		end

		# Use callbacks to share common setup or constraints between actions.
		def set_coupon
			@coupon = Admin::Coupon.where(id: params[:id]).first
			notice = "El cupón buscado no se encontró."
			redirect_to(admin_coupons_url, notice: notice) if @coupon.nil?
		end

		def set_business_temp_variables 
			@business_temp = set_business_temp()
			@branches = set_branches()
			@branches_short_address = set_branches_short_address()
		end

		def set_business_temp
			if params[:admin_coupon].present? and @coupon.nil?
				@coupon = Admin::Coupon.new coupon_params
			else
				@coupon ||= Admin::Coupon.new
			end

			if params[:coupon_business_id]
				return Admin::Business.where(id: params[:coupon_business_id]).first || {}
			elsif @coupon.business_id? and params[:coupon_business_id].nil?
				return Admin::Business.where(id: @coupon.business_id).first || {}
			else
				return {}
			end
		end

		def set_branches
			if @business_temp.present?
				return Admin::BusinessBranch.where(business_id: @business_temp.id) || {}
			elsif @coupon.persisted?
				return Admin::BusinessBranch.where(business_id: @coupon.business_branch_id) || {}
			else
				return {}
			end
		end

		def set_branches_short_address 
			if @branches.present?
				return @branches.map do |branch|
					address = branch.street_name + ' ' + branch.street_no
					address += ' int. ' + branch.inner_no if branch.inner_no?
					address += ', ' + branch.borough + ', ' + branch.city
					[address, branch.id]
				end
			else
				return []
			end
		end

		def gallery_next_img_file_name(last_file_name, extension) 
			file_name = ''

			case last_file_name
			when nil
				file_name = "cupon_#{@coupon.id}_1.#{extension}"
			else
				next_number = last_file_name.match(/_\d+\./).to_s[1..-2].to_i + 1
				file_name = "cupon_#{@coupon.id}_#{next_number}.#{extension}"
			end

			return file_name
		end

end
