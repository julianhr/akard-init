class Admin::SurveysDispensedController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_surveys_dispensed, only: [:show, :edit, :update, :destroy]

	# GET /surveys_dispensed
	# GET /surveys_dispensed.json
	def index
		@surveys_dispensed = Admin::SurveysDispensed.order(:id)
	end

	# GET /surveys_dispensed/1
	# GET /surveys_dispensed/1.json
	def show
	end

	# GET /surveys_dispensed/new
	def new
		@surveys_dispensed = Admin::SurveysDispensed.new
	end

	# GET /surveys_dispensed/1/edit
	def edit
	end

	# POST /surveys_dispensed
	# POST /surveys_dispensed.json
	def create
		@surveys_dispensed = Admin::SurveysDispensed.new(surveys_dispensed_params)

		respond_to do |format|
			if @surveys_dispensed.save
				format.html { redirect_to @surveys_dispensed, notice: 'Surveys dispensed was successfully created.' }
				format.json { render :show, status: :created, location: @surveys_dispensed }
			else
				format.html { render :new }
				format.json { render json: @surveys_dispensed.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /surveys_dispensed/1
	# PATCH/PUT /surveys_dispensed/1.json
	def update
		respond_to do |format|
			if @surveys_dispensed.update(surveys_dispensed_params)
				format.html { redirect_to @surveys_dispensed, notice: 'Surveys dispensed was successfully updated.' }
				format.json { render :show, status: :ok, location: @surveys_dispensed }
			else
				format.html { render :edit }
				format.json { render json: @surveys_dispensed.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /surveys_dispensed/1
	# DELETE /surveys_dispensed/1.json
	def destroy
		@surveys_dispensed.destroy

		respond_to do |format|
			format.html { redirect_to admin_surveys_dispensed_index_url, notice: 'Surveys dispensed was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_surveys_dispensed
			@surveys_dispensed = Admin::SurveysDispensed.where(id: params[:id]).first
			notice = "La encuesta dispensada buscada no se encontró."
			redirect_to(admin_surveys_dispensed_index_url, notice: notice) if @surveys_dispensed.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def surveys_dispensed_params
			params.require(:admin_surveys_dispensed).permit(:survey_id, :user_id, :where, :response)
		end
end
