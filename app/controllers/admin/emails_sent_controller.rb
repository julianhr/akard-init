class Admin::EmailsSentController < ApplicationController
	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_email_sent, only: [:show, :edit, :update, :destroy]

	# GET /emails_sent
	# GET /emails_sent.json
	def index
		@emails_sent = Admin::EmailSent.all
	end

	# GET /emails_sent/1
	# GET /emails_sent/1.json
	def show
	end

	# GET /emails_sent/new
	def new
		@email_sent = Admin::EmailSent.new
	end

	# GET /emails_sent/1/edit
	def edit
	end

	# POST /emails_sent
	# POST /emails_sent.json
	def create
		@email_sent = Admin::EmailSent.new(email_sent_params)

		respond_to do |format|
			if @email_sent.save
				format.html { redirect_to @email_sent, notice: 'Email sent was successfully created.' }
				format.json { render :show, status: :created, location: @email_sent }
			else
				format.html { render :new }
				format.json { render json: @email_sent.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /emails_sent/1
	# PATCH/PUT /emails_sent/1.json
	def update
		respond_to do |format|
			if @email_sent.update(email_sent_params)
				format.html { redirect_to @email_sent, notice: 'Email sent was successfully updated.' }
				format.json { render :show, status: :ok, location: @email_sent }
			else
				format.html { render :edit }
				format.json { render json: @email_sent.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /emails_sent/1
	# DELETE /emails_sent/1.json
	def destroy
		@email_sent.destroy
		respond_to do |format|
			format.html { redirect_to admin_emails_sent_url, notice: 'Email sent was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_email_sent
			@email_sent = Admin::EmailSent.where(id: params[:id]).first
			notice = "El correo enviado buscado no se encontró"
			redirect_to(admin_emails_sent_url, notice: notice) if @email_sent.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def email_sent_params
			params.require(:admin_email_sent).permit(
				:email_id, 
				:user_id, 
				:sent, 
				:status, 
				:attempts, 
				:response,
				:metadata
			)
		end
end
