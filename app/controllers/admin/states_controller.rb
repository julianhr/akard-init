class Admin::StatesController < ApplicationController

	before_action :authenticate_agent!
	before_action :authorize_agent!
	before_action :set_state, only: [:show, :edit, :update, :destroy]

	# GET /states
	# GET /states.json
	def index
		@states = Admin::State.all
	end

	# GET /states/1
	# GET /states/1.json
	def show
	end

	# GET /states/1/edit
	def edit
	end

	# PATCH/PUT /states/1
	# PATCH/PUT /states/1.json
	def update
		respond_to do |format|
			if @state.update(state_params)
				format.html { redirect_to @state, notice: 'State was successfully updated.' }
				format.json { render :show, status: :ok, location: @state }
			else
				format.html { render :edit }
				format.json { render json: @state.errors, status: :unprocessable_entity }
			end
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_state
			@state = Admin::State.where(id: params[:id]).first
			notice = "El estado buscado no se encontró."
			redirect_to(admin_states_url, notice: notice) if @state.nil?
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def state_params
			params.require(:admin_state).permit(:name)
		end
end
