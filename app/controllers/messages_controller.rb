class MessagesController < ApplicationController

	# GET /messages
	def new
		@message = Message.new
		@message_sent = params[:sent] == 'true' ? true : false
	end

	# POST /messages
	def create
		@message = Message.new message_params

		if @message.valid?
			email = MailerGeneral.internal(@message, :contact)

			begin
				email.deliver_now
			rescue Exception => e
				ExceptionHandler.log(e, :error, __FILE__, __LINE__)
				alert = "Hubo un error al enviar el mensaje. Favor de volver a intentar."
				redirect_to :back, alert: alert and return
			end

			redirect_to contact_url(sent: true)
		else
			flash[:alert] = "Hubo un error al enviar el mensaje. Favor de intentar otra vez."
			render :new 	
		end
	end

	private

		def message_params
			params.require(:message).permit(:name, :email, :subject, :body)
		end
	
end
