class RegistrationController < ApplicationController
	include DeliverEmail

	# GET /new
	def new
		@user = User.new
		@errors = flash[:new_user_errors] || {}
	end

	# POST /create
	def create
		# variables
		@user = User.new(user_params)
		@project_update = nil
		@survey = nil
		@email = nil
		@email_sent = nil
		notice = nil

		# strip email
		@user.email.strip! if @user.email.class == String

		# attempt to save the user
		begin
			@user.save!
		rescue ActiveRecord::RecordInvalid
			flash[:new_user_errors] = @user.errors
			redirect_to root_url and return
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			alert = "Hubo un error al intentar guardar el correo. Favor de voler a intentar."
			redirect_to root_url, alert: alert and return
		end

		# assign rest of the variables
		@project_update = Admin::ProjectUpdate.new(user_id: @user.id, email_status: 'habilitado')
		@survey = new_signup_survey(@user.id)
		@email = Admin::Email.where(model: 'users', purpose: 'registro_exito').first
		@email_sent = new_email_sent(@user.id, @email.id)

		# attempt to save @project_update, @survey and @email_sent
		begin
			@project_update.save!
			@survey.save!
			@email_sent.save!
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			notice = "Lo sentimos pero hubo un error guardando la información. El administrador ya fue notificado. No es necesario volver a registrar el correo."
			redirect_to root_url, notice: notice and return		
		end

		# attempt to send email to user
		@user_mailer = MailerGeneral.user_registration(@user.id, @email.id, @email_sent.id)
		deliver_emailsent(@user_mailer, :deliver_later, @email_sent)

		# redirect to the survey
		opt_h = {email: @user.email, id: @user.id}
		redirect_to registro_exito_url(opt_h)
	end

	# GET /success
	def success
		# variables
		@user = get_user(params['id'])
		@survey_dispensed = nil
		params_email = params['email'].strip

		# redirect to root with notice if nil on @user, params_email or @user
		if @user.nil? or params_email.nil? or @user.email != params_email
			alert = "La información proporcionada no es correcta. No se pudo dirigir hacia la encuesta."
			redirect_to root_url, alert: alert and return 
		end

		# assign @survey_dispensed
		@survey_dispensed = Admin::SurveysDispensed.where(
			user_id: @user.id,
			survey_id: registration_survey().id
		).first
	end

	# POST /survey
	def survey
		# variables
		@user = nil
		@survey_dispensed = nil
		@survey = nil
		@ssp = nil
		registro_exito_opt = {}

		# assign @user, @survey_dispensed and @survey
		if @user = get_user(params[:user_id])
			@survey_dispensed = get_survey_dispensed(params[:sd_id], @user.id)
			@survey = params[:survey]
		end

		# global alert message
		alert = "Hubo un error al procesar la encuesta. Favor de intentar otra vez."

		# redirect to root_url if nil values on @user, @survey_dispensed or @survey
		if @user.nil? or @survey_dispensed.nil? or @survey.nil?
			if @user
				redirect_to :back, alert: alert and return
			else
				redirect_to root_url, alert: alert and return
			end
		end

		# verify that user and survey_dispensed agree
		unless @survey_dispensed.user_id == @user.id
			redirect_to :back, alert: alert and return
		end

		# verify survey responses and attempt to save them
		@ssp = SurveySignupProcessor.new(@survey, @user.id, @survey_dispensed.id)
		begin
			@ssp.verify_responses()
			@ssp.save()
		rescue TypeError, IndexError => e
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
			redirect_to :back, alert: alert and return
		rescue Exception => e
			ExceptionHandler.log(e, :fatal, __FILE__, __LINE__)
			redirect_to :back, alert: alert and return
		end

		# happy path to successful registration
		flash[:referrer] = 'signup_survey'
		redirect_to promotions_url, notice: notice
	end

	private

		# Never trust parameters from the scary internet, only allow the white list through.
		def user_params
			params.require(:user).permit(:email)
		end

		def sanitize_id(id)
			id.strip! if id.class == String
			return nil unless id.respond_to? :to_i

			return id.to_i
		end

		def get_user(user_id) 
			return nil unless user_id = sanitize_id(user_id)
			User.where(id: user_id).first
		end

		def new_signup_survey(user_id) 
			Admin::SurveysDispensed.new(
				survey_id: registration_survey().id,			
				user_id: user_id,
				where: 'registro_exito'
			)
		end

		def new_email_sent(user_id, email_id)
			Admin::EmailSent.new(
				user_id: user_id, 
				email_id: email_id, 
				status: 'staged_for_send',
				attempts: 0, 
				response: {}
			)
		end

		def registration_survey 
			Admin::Survey.where(purpose: 'registro_exito', audience: 'users').first
		end

		def get_survey_dispensed(sd_id, user_id) 
			survey_id = registration_survey().id
			sd_id = sanitize_id(sd_id) or return nil
			sd = Admin::SurveysDispensed.where(id: sd_id).first or return nil

			if sd.user_id != user_id or sd.survey_id != survey_id
				return nil
			else
				return sd
			end
		end
end
