class AwsSqsEmailWorker
	include Shoryuken::Worker
	shoryuken_options queue: 'sqs_prelaunch_email_sent', auto_delete: false

	def perform(sqs_msg, body) 
		updater = EmailSqsUpdater.new( body, 1, master_email() )

		begin			
			updater.update() or raise
			sqs_msg.delete()
		rescue Exception => e
			ExceptionHandler.log(e, :error, __FILE__, __LINE__)
		end
	end

	private

		def master_email 
			master_email ||= Admin::Email.where(model: 'users', purpose: 'registro_exito').first
		end

end
