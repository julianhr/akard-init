// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function countdown(jq_selector) {
	var endDate = jq_selector.data('end');
	jq_selector.countdown(endDate, function(event) {
		var that = $(this);
		var hourInSec = 60*60;
		var dayInSec = hourInSec*24;
		var daysLeft = event.offset.totalDays;
		var hoursLeft = event.strftime('%H');
		var minLeft = event.strftime('%M');
		var secLeft = event.strftime('%S');
		var timeLeftInSec = daysLeft*dayInSec + hoursLeft*60*60 + minLeft*60 + secLeft*1;

		if (timeLeftInSec === 0) {
			that.text('Tiempo agotado');
		} else if (timeLeftInSec < hourInSec) {
			that.text(event.strftime('%-M min %S seg'));
		} else if (timeLeftInSec < dayInSec ) {
			that.text(event.strftime('%-H hr %M min %S seg'));
		} else if (timeLeftInSec < dayInSec*2) {
			that.text(event.strftime('%-D día %H:%M:%S'));
		} else if (timeLeftInSec > dayInSec*2) {
			that.text(event.strftime('%-D días %H:%M:%S'));
		}
	});
}

var coupon = function() {
	jq_selector = $('.countdown .timer');
	if (jq_selector) { new countdown(jq_selector); }
};

$(document).ready(coupon);
$(document).on('page:load', coupon);
