function agentApprove(event, agent_id, approve_bool) {
	event.preventDefault();

	data_h = {
		_method: 'PATCH',
		agent: {approved: approve_bool}
	};

	$.ajax({
		type: "POST",
		url: '/admin/agents/' + agent_id,
		data: data_h
	}).done(function(msg) {
		window.location.reload(false);
	}).fail(function(error) {
		console.log(error);
	});
}
