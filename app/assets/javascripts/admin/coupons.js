'strict mode';

function CouponForm (html) {
	var select_business = $('select#admin_coupon_business_id')[0];

	if (!select_business) { return; }

	select_business.addEventListener('change', function() {
		var path = window.location.pathname;
		path += '?coupon_business_id=' + select_business.selectedIndex;
		window.location = path;
	});
}


/** 
* Variable that contains all calls to be fired when document is ready.
* @global
*/
var ready = function() {
	var x = new CouponForm(this);
};

$(document).ready(ready);
$(document).on('page:load', ready);