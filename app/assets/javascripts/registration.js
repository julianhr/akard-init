// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function formSurveySignup() {
	var spanRadio = document.querySelectorAll('span.radio');

	for(var i = 0; i < spanRadio.length; i++) {
		spanRadio[i].addEventListener('click', function(event) {
			var that = $(this);
			var parent_li = that.closest('li');
			parent_li.find('span').removeClass('selected');
			that.addClass('selected');
			that.find('input[type="radio"]').prop('checked', true);
		});
	}
}

var registration = function() {
	if ($('form[registro-encuesta]')) { new formSurveySignup(); }
};

$(document).ready(registration);
$(document).on('page:load', registration);
