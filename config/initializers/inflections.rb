# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:

ActiveSupport::Inflector.inflections(:en) do |inflect|
	inflect.plural /^(email)(sent)/i, '\1s\2'
	inflect.plural /^(admin_)?(email)(_sent)/i, '\1\2s\3'
	inflect.singular /^(admin_)?(email)s(_sent)/i, '\1\2\3'
	inflect.uncountable %w(adminsurveysdispensed admin_surveys_dispensed surveysdispensed surveys_dispensed)
	# inflect.uncountable %w(emailssent emails_sent)
end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
