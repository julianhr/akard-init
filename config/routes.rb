require 'sidekiq/web'

Rails.application.routes.draw do

	# The priority is based upon order of creation: first created -> highest priority.
	# See how all your routes lay out with "rake routes".

	root 'registration#new'

	scope :registro, as: 'registro' do
		get 'nuevo' => 'registration#new'
		get 'exito' => 'registration#success'
		post 'crear' => 'registration#create'
		post 'encuesta' => 'registration#survey'
	end

	scope 'promociones' do
		get 	'/', 					to: 'promotions#index', 		as: 'promotions'
		get 	'/:id', 				to: 'promotions#show', 			as: 'promotion'
		post 	'/:id', 				to: 'promotions#claim'
		get 	'/:id/ganador',	to: 'promotions#winner', 		as: 'promotion_winner'
	end

	scope 'contacto', as: 'contact' do
		get '/', to: 'messages#new'
		post '/', to: 'messages#create'
	end

	get '/preguntas-frecuentes',	 to: 'static_pages#faq'
	get '/terminos',					 to: 'static_pages#site_terms'
	get '/quienes-somos',			 to: 'static_pages#about'
	get '/instrucciones',			 to: 'static_pages#instructions'
	
	devise_for :agents, path_names: {
 		sign_in: 'acceder', 
 		sign_out: 'salir', 
 		password: 'contrasena', 
 		confirmation: 'confirmacion', 
 		unlock: 'desbloquear', 
 		registration: 'registro', 
 		sign_up: 'nueva-cuenta'
	}

	authenticate :agent, lambda { |a| a.approved? } do
		mount Sidekiq::Web => 'admin/sidekiq'
	end

	get '/admin', to: redirect('admin/dashboard/index')

	namespace :admin do
		resources :agents, except: [:new, :create]
		resources :business_branches
		resources :businesses do
			member do
				post 'upload_logo'
				delete 'delete_logo'
			end
		end
		resources :coupon_winners
		resources :coupons do
			member do
				get 'preview'
				post 'upload_image'
				delete 'delete_image'
			end
		end
		get 'dashboard/index'
		resources :emails
		resources :emails_sent
		resources :project_updates
		resources :states, except: [:new, :create, :destroy]
		resources :surveys
		resources :surveys_dispensed
		resources :users
	end

	match '*path', to: redirect('/404'), via: :all

end
