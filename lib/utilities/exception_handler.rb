class ExceptionHandler

	ERROR_LEVELS = [
		:debug,
		:info,
		:warn,
		:error,
		:fatal
	]

	def self.log(error, level, file, line) 
		raise TypeError, "error is of incorrect type" unless error.class <= Exception
		raise TypeError, "level is of incorrect type" unless [String, Symbol].include? level.class
		raise ArgumentError, "level is not a valid option" unless ERROR_LEVELS.include? level.to_sym
		raise TypeError, "file is not a String" unless file.class == String
		raise TypeError, "line is not a Fixnum" unless line.class == Fixnum

		level.strip! if level.class == String
		
		separator = "--------------------------------------"
		nl = "\n"
		level = level.to_sym

		exception_class = "Exception Class -> #{error.class.to_s}"
		descriptors = "File: #{file}"
		descriptors += nl + "Line: #{line}"
		descriptors += nl + "Timestamp: #{Time.now}"

		log_output = separator
		log_output += nl + "Error logged by ExceptionHandler"
		log_output += nl
		log_output += nl + exception_class
		log_output += nl + descriptors

		if error.message
			log_output += nl
			log_output += nl + "-- Error Message: --"
			log_output += nl + error.message
		end

		if error.backtrace
			log_output += nl
			log_output += nl + "-- Error Backtrace: --"
			log_output += nl + error.backtrace.join("\n")
		end

		log_output += nl + separator

		Rails.logger.send level, log_output

		return "#{exception_class}\n#{descriptors}\n#{error.message}"
	end

end
