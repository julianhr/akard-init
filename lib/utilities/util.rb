class Util
	REGEX = {
		email: /\A([^@\s]+)@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i,
		url: /\Ahttps?:\/\/(?#tld)(www\.)?([\w\-])+(?<!www)\.\w{2,10}(?#path)(\/[\w\-]+)*(?#resource)(\/[\w\-]+\.\w{2,6})?\/?\z/i
	}
end
