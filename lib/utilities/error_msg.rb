class ErrorMsg

	MSG = {
		# Generic messages
		empty_field: 'Este campo no puede quedar vacío',
		invalid_option: "'%{value}' no es una opción válida.",
		not_unique: "El texto capturado ya existe.",

		# Text
		text_too_long: 'El texto capturado es demasiado largo.',

		# Numbers
		number_digits_only: 'Este campo solo puede recibir dígitos.',
		number_digits_positive: 'Este campo solo puede recibir dígitos positivos y cero.',
		number_only: 'Este campo solo puede recibir números con signo y/o parte decimal.',
		number_currency: 'Debe contener dígitos y opcionalmente hasta dos decimales.',
		number_currency_gr_zero: 'Debe contener dígitos positivos y opcionalmente hasta dos decimales.',
		number_too_long: 'El número capturado es demasiado largo.',
		number_zip: 'El número capturado debe ser de 5 digitos.',

		# Date, DateTime
		date_end_lt_or_eq_to_start: "Esta fecha no puede ser anterior o igual a la de inicio.",

		# Password
		pass_invalid: 'La contraseña proporcionada parece no ser válida.',
		pass_too_short: 'La contraseña proporcionada es muy corta.',

		# Email
		email_missing: 'Parece que olvidaste poner tu correo.',
		email_too_long: 'El correo proporcionado es demasiado largo.',
		email_invalid: 'El correo proporcionado parece no ser válido.',
		email_duplicity: 'Lo sentimos pero ese correo ya está registrado.',

		# Promotions
		promo_depleted: "Lo sentimos pero este cupón se ha agotado.",
		promo_user_repeat: "Lo sentimos pero el correo proporcionado ya se ganó esta promoción.",

		# url
		url_invalid: 'La URL proporcionada no parece ser válida.',
		url_duplicity: 'Lo sentimos pero esta URL ya está registrada.',

		# customized
		email_missing_cry: 'Parece que olvidaste poner tu correo :(',
		coupon_availability_out_of_range: 'La disponibilidad tiene que ser >= 0, <= Cantidad, y contener solo dígitos.',
		coupon_price_promo_gt_regular: "El precio de pomoción no puede igual o mayor al precio regular.",
		date_valid_lt_publish: "La fecha final de validez no puede ser anterior a la fecha final de publicación.",
		date_exchanged_lt_valid_start: "La fecha de canjeo no puede ser anterior a la fecha de inicio de validez.",
	}

end
